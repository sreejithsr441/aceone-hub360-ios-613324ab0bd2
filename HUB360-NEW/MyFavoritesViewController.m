//
//  MyFavoritesViewController.m
//  HUB360-NEW
//
//  Created by Oyuka on 1/13/14.
//  Copyright (c) 2014 Oyuka. All rights reserved.
//

#import "MyFavoritesViewController.h"
#import "NavigationControllerBuilder.h"
#import "hub360Config.h"
#import "SideNavigationView.h"
#import "UIImageView+Network.h"
#import "Home.h"
#import "ViewDetailViewController.h"
#import "SearchBox.h"
#import "SearchResultViewController.h"
#import "ActivityIndicator.h"
#import "WebViewController.h"
#import "AdvertisementBlock.h"

@interface MyFavoritesViewController ()

@end

@implementation MyFavoritesViewController
{
    NSMutableArray *_myFavoritesListArray;
    UITableView *_myFavoritesTable;
    NSXMLParser *xmlParser;
    UIView *_mainContainer;
    hub360Config *_config;
    SideNavigationView *_sideNav;
    NavigationControllerBuilder *_navigationBar;
    SearchBox *_searchBox;
    ActivityIndicator *_activityIndicator;
    AdvertisementBlock *_advertisementBanner;
    AdvertisementBlock *_advertisementHalfPage;
}

-(void)viewDidAppear:(BOOL)animated{
    
    [self checkAdsCount];

}

-(void)checkAdsCount{
    NSUserDefaults *userDefaults=[NSUserDefaults standardUserDefaults];
    NSString *countString = [userDefaults objectForKey:@"saveCountMyFavAds"];
    NSLog(@"Your Count: %@",countString);
    
    if(countString == nil)
        [userDefaults setObject:@"1" forKey:@"saveCountMyFavAds"];
    
    
    [userDefaults setObject:[NSString stringWithFormat:@"%d", [countString intValue]+1] forKey:@"saveCountMyFavAds"];
    [userDefaults synchronize];
    
    if(([countString integerValue]) % 2 == 0 && countString != nil){
        _advertisementHalfPage = [AdvertisementBlock new];
        [_mainContainer addSubview:_advertisementHalfPage];
        [_advertisementHalfPage getHalfAds];
        [userDefaults setObject:@"1" forKey:@"saveCountMyFavAds"];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    _config = [hub360Config new];
    
    _sideNav = [[SideNavigationView alloc]initWithDelegate:self frame:CGRectMake(0, 20, [_config width]-60, [_config height])];
    [_sideNav setDataSource:self];
    [self.view addSubview:_sideNav];
    
    _mainContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 20, [_config width], [_config height])];
    [_mainContainer setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:_mainContainer];
    
    [self getMyFavorites];
    
    // adding navigation controller
    _navigationBar = [[NavigationControllerBuilder alloc]initWithFrame:CGRectMake(0, 0, 320, 45)];
    [_navigationBar addNavigationContoller:@"My Favorites" :NO];
    [_mainContainer addSubview:_navigationBar];
    
    _activityIndicator = [ActivityIndicator new];
    [_mainContainer addSubview:_activityIndicator];
    
    // Banner add
    _advertisementBanner = [AdvertisementBlock new];
    [_mainContainer addSubview:_advertisementBanner];
    [_advertisementBanner getBannerAds];
    
    if([_config height] < 500){
        [_advertisementBanner setFrame:CGRectMake(0, 410, 320, 50)];
    }
    
    
}

-(void)getMyFavorites{
    
    UIDevice *myDevice = [UIDevice currentDevice];
	NSString *deviceNumber = [[myDevice identifierForVendor]UUIDString];

	NSLog(@"globaluid is%@",deviceNumber);
	
    NSString *urlString = [NSString stringWithFormat:@"http://webdesignjonesboro.com/hub360service/hub360Service.asmx/GetMyFavouritesNew?username=%@", deviceNumber];
    
	NSMutableURLRequest *req=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
	[req addValue:@"http://webdesignjonesboro.com/hub360service/hub360Service.asmx" forHTTPHeaderField:@"SOAPAction"]; //
	[req setHTTPMethod:@"GET"];
	
	NSError *error1;
	error1 = [[NSError alloc] init];
	
	NSData *returnData = [NSURLConnection sendSynchronousRequest:req returningResponse:nil error:&error1];
	NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
	NSLog(@"%@",returnString);
    
    NSRange start;
    NSRange stop;
    
    start = [returnString rangeOfString:@"<string xmlns=\"http://www.hub360nea.com/\">"];
    stop = [returnString rangeOfString: @"</string>"];
    
    unsigned long start1 = start.location+start.length;
    unsigned long start2 = stop.location-start1;
    
    NSString *jsonString = [returnString substringWithRange: NSMakeRange (start1, start2)];
    
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *e = nil;
    NSMutableArray *json = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&e];
    NSLog(@"%@", json);
    _myFavoritesListArray = [json valueForKey:@"List`1"];
    
    // my favorites table
    _myFavoritesTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 45, 320, 449)];
    _myFavoritesTable.layer.borderColor = [[UIColor grayColor] CGColor];
    _myFavoritesTable.layer.borderWidth = 1.0f;
    [_mainContainer addSubview:_myFavoritesTable];
    _myFavoritesTable.delegate = self;
    _myFavoritesTable.dataSource = self;
    
    if([_config height] < 500){
        [_myFavoritesTable setFrame:CGRectMake(0, 45, 320, 365)];
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if(tableView == _myFavoritesTable){
        NSLog(@"Favorites %lu", (unsigned long)[_myFavoritesListArray count]);
        if([_myFavoritesListArray count] == 0){
            return 1;
        }
        return ([_myFavoritesListArray count]);
    }else{
        return [_sideNav menucount];
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == _myFavoritesTable){
        return 80.0;
    }
    return 40.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell;
    
    if(tableView == _myFavoritesTable){
        
        if([_myFavoritesListArray count] == 0){
            cell = [tableView dequeueReusableCellWithIdentifier:@"emptycell"];
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"emptycell"];
            cell.textLabel.text = @"No saved item";
            cell.detailTextLabel.text = @"";
            cell.editing = FALSE;
            return cell;
        }else{
            NSLog(@"Events %lu", (unsigned long)[_myFavoritesListArray count]);
        }

        cell = [tableView dequeueReusableCellWithIdentifier:[[_myFavoritesListArray objectAtIndex:indexPath.row] objectForKey:@"companyName"]];
        
        if (cell == nil){
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:[[_myFavoritesListArray objectAtIndex:indexPath.row] objectForKey:@"companyName"]];
            
            NSURL *url = [NSURL URLWithString:[[_myFavoritesListArray objectAtIndex:indexPath.row] objectForKey:@"image"]];
            UIImageView *cellIconImage = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 60, 60)];
            if(url!=nil && [[[_myFavoritesListArray objectAtIndex:indexPath.row] objectForKey:@"companyID"] integerValue ] != 25){
                
                NSString *tmpimageurl = [[_myFavoritesListArray objectAtIndex:indexPath.row] objectForKey:@"image"];
                NSArray *cachingkey = [tmpimageurl componentsSeparatedByString:@"/"];
                
                [cellIconImage loadImageFromURL:url
                                placeholderImage:[UIImage imageNamed:@"default"]
                                      cachingKey:[cachingkey lastObject]
                                          frame:cellIconImage.frame];
            }else{
                cellIconImage.image = [UIImage imageNamed:@"fav-icon@2x"];
            }
            
            [cell addSubview:cellIconImage];
            cell.indentationLevel = 7;
            cell.accessoryType =  UITableViewCellAccessoryDisclosureIndicator;
            //    cell.imageView.image = imgView.image;
            
            cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14.0f];
            cell.textLabel.text = [self replaceAscii:[[_myFavoritesListArray objectAtIndex:indexPath.row] objectForKey:@"companyName"]];
            cell.textLabel.numberOfLines = 2;
            cell.textLabel.textColor = [UIColor grayColor];
            
            cell.detailTextLabel.text = [[_myFavoritesListArray objectAtIndex:indexPath.row] objectForKey:@"address"];
            cell.detailTextLabel.textColor = [UIColor colorWithRed:(68.0/255.0) green:(98.0/255.0) blue:(204.0/255.0) alpha:1];
            cell.detailTextLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:12.0f];
            cell.detailTextLabel.numberOfLines = 2;
        }
        
    }else{
        
        NSArray *list = [[NSArray alloc]initWithArray:[_sideNav listMenuItems]];
        NSArray *iconList = [[NSArray alloc]initWithArray:[_sideNav listMenuItemIcons]];
        
        if(indexPath.row == 0){
            cell = [tableView dequeueReusableCellWithIdentifier:@"searchBox"];
        }else{
            cell = [tableView dequeueReusableCellWithIdentifier:[list objectAtIndex:indexPath.row-1]];
        }
        
        
        if (cell == nil){
            
            if(indexPath.row == 0){
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"searchBox"];
                cell.backgroundColor = [UIColor clearColor];
                
                 _searchBox = [[SearchBox alloc]initWithFrame:CGRectMake(0, 0, cell.frame.size.width-60, cell.frame.size.height)];
                _searchBox.delegate = self;
                [cell.contentView addSubview:_searchBox];
                
            }else{
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:[list objectAtIndex:indexPath.row-1]];
                
                cell.textLabel.text = [list objectAtIndex:indexPath.row-1];
                [cell.textLabel setTextColor:[UIColor whiteColor]];
                cell.backgroundColor = [UIColor clearColor];
                cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14.0];
                
                UIImageView *accessoryImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 12.5, 19)];
                [accessoryImage setImage:[UIImage imageNamed:@"menu_cell_indicator"]];
                cell.accessoryView = accessoryImage;
                [cell.imageView setImage:[UIImage imageNamed:[iconList objectAtIndex:indexPath.row-1]]];
            }
            
        }

    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
   
    
    
    if(tableView == _myFavoritesTable){
    
        if([_myFavoritesListArray count]>0){
            
            [NSThread detachNewThreadSelector:@selector(startActivityIndicator) toTarget:_activityIndicator withObject:nil];
            
            ViewDetailViewController *viewDetail = [ViewDetailViewController new];
            
            // with offer
            viewDetail.subCategoryName = [[_myFavoritesListArray objectAtIndex:indexPath.row] objectForKey:@"All"];
            viewDetail.businessName = [self replaceAscii: [[_myFavoritesListArray objectAtIndex:indexPath.row] objectForKey:@"companyName"]];
            viewDetail.companyID = [[_myFavoritesListArray objectAtIndex:indexPath.row] objectForKey:@"companyID"];
            viewDetail.businessAddress = [[_myFavoritesListArray objectAtIndex:indexPath.row] objectForKey:@"address"];
            //            viewDetail.businessDistance = [[_myFavoritesListArray objectAtIndex:indexPath.row] objectForKey:@"All"];
            viewDetail.phoneNumber = [[_myFavoritesListArray objectAtIndex:indexPath.row] objectForKey:@"phone"];
            viewDetail.businessHours = [[_myFavoritesListArray objectAtIndex:indexPath.row] objectForKey:@"hours"];
            viewDetail.trending = [[_myFavoritesListArray objectAtIndex:indexPath.row] objectForKey:@"teaser"];
            viewDetail.rating = [[_myFavoritesListArray objectAtIndex:indexPath.row] objectForKey:@"rating"];
            viewDetail.couponImageURL = [[_myFavoritesListArray objectAtIndex:indexPath.row] objectForKey:@"image"];
            viewDetail.offer = [[_myFavoritesListArray objectAtIndex:indexPath.row] objectForKey:@"offer"];
            viewDetail.offerID = [[_myFavoritesListArray objectAtIndex:indexPath.row] objectForKey:@"offerID"];
            viewDetail.webAddress = [[_myFavoritesListArray objectAtIndex:indexPath.row] objectForKey:@"webAdd"];
            viewDetail.menuLink = [[_myFavoritesListArray objectAtIndex:indexPath.row] objectForKey:@"menu"];
            
            viewDetail.latestReview = [[_myFavoritesListArray objectAtIndex:indexPath.row] objectForKey:@"companyName"];
            
            if([[[_myFavoritesListArray objectAtIndex:indexPath.row] objectForKey:@"without_offer"] integerValue ] == 0){
                
                viewDetail.withCoupon = YES;
                
            }else{
                viewDetail.withCoupon = NO;
            }
            
            
            NSLog(@"Menu item selected");
            [self presentViewController:viewDetail animated:YES completion:nil];
            
             [NSThread detachNewThreadSelector:@selector(stopActivityIndicator) toTarget:_activityIndicator withObject:nil];
        }
        

        
    }else{
        
        if(indexPath.row > 0){
            [self selectMenuItem:indexPath.row];
            NSLog(@"Menu item selected");
        }

    }
    
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == _myFavoritesTable && [_myFavoritesListArray count]>0){
        return YES;
    }
    return  NO;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(editingStyle == UITableViewCellEditingStyleDelete && [_myFavoritesListArray count] > 0){
        
        NSString *urlString =[NSString stringWithFormat:@"http://webdesignjonesboro.com/hub360service/hub360Service.asmx/DeleteMyFav?myfID=%@", [[_myFavoritesListArray  objectAtIndex: [indexPath row]] objectForKey:@"myfID"]];
        
        NSMutableURLRequest *req=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
        [req addValue:@"http://webdesignjonesboro.com/hub360service/hub360Service.asmx" forHTTPHeaderField:@"SOAPAction"];
        [req setHTTPMethod:@"GET"];
        
        NSError *error1;
        error1 = [[NSError alloc] init];
        
        NSData *returnData1 = [NSURLConnection sendSynchronousRequest:req returningResponse:nil error:&error1];
        NSString *returnString = [[NSString alloc] initWithData:returnData1 encoding:NSUTF8StringEncoding];
        
        
        NSString *successMsg = @"Deleted Successfully";
        NSString *alertMsg =@"";
        if([returnString rangeOfString:successMsg].location != NSNotFound){
            alertMsg = successMsg;
        }
        
        UIAlertView *alrt=[[UIAlertView alloc]initWithTitle:alertMsg
                                                    message:@""
                                                   delegate:self
                                          cancelButtonTitle:nil
                                          otherButtonTitles:@"Ok",nil];
        [alrt show];
        
        [_myFavoritesListArray removeObjectAtIndex:[indexPath row]];
        
        [tableView reloadData];
        
    }
}

#pragma mark - Helper Methods

-(NSString *)replaceAscii:(NSString*)string{
    NSString *s = string;
    s = [s stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    return s;
}

-(void)selectMenuItem:(NSInteger)index{
    [_navigationBar showLeftNav];
    
    // change view controller
    
    switch (index) {
        case 1:
        {
            Home *homePage = [Home new];
            [self presentViewController:homePage animated:YES completion:nil];
            break;
        }
            
        case 2:{
            [self checkAdsCount];
            break;
        }
            
        case 3:
        {
            WebViewController *webVC = [WebViewController new];
            webVC.webURL = [NSURL URLWithString:@"http://aceonetechnologies.com/asu-game-schedule/"];
            webVC.title = @"ASU Game Schedule";
            [self presentViewController:webVC animated:YES completion:nil];
            break;
        }
            
        case 4:
        {
            WebViewController *webVC = [WebViewController new];
            webVC.webURL = [NSURL URLWithString:@"http://aceonetechnologies.com/friday-night-lights/"];
            webVC.title = @"High School Game Schedule";
            [self presentViewController:webVC animated:YES completion:nil];
            break;
        }
            
        case 5:
        {
            WebViewController *webVC = [WebViewController new];
            webVC.webURL = [NSURL URLWithString:@"http://aceonetechnologies.com/score-alert/"];
            webVC.title = @"Score Alert";
            [self presentViewController:webVC animated:YES completion:nil];
            break;
        }
            
        case 6:
        {
            WebViewController *webVC = [WebViewController new];
            webVC.webURL = [NSURL URLWithString:@"http://aceonetechnologies.com/news"];
            webVC.title = @"News";
            [self presentViewController:webVC animated:YES completion:nil];
            break;
        }
            
        default:
            break;
    }
    
    [_searchBox resignFirstResponder];
}

#pragma mark - Search Method

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    //    [_leftMenu menuCollapse];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    NSLog(@"Start Search");
    NSLog(@"search term %@", searchBar.text);
    
    SearchResultViewController *viewResultFromSearch = [SearchResultViewController new];
    
    //pass values
    viewResultFromSearch.distance = 0;
    viewResultFromSearch.subCategoryName = @"All";
    viewResultFromSearch.subCategoryId = 33;
    viewResultFromSearch.searchText = searchBar.text;
    viewResultFromSearch.mainCategory = @"Shopping";
    
    
    [self presentViewController:viewResultFromSearch animated:YES completion:nil];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

@end
