//
//  WebViewController.h
//  HUB360-NEW
//
//  Created by Oyuka on 10/24/14.
//  Copyright (c) 2014 Oyuka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SideNavigationDelegate.h"

@interface WebViewController : UIViewController<SideNavigationDelegate, UITableViewDataSource, UITableViewDelegate, UIWebViewDelegate, UISearchBarDelegate>

@property (nonatomic, readwrite) NSURL *webURL;
@property (nonatomic, readwrite) NSString *title;

@end
