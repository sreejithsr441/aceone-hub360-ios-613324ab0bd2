//
//  UIImageView.m
//  FSN
//
//  Created by Oyuka on 12/11/13.
//  Copyright (c) 2013 Oyuka. All rights reserved.
//
#import "UIImageView+Network.h"
#import "FTWCache.h"
#import <objc/runtime.h>

static char URL_KEY;


@implementation UIImageView(Network)

@dynamic imageURL;

- (void) loadImageFromURL:(NSURL*)url placeholderImage:(UIImage*)placeholder cachingKey:(NSString*)key frame:(CGRect)frame
{
    
	self.imageURL = url;
	self.image = placeholder;
	
	NSData *cachedData = [FTWCache objectForKey:key];
	if (cachedData) {
        self.imageURL   = nil;
        self.image      = [UIImage imageWithData:cachedData];
        return;
	}
    
	dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
	dispatch_async(queue, ^{
        
		NSData *data = [NSData dataWithContentsOfURL:url];
		
		UIImage *imageFromData = [UIImage imageWithData:data];
		NSLog(@"imageFromData.size: %f, %f ", imageFromData.size.width, imageFromData.size.height);
		[FTWCache setObject:data forKey:key];
//        CGSize size = imageFromData.size;
        
//        if(size.width > width && size.height < height){
//            
//            float newheight;
//            newheight = width * size.height / size.width;
        
            [self setFrame:frame];
//            NSLog(@"a");
//        }
        
//        if(size.width <= width && size.height > height){
//            float newwidth;
//            newwidth = height * size.width / size.height;
//            
//            [self setFrame:CGRectMake((width-newwidth)/2, 0, newwidth, height)];
//            NSLog(@"b");
//        }
//        
//        if(size.width <= width && size.height <= height){
//            [self setFrame:CGRectMake((width-size.width)/2, (height-size.height)/2, size.width, size.height)];
//            NSLog(@"c");
//        }
//        
//        if(size.width >= width && size.height >= height){
//            
//            float  newwidth;
//            newwidth = size.width * height / size.height;
//            
//            [self setFrame:CGRectMake((width-newwidth)/2, 0, newwidth, height)];
//            NSLog(@"d");
//            
//            if(newwidth > width){
//                float  newheight;
//                newheight = size.height * width / size.width;
//                
//                [self setFrame:CGRectMake(0, (height-newheight)/2, width, newheight)];
//                NSLog(@"d.1");
//            }
//        }

        
//        [self setFrame:CGRectMake(0, 0, size.width, size.width)];
        
        
		if (imageFromData) {
			if ([self.imageURL.absoluteString isEqualToString:url.absoluteString]) {
				dispatch_sync(dispatch_get_main_queue(), ^{
					self.image = imageFromData;
				});
			} else {
                //				NSLog(@"urls are not the same, bailing out!");
			}
		}
		self.imageURL = nil;
	});
}

- (void) setImageURL:(NSURL *)newImageURL {
	objc_setAssociatedObject(self, &URL_KEY, newImageURL, OBJC_ASSOCIATION_COPY);
}

- (NSURL*) imageURL {
	return objc_getAssociatedObject(self, &URL_KEY);
}

@end
