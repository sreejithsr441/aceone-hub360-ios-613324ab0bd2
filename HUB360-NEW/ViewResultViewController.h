//
//  ViewResultViewController.h
//  HUB360-NEW
//
//  Created by Oyuka on 1/2/14.
//  Copyright (c) 2014 Oyuka. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "CategoryButton.h"
#import "Carousel.h"
#import "NavigationControllerBuilder.h"
//#import "ChangeCategoryDelegate.h"
#import "SideNavigationDelegate.h"
#import <CoreLocation/CoreLocation.h>

@interface ViewResultViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, UIScrollViewDelegate, SideNavigationDelegate, CLLocationManagerDelegate>
{
    
}

@property float distance;
@property NSString *subCategoryName;
@property int subCategoryId;
@property BOOL showCouponOnly;
@property NSString *mainCategory;

@end
