//
//  AppDelegate.h
//  HUB360-NEW
//
//  Created by Oyuka on 12/18/13.
//  Copyright (c) 2013 Oyuka. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
