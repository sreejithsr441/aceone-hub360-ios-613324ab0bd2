//
//  Splash.h
//  HUB360-NEW
//
//  Created by Oyuka on 12/18/13.
//  Copyright (c) 2013 Oyuka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
@interface Splash : UIViewController
{
    Reachability *reachability;
    Reachability *internetReachable;
  
}
@property(nonatomic,assign) BOOL hasInet;
@end
