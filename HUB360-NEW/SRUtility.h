//
//  SRUtility.h
//  MainImplementation
//
//  Created by Sreejith Rajan on 19/09/14.
//  Copyright (c) 2014 Sreejith Rajan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"
@interface SRUtility : NSObject
+(BOOL)reachable ;
@end
