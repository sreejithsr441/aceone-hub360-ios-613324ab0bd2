//
//  hub360Config.h
//  HUB360-NEW
//
//  Created by Oyuka on 12/29/13.
//  Copyright (c) 2013 Oyuka. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import <Foundation/Foundation.h>

@interface hub360Config : NSObject <CLLocationManagerDelegate>

-(CGFloat)height;

-(CGFloat)width;

-(CLLocationCoordinate2D)getLocation;
@end
