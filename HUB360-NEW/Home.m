//
//  Home.m
//  HUB360-NEW
//
//  Created by Oyuka on 12/18/13.
//  Copyright (c) 2013 Oyuka. All rights reserved.
//

#import "Home.h"
#import "EventsViewController.h"
#import "NearByMeViewController.h"
#import "ByMainCategoryViewController.h"
#import "GalleryViewController.h"
#import "SideNavigationView.h"
#import "hub360Config.h"
#import "MyFavoritesViewController.h"
#import "SearchResultViewController.h"
#import "SearchBox.h"
#import "Reachability.h"
#import "ActivityIndicator.h"
#import "AdvertisementBlock.h"
#import "WebViewController.h"
#import "SRUtility.h"
@interface Home ()

@end

@implementation Home
{
    Carousel *_carouselView;
    SideNavigationView *_sideNav;
    UIView *_mainContainer;
    hub360Config *_config;
    NavigationControllerBuilder *_navigationBar;
    SearchBox *_searchBox;
//    UIActivityIndicatorView *_activityIndicator;
    Reachability *_networkReachability;
    NetworkStatus _networkStatus;
    CLLocationManager *_locationManager;
    ActivityIndicator *_activityIndicator;
    AdvertisementBlock *_advertisementFullPage;
    AdvertisementBlock *_advertisementBanner;
}

-(void)viewDidAppear:(BOOL)animated{
    
    NSUserDefaults *userDefaults=[NSUserDefaults standardUserDefaults];
    NSString *countString = [userDefaults objectForKey:@"saveCount"];
    NSLog(@"Your Count: %@",countString);
    
    [userDefaults setObject:[NSString stringWithFormat:@"%d", [countString intValue]+1] forKey:@"saveCount"];
    [userDefaults synchronize];
    
    if(([countString integerValue]) % 5 == 0)
    {
        _advertisementFullPage = [AdvertisementBlock new];
        [_mainContainer addSubview:_advertisementFullPage];
        [_advertisementFullPage getFullAds];
        [userDefaults setObject:@"1" forKey:@"saveCount"];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
     [self setUpRechability];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    
    internetReachable = [Reachability reachabilityForInternetConnection] ;
    
    [internetReachable startNotifier];
    // location
    //---- For getting current gps location
    _locationManager = [CLLocationManager new];
    _locationManager.delegate = self;
    _locationManager.distanceFilter = 100.0; // Will notify the LocationManager every 100 meters
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    [_locationManager startUpdatingLocation];
    
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0){
        [_locationManager requestAlwaysAuthorization];
    }
    
    float latitude = _locationManager.location.coordinate.latitude;
    float longitude = _locationManager.location.coordinate.longitude;
    
    NSLog(@"%f, %f", latitude, longitude);
    
    // Allocate a reachability object
    _networkReachability = [Reachability reachabilityForInternetConnection];
     _networkStatus = [_networkReachability currentReachabilityStatus];
    
    _config = [hub360Config new];

    // side navigation table view
    
    _sideNav = [[SideNavigationView alloc]initWithDelegate:self frame:CGRectMake(0, 20, [_config width]-60, [_config height])];
    [_sideNav setDataSource:self];
    [self.view addSubview:_sideNav];
    
    // main container
    _mainContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 20, [_config width], [_config height])];
    _mainContainer.backgroundColor =[UIColor colorWithRed:(237.0/255.0) green:(237.0/255.0) blue:(237.0/255.0) alpha:(1)];
    [self.view addSubview:_mainContainer];
    
    // scroll view
    _carouselView = [[Carousel alloc]initWithFrame:CGRectMake(0, 45, [_config width], [_config height])];
    
    [_carouselView setBackgroundColor:[UIColor clearColor]];
    _carouselView.scrollView.showsVerticalScrollIndicator = YES;
    [_carouselView.scrollView setDelegate:self];
    [_carouselView.scrollView setFrame:CGRectMake(0, 0, 320, [_config height])];
    _carouselView.scrollView.clipsToBounds = YES;
    [_mainContainer addSubview:_carouselView];
    [_carouselView.scrollView flashScrollIndicators];

    
    // creating category buttons
    CategoryButton *NearByMeButton = [[CategoryButton alloc]initWithDelegate:self initWithFrame:CGRectMake( 0, 0, 160, 150 )];
    [NearByMeButton setCustomTitle:@"Near By Me" : [UIColor colorWithRed:(245.0/255.0) green:(76.0/255.0) blue:(10.0/255.0) alpha:1]];
    [NearByMeButton setCustomIcon:[UIImage imageNamed:@"nearbyme-icon"]];
    NearByMeButton.tag = 101;
    [_carouselView.scrollView addSubview:NearByMeButton];

    CategoryButton *Shopping = [[CategoryButton alloc]initWithDelegate:self initWithFrame:CGRectMake( 160, 0, 160, 150 )];
    [Shopping setCustomTitle:@"Shopping" : [UIColor colorWithRed:(1.0/255.0) green:(157.0/255.0) blue:(89.0/255.0) alpha:1]];
    [Shopping setCustomIcon:[UIImage imageNamed:@"shopping-icon"]];
    Shopping.tag = 102;
    [_carouselView.scrollView addSubview:Shopping];
    
    CategoryButton *Dining = [[CategoryButton alloc]initWithDelegate:self initWithFrame:CGRectMake( 0, 150, 160, 150 )];
    [Dining setCustomTitle:@"Dining" : [UIColor colorWithRed:(255.0/255.0) green:(181.0/255.0) blue:(2.0/255.0) alpha:1]];
    [Dining setCustomIcon:[UIImage imageNamed:@"dining-icon"]];
    Dining.tag = 103;
    [_carouselView.scrollView addSubview:Dining];
    
    CategoryButton *Services = [[CategoryButton alloc]initWithDelegate:self initWithFrame:CGRectMake( 160, 150, 160, 150 )];
    [Services setCustomTitle:@"Services" : [UIColor colorWithRed:(211.0/255.0) green:(62.0/255.0) blue:(42.0/255.0) alpha:1]];
    [Services setCustomIcon:[UIImage imageNamed:@"settings-icon"]];
    Services.tag = 104;
    [_carouselView.scrollView addSubview:Services];
    
    CategoryButton *Events = [[CategoryButton alloc]initWithDelegate:self initWithFrame:CGRectMake( 0, 300, 160, 150 )];
    [Events setCustomTitle:@"Events": [UIColor colorWithRed:(27.0/255.0) green:(112.0/255.0) blue:(239.0/255.0) alpha:1]];
    [Events setCustomIcon:[UIImage imageNamed:@"events-icon"]];
    Events.tag = 105;
    [_carouselView.scrollView addSubview:Events];
    
    CategoryButton *Gallery = [[CategoryButton alloc]initWithDelegate:self initWithFrame:CGRectMake( 160, 300, 160, 150 )];
    [Gallery setCustomTitle:@"Gallery": [UIColor colorWithRed:(101.0/255.0) green:(30.0/255.0) blue:(0.0/255.0) alpha:1]];
    [Gallery setCustomIcon:[UIImage imageNamed:@"gallery-icon"]];
    Gallery.tag = 106;
    [_carouselView.scrollView addSubview:Gallery];
    
    // adding navigation controller
    _navigationBar = [[NavigationControllerBuilder alloc]initWithFrame:CGRectMake(0, 0, 320, 45)];
    
    [_navigationBar addNavigationContoller:@"Categories" :NO];
    [_mainContainer addSubview:_navigationBar];
    
    _activityIndicator = [ActivityIndicator new];
    [_carouselView.scrollView addSubview:_activityIndicator];
    
    // Banner add
    _advertisementBanner = [AdvertisementBlock new];
    [_mainContainer addSubview:_advertisementBanner];
    [_advertisementBanner getBannerAds];
    
    UIImageView *poweredBy = [[UIImageView alloc]initWithFrame:CGRectMake(120, 460, 80, 35)];
    poweredBy.image = [UIImage imageNamed:@"poweredby@2x"];
    [_carouselView.scrollView addSubview:poweredBy];
    
    if([_config height] < 500){
        [NearByMeButton setFrame:CGRectMake( 0, 0, 160, 121 )];
        [Shopping setFrame:CGRectMake( 160, 0, 160, 121 )];
        [Dining setFrame:CGRectMake( 0, 121, 160, 121 )];
        [Services setFrame:CGRectMake( 160, 121, 160, 121 )];
        [Events setFrame:CGRectMake( 0, 242, 160, 121 )];
        [Gallery setFrame:CGRectMake( 160, 242, 160, 121 )];
        [poweredBy setFrame:CGRectMake(120, 380, 80, 35)];
        [_advertisementBanner setFrame:CGRectMake(0, 410, 320, 50)];
//        _carouselView.scrollView.contentSize = CGSizeMake(0, 570);
    }
    
   

}
#pragma mark
#pragma mark NetworkConnectionChange

-(void)setUpRechability

{   [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNetworkChange:) name:kReachabilityChangedNotification object:nil];
    reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    if          (remoteHostStatus == NotReachable)      {NSLog(@"no");      self.hasInet-=NO;   }
    else if     (remoteHostStatus == ReachableViaWiFi)  {NSLog(@"wifi");    self.hasInet-=YES;  }
    else if     (remoteHostStatus == ReachableViaWWAN)  {NSLog(@"cell");    self.hasInet-=YES;  }
}



- (void) handleNetworkChange:(NSNotification *)notice

{
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    if          (remoteHostStatus == NotReachable)      {NSLog(@"no");      self.hasInet-=NO;   }
    else if     (remoteHostStatus == ReachableViaWiFi)  {NSLog(@"wifi");    self.hasInet-=YES;  }
    else if     (remoteHostStatus == ReachableViaWWAN)  {NSLog(@"cell");    self.hasInet-=YES;  }
    
}

- (void)checkNetworkStatus:(NSNotification *)notice
{
    NetworkStatus internetStatus = [internetReachable currentReachabilityStatus];
    switch (internetStatus)
    {  case NotReachable:
        {
            NSLog(@"The internet is down.");
            UIAlertView *alret=[[UIAlertView alloc]initWithTitle:@"Network Error" message:@"Your Network Is Disconnected. Please Check Your Network Connection And Try Again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alret show];
            
        }
            break;
        case ReachableViaWiFi:
        {
            NSLog(@"The internet is working via WIFI");
           
            
            
        }
            break;
            
        case ReachableViaWWAN:
        {
            NSLog(@"The internet is working via WWAN!");
           
            
        }
            
            break;
            
    }
    
}
#pragma mark 
#pragma mark markNavigateButton
-(void)categoryButtonTappedWithTag:(int)Tag
{
    
    [NSThread detachNewThreadSelector:@selector(startActivityIndicator) toTarget:_activityIndicator withObject:nil];
    
    if(![SRUtility reachable])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Network Error" message:@"No internet connection available. Please check your connection." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        
        switch (Tag) {
            case 101:   // Near By Me
            {
                NearByMeViewController *nearByMePage = [NearByMeViewController new];
                [self presentViewController:nearByMePage animated:NO completion:nil];
            }
                break;
                
            case 102:   // Shopping
            {
                ByMainCategoryViewController *byShopping = [ByMainCategoryViewController new];
                byShopping.mainCategory = @"Shopping";
                byShopping.mainCategoryID = 5;
                [self presentViewController:byShopping animated:NO completion:nil];
            }
                break;
                
            case 103:   // Dining
            {
                ByMainCategoryViewController *byDining = [ByMainCategoryViewController new];
                byDining.mainCategory = @"Dining";
                byDining.mainCategoryID = 1;
                [self presentViewController:byDining animated:NO completion:nil];
            }
                break;
                
            case 104:   // Services
            {
                ByMainCategoryViewController *byServices = [ByMainCategoryViewController new];
                byServices.mainCategory = @"Services";
                byServices.mainCategoryID = 2;
                [self presentViewController:byServices animated:NO completion:nil];
            }
                break;
                
            case 105:   // Events
            {
                EventsViewController *eventPage = [EventsViewController new];
                [self presentViewController:eventPage animated:YES completion:nil];
            }
                break;
                
            case 106:   // Gallery
            {
                GalleryViewController *galleryPage = [GalleryViewController new];
                [self presentViewController:galleryPage animated:YES completion:nil];
            }
                break;
                
            default:
                break;
        }

    };
    
    [NSThread detachNewThreadSelector:@selector(stopActivityIndicator) toTarget:_activityIndicator withObject:nil];
//        [self stopActivityIndicator];

}

#pragma mark - TableView Delegate Methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_sideNav menucount];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *list = [[NSArray alloc]initWithArray:[_sideNav listMenuItems]];
    NSArray *iconList = [[NSArray alloc]initWithArray:[_sideNav listMenuItemIcons]];
    
    UITableViewCell *cell;
   
    if(indexPath.row == 0){
        cell = [tableView dequeueReusableCellWithIdentifier:@"searchBox"];
    }else{
        cell = [tableView dequeueReusableCellWithIdentifier:[list objectAtIndex:indexPath.row-1]];
    }
    
    
    if (cell == nil){
        
        if(indexPath.row == 0){
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"searchBox"];
            cell.backgroundColor = [UIColor clearColor];

             _searchBox = [[SearchBox alloc]initWithFrame:CGRectMake(0, 0, cell.frame.size.width-60, cell.frame.size.height)];
            _searchBox.delegate = self;
            [cell.contentView addSubview:_searchBox];
            
        }else{
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:[list objectAtIndex:indexPath.row-1]];
            
            cell.textLabel.text = [list objectAtIndex:indexPath.row-1];
            [cell.textLabel setTextColor:[UIColor whiteColor]];
            cell.backgroundColor = [UIColor clearColor];
            cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14.0];
            
            UIImageView *accessoryImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 12.5, 19)];
            [accessoryImage setImage:[UIImage imageNamed:@"menu_cell_indicator"]];
            cell.accessoryView = accessoryImage;
            [cell.imageView setImage:[UIImage imageNamed:[iconList objectAtIndex:indexPath.row-1]]];
        }
            
        
        
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([SRUtility reachable])
    {
        if(indexPath.row > 0){
            [self selectMenuItem:indexPath.row];
            NSLog(@"Menu item selected");
        }
    }
    else
    {
        UIAlertView *alret=[[UIAlertView alloc]initWithTitle:@"Network Error" message:@"Your Network Is Disconnected. Please Check Your Network Connection And Try Again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alret show];
    }
    
}

#pragma mark - Helper Methods

-(void)selectMenuItem:(NSInteger)index{
    [_navigationBar showLeftNav];
    
    // change view controller
    
    switch (index) {
        case 1:
        {
            break;
        }
           
            
        case 2:
        {
            MyFavoritesViewController *myFavoritesPage = [MyFavoritesViewController new];
            [self presentViewController:myFavoritesPage animated:YES completion:nil];
             break;
        }
            
        case 3:
        {
            WebViewController *webVC = [WebViewController new];
            webVC.webURL = [NSURL URLWithString:@"http://aceonetechnologies.com/asu-game-schedule/"];
            webVC.title = @"ASU Game Schedule";
            [self presentViewController:webVC animated:YES completion:nil];
             break;
        }
            
        case 4:
        {
            WebViewController *webVC = [WebViewController new];
            webVC.webURL = [NSURL URLWithString:@"http://aceonetechnologies.com/friday-night-lights/"];
            webVC.title = @"High School Game Schedule";
            [self presentViewController:webVC animated:YES completion:nil];
             break;
        }
            
        case 5:
        {
            WebViewController *webVC = [WebViewController new];
            webVC.webURL = [NSURL URLWithString:@"http://aceonetechnologies.com/score-alert/"];
            webVC.title = @"Score Alert";
            [self presentViewController:webVC animated:YES completion:nil];
             break;
        }
            
            
        case 6:
        {
            WebViewController *webVC = [WebViewController new];
            webVC.webURL = [NSURL URLWithString:@"http://aceonetechnologies.com/news"];
            webVC.title = @"News";
            [self presentViewController:webVC animated:YES completion:nil];
            break;
        }
            
        default:
            break;
    }
    
}


#pragma mark - Search Method

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
//    [_leftMenu menuCollapse];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    NSLog(@"Start Search");
    NSLog(@"search term %@", searchBar.text);
    
    SearchResultViewController *viewResultFromSearch = [SearchResultViewController new];
    
    //pass values
    viewResultFromSearch.distance = 0;
    viewResultFromSearch.subCategoryName = @"All";
    viewResultFromSearch.subCategoryId = 33;
    viewResultFromSearch.searchText = searchBar.text;
    viewResultFromSearch.mainCategory = @"Shopping";

    
    [self presentViewController:viewResultFromSearch animated:YES completion:nil];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

@end
