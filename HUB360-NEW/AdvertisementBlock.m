//
//  AdvertisementBlock.m
//  HUB360-NEW
//
//  Created by Oyuka on 1/16/14.
//  Copyright (c) 2014 Oyuka. All rights reserved.
//

#import "AdvertisementBlock.h"
#import "UIImageView+Network.h"

@implementation AdvertisementBlock{
    UIImageView *_adSample;
    NSArray *_adsImagesArray;
    NSArray *_bannerImagesArray;
    UIImageView *_bannerImagesView;
    NSMutableArray *_chosenNumbersBanner;
    NSMutableArray *_chosenNumbersFullAdd;
    NSMutableArray *_chosenNumbersHalfAdd;
    
    NSTimer *_adsTimer;
}

#define IS_IPHONE_5 (((double)[[UIScreen mainScreen] bounds].size.height) == ((double)568))

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor blackColor];
        _chosenNumbersBanner = [[NSMutableArray alloc] init];
        _chosenNumbersFullAdd = [[NSMutableArray alloc] init];
        _chosenNumbersHalfAdd = [[NSMutableArray alloc] init];
    }
    return self;
}

-(void)addCloseButton
{
    
//    UIButton *closeButton = [[UIButton alloc]initWithFrame:CGRectMake(262.5, 5, 47.5, 58.5)];
//    [closeButton setBackgroundImage:[UIImage imageNamed:@"close-ad@2x"] forState:UIControlStateNormal];
//    [closeButton addTarget:self action:@selector(closeAdvertisement) forControlEvents:UIControlEventTouchUpInside];
//    [self addSubview:closeButton];

}

-(void)closeAdvertisement{
    [self removeFromSuperview];
    [_adsTimer invalidate];
}

- (int) generateRandomNumber: (int)range{
    
    int number = arc4random() % range;
    
    if([_chosenNumbersBanner count] == range)
        [_chosenNumbersBanner removeAllObjects];
        
    if ([_chosenNumbersBanner indexOfObject:[NSNumber numberWithInt:number]] != NSNotFound)
        number = [self generateRandomNumber:range];
    else
        [_chosenNumbersBanner addObject:[NSNumber numberWithInt:number]];
    return number;
    
}

- (int)generateRandomNumberForAds: (int)range{
    
    int number = arc4random() % range;
    
    if([_chosenNumbersFullAdd count] == range)
        [_chosenNumbersFullAdd removeAllObjects];
    
    if ([_chosenNumbersFullAdd indexOfObject:[NSNumber numberWithInt:number]] != NSNotFound)
        number = [self generateRandomNumberForAds:range];
    else
        [_chosenNumbersFullAdd addObject:[NSNumber numberWithInt:number]];
    return number;
    
}

#pragma mark - Rotate Banner Images
-(void)rotateBannerImage:(NSTimer *)sender
{
    
    int index = [self generateRandomNumber:[_bannerImagesArray count]];
    
    _bannerImagesView.alpha = 0.0;
    
    
    NSString *tmpimageurl = [[_bannerImagesArray objectAtIndex:index] objectForKey:@"ImageURL"];
    NSArray *cachingkey = [tmpimageurl componentsSeparatedByString:@"/"];
    
    [UIView animateWithDuration:0.5 animations:^{
        _bannerImagesView.alpha = 1.5;
        [_bannerImagesView loadImageFromURL:[NSURL URLWithString:[[_bannerImagesArray objectAtIndex:index] objectForKey:@"ImageURL"]]
                   placeholderImage:[UIImage imageNamed:@""]
                         cachingKey:[cachingkey lastObject]
                              frame:_bannerImagesView.frame];
        
    }];
    
    [NSTimer scheduledTimerWithTimeInterval: 5.0 target: self
                                   selector: @selector(rotateBannerImage:)
                                   userInfo: [NSNumber numberWithInt:index+1]
                                    repeats: NO];
}

#pragma mark - Rotate Ads Images
-(void)rotateImage:(NSTimer *)sender
{
    
    int index = [self generateRandomNumberForAds:[_adsImagesArray count]];
    NSLog(@"Array of numbers chosen %@", _chosenNumbersFullAdd);
    
    if([[[_adsImagesArray objectAtIndex:index] objectForKey:@"AdType"] isEqualToString:@"1"]){
        
        _adSample.frame = CGRectMake(10, 67, 300, 250);
        [self setFrame:CGRectMake(0, 115, 320, 337)];
        
    }else{
        [self setFrame:CGRectMake(0, 0, 320, 548)];
        _adSample.frame = CGRectMake(10, 67, 300, 470);
            
            if(!IS_IPHONE_5){
                [_adSample setFrame:CGRectMake(10, 67, 300, 390)];
            }

    }
    
    _adSample.alpha = 0.0;
    
    NSString *tmpimageurl = [[_adsImagesArray objectAtIndex:index] objectForKey:@"ImageURL"];
    NSArray *cachingkey = [tmpimageurl componentsSeparatedByString:@"/"];
    
    [UIView animateWithDuration:0.5 animations:^{
        _adSample.alpha = 1.5;
        [_adSample loadImageFromURL:[NSURL URLWithString:[[_adsImagesArray objectAtIndex:index] objectForKey:@"ImageURL"]]
                   placeholderImage:[UIImage imageNamed:@""]
                         cachingKey:[cachingkey lastObject]
                              frame:_adSample.frame];

    }];
    
    _adsTimer = [NSTimer scheduledTimerWithTimeInterval: 5.0 target: self
                                   selector: @selector(rotateImage:)
                                   userInfo: [NSNumber numberWithInt:index+1]
                                    repeats: NO];
}

#pragma mark - Full+Half
-(void)getFullAndHalfAds
{
    
//    NSString *sopMessage=[NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
//                          "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
//                          "<soap:Body>"
//                          "<GetFullAndHalfAds xmlns=\"http://www.hub360.com/\">"
//                          "</GetFullAndHalfAds>"
//                          "</soap:Body>"
//                          "</soap:Envelope>"];
//    
//    NSLog(@"value is%@",sopMessage);
//    
//    NSURL *url=[NSURL URLWithString:@"http://webdesignjonesboro.com/hub360service/hub360Service.asmx/GetFullAndHalfAds"];
//    NSMutableURLRequest *req=[NSMutableURLRequest requestWithURL:url];
//    NSString *msglength=[NSString stringWithFormat:@"%lu",(unsigned long)[sopMessage length]];
//    [req addValue:@"text/xml;charset=utf-8"  forHTTPHeaderField:@"Content-Type"];
//    [req addValue:@"http://webdesignjonesboro.com/hub360service/hub360Service.asmx" forHTTPHeaderField:@"SOAPAction"];
//    
//    [req addValue: msglength forHTTPHeaderField:@"Content-Length"];
//    [req setHTTPMethod:@"POST"];
//    [req setHTTPBody:[sopMessage dataUsingEncoding:NSUTF8StringEncoding]];
//    
//    NSError *error1;
//    error1 = [NSError new];
//    
//    NSData *returnData1 = [NSURLConnection sendSynchronousRequest:req returningResponse:nil error:&error1];
//    NSString *returnString = [[NSString alloc] initWithData:returnData1 encoding:NSUTF8StringEncoding];
//    NSLog(@"return string %@",returnString);
//    
//    NSRange start;
//    NSRange stop;
//    
//    start = [returnString rangeOfString:@"<string xmlns=\"http://www.hub360nea.com/\">"];
//    stop = [returnString rangeOfString: @"</string>"];
//    
//    unsigned long start1 = start.location+start.length;
//    unsigned long start2 = stop.location-start1;
//    
//    NSString *jsonString = [returnString substringWithRange: NSMakeRange (start1, start2)];
//    
//    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
//    NSError *e = nil;
//    NSMutableArray *json = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&e];
//    NSLog(@"%@", json);
//    
//    _adsImagesArray = [json valueForKey:@"List`1"];
//    
//    
//    // create the add image
//    [self setFrame:CGRectMake(0, 0, 320, 548)];
//    self.alpha = 0.0;
//    
//    if([_adsImagesArray count]>0){
//        
//        int randomIndex = [self generateRandomNumberForAds:[_adsImagesArray count]];
//        [_chosenNumbersFullAdd addObject:[NSNumber numberWithInt:randomIndex]];
//        
//        NSString *url = [[_adsImagesArray objectAtIndex:randomIndex] objectForKey:@"ImageURL"];
//        _adSample = [[UIImageView alloc]initWithFrame:CGRectMake(10, 67, 300, 470)];
//        
//        if(!IS_IPHONE_5){
//            [_adSample setFrame:CGRectMake(10, 67, 300, 390)];
//        }
//        
//        NSString *tmpimageurl = url;
//        NSArray *cachingkey = [tmpimageurl componentsSeparatedByString:@"/"];
//        
//        [_adSample loadImageFromURL:[NSURL URLWithString:url]
//                   placeholderImage:[UIImage imageNamed:@""]
//                         cachingKey:[cachingkey lastObject]
//                              frame:_adSample.frame];
//        
//        if([[[_adsImagesArray objectAtIndex:randomIndex] objectForKey:@"AdType"] isEqualToString:@"1"]){
//            
//            _adSample.frame = CGRectMake(10, 67, 300, 250);
//            [self setFrame:CGRectMake(0, 115, 320, 337)];
//            
//        }else{
//            [self setFrame:CGRectMake(0, 0, 320, 548)];
//            _adSample.frame = CGRectMake(10, 67, 300, 470);
//            
//            if(!IS_IPHONE_5){
//                [_adSample setFrame:CGRectMake(10, 67, 300, 390)];
//            }
//            
//        }
//        
//        [self addSubview:_adSample];
//        
//        
//        [UIView animateWithDuration:0.5 animations:^{
//            self.alpha = 1.5;
//        }];
//        
//        if([_adsImagesArray count]>1){
//            _adsTimer = [NSTimer scheduledTimerWithTimeInterval: 5.0 target: self
//                                                       selector: @selector(rotateImage:)
//                                                       userInfo: [NSNumber numberWithInt:1]
//                                                        repeats: NO];
//        }
//        
//    }
//    
//    [self addCloseButton];

}


#pragma mark - getFullAds
-(void)getFullAds
{
    
//    NSString *sopMessage=[NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
//                          "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
//                          "<soap:Body>"
//                          "<GetFullAds xmlns=\"http://www.hub360.com/\">"
//                          "</GetFullAds>"
//                          "</soap:Body>"
//                          "</soap:Envelope>"];
//    
//    NSLog(@"value is%@",sopMessage);
//    
//    NSURL *url=[NSURL URLWithString:@"http://webdesignjonesboro.com/hub360service/hub360Service.asmx/GetFullAds"];
//    NSMutableURLRequest *req=[NSMutableURLRequest requestWithURL:url];
//    NSString *msglength=[NSString stringWithFormat:@"%lu",(unsigned long)[sopMessage length]];
//    [req addValue:@"text/xml;charset=utf-8"  forHTTPHeaderField:@"Content-Type"];
//    [req addValue:@"http://webdesignjonesboro.com/hub360service/hub360Service.asmx" forHTTPHeaderField:@"SOAPAction"];
//    
//    [req addValue: msglength forHTTPHeaderField:@"Content-Length"];
//    [req setHTTPMethod:@"POST"];
//    [req setHTTPBody:[sopMessage dataUsingEncoding:NSUTF8StringEncoding]];
//    
//    NSError *error1;
//    error1 = [NSError new];
//    
//    NSData *returnData1 = [NSURLConnection sendSynchronousRequest:req returningResponse:nil error:&error1];
//    NSUInteger DataLength=[returnData1 length];
//    if (DataLength==0)
//    {
//        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Data Missing" message:@"There is no ad to display" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//        [alert show];
//        [self addCloseButton];
//        
//    }
//    else
//    {
//    
//    NSString *returnString = [[NSString alloc] initWithData:returnData1 encoding:NSUTF8StringEncoding];
//    NSLog(@"%@",returnString);
//    
//    NSRange start;
//    NSRange stop;
//    
//    start = [returnString rangeOfString:@"<string xmlns=\"http://www.hub360nea.com/\">"];
//    stop = [returnString rangeOfString: @"</string>"];
//    
//    unsigned long start1 = start.location+start.length;
//    unsigned long start2 = stop.location-start1;
//    
//    
//    NSString *jsonString = [returnString substringWithRange: NSMakeRange (start1, start2)];
//    
//    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
//    NSError *e = nil;
//    NSMutableArray *json = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&e];
//    NSLog(@"%@", json);
//    
//    _adsImagesArray = [json valueForKey:@"List`1"];
//    
//    
//    // create the add image
//    [self setFrame:CGRectMake(0, 0, 320, 548)];
//    self.alpha = 0.0;
//    
//    if([_adsImagesArray count]>0){
//        
//        int randomIndex = [self generateRandomNumberForAds:[_adsImagesArray count]];
//        [_chosenNumbersFullAdd addObject:[NSNumber numberWithInt:randomIndex]];
//        
//        NSString *url = [[_adsImagesArray objectAtIndex:randomIndex] objectForKey:@"ImageURL"];
//        _adSample = [[UIImageView alloc]initWithFrame:CGRectMake(10, 67, 300, 470)];
//        
//        if(!IS_IPHONE_5){
//            [_adSample setFrame:CGRectMake(10, 67, 300, 390)];
//        }
//        
//        NSString *tmpimageurl = url;
//        NSArray *cachingkey = [tmpimageurl componentsSeparatedByString:@"/"];
//        
//        [_adSample loadImageFromURL:[NSURL URLWithString:url]
//                   placeholderImage:[UIImage imageNamed:@""]
//                         cachingKey:[cachingkey lastObject]
//                              frame:_adSample.frame];
//        
//        [_adSample setBackgroundColor:[UIColor grayColor]];
//        [self addSubview:_adSample];
//        
//        [UIView animateWithDuration:0.5 animations:^{
//            self.alpha = 1.5;
//        }];
//        
//        if([_adsImagesArray count]>1){
//            _adsTimer = [NSTimer scheduledTimerWithTimeInterval: 5.0 target: self
//                                                       selector: @selector(rotateImage:)
//                                                       userInfo: [NSNumber numberWithInt:1]
//                                                        repeats: NO];
//        }
//        
//    }
//    
//    [self addCloseButton];
//    }
}


#pragma mark - getHalfAds

-(void)getHalfAds
{
    
//    NSString *sopMessage=[NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
//                          "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
//                          "<soap:Body>"
//                          "<GetHalfAds xmlns=\"http://www.hub360.com/\">"
//                          "</GetHalfAds>"
//                          "</soap:Body>"
//                          "</soap:Envelope>"];
//    
//    NSLog(@"value is%@",sopMessage);
//	
//	NSURL *url=[NSURL URLWithString:@"http://webdesignjonesboro.com/hub360service/hub360Service.asmx/GetHalfAds"];
//	NSMutableURLRequest *req=[NSMutableURLRequest requestWithURL:url];
//	NSString *msglength=[NSString stringWithFormat:@"%lu",(unsigned long)[sopMessage length]];
//	[req addValue:@"text/xml;charset=utf-8"  forHTTPHeaderField:@"Content-Type"];
//	[req addValue:@"http://webdesignjonesboro.com/hub360service/hub360Service.asmx" forHTTPHeaderField:@"SOAPAction"];
//	
//	[req addValue: msglength forHTTPHeaderField:@"Content-Length"];
//	[req setHTTPMethod:@"POST"];
//	[req setHTTPBody:[sopMessage dataUsingEncoding:NSUTF8StringEncoding]];
//	
//	NSError *error1;
//	error1 = [NSError new];
//	
//	NSData *returnData1 = [NSURLConnection sendSynchronousRequest:req returningResponse:nil error:&error1];
//    NSUInteger DataLength=[returnData1 length];
//    if (DataLength==0)
//    {
//        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Data Missing" message:@"There is no ad to display" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//        [alert show];
//        [self addCloseButton];
//        
//    }
//    else
//    {
//	   NSString *returnString = [[NSString alloc] initWithData:returnData1 encoding:NSUTF8StringEncoding];
//	   NSLog(@"return string %@",returnString);
//    
//       NSRange start;
//       NSRange stop;
//    
//      start = [returnString rangeOfString:@"<string xmlns=\"http://www.hub360nea.com/\">"];
//      stop = [returnString rangeOfString: @"</string>"];
//    
//      unsigned long start1 = start.location+start.length;
//      unsigned long start2 = stop.location-start1;
//    
//    
//      NSString *jsonString = [returnString substringWithRange: NSMakeRange (start1, start2)];
//    
//      NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
//      NSError *e = nil;
//      NSMutableArray *json = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&e];
//      NSLog(@"%@", json);
//    
//      _adsImagesArray = [json valueForKey:@"List`1"];
//    
//
//      // create the add image
//      [self setFrame:CGRectMake(0, 115, 320, 337)];
//       self.alpha = 0.0;
//    
//      if([_adsImagesArray count]>0)
//      {
//
//        int randomIndex = [self generateRandomNumberForAds:[_adsImagesArray count]];
//        [_chosenNumbersFullAdd addObject:[NSNumber numberWithInt:randomIndex]];
//        
//        NSString *url = [[_adsImagesArray objectAtIndex:randomIndex] objectForKey:@"ImageURL"];
//        _adSample = [[UIImageView alloc]initWithFrame:CGRectMake(10, 67, 300, 250)];
//        
//        NSString *tmpimageurl = url;
//        NSArray *cachingkey = [tmpimageurl componentsSeparatedByString:@"/"];
//        
//        [_adSample loadImageFromURL:[NSURL URLWithString:url]
//                   placeholderImage:[UIImage imageNamed:@""]
//                         cachingKey:[cachingkey lastObject]
//                              frame:_adSample.frame];
//        
//        [_adSample setBackgroundColor:[UIColor grayColor]];
//        [self addSubview:_adSample];
//        
//        [UIView animateWithDuration:0.5 animations:^{
//            self.alpha = 1.5;
//        }];
//        
//        
//        if([_adsImagesArray count]>1){
//            _adsTimer = [NSTimer scheduledTimerWithTimeInterval: 5.0 target: self
//                                                       selector: @selector(rotateImage:)
//                                                       userInfo: [NSNumber numberWithInt:1]
//                                                        repeats: NO];
//        }
//      }
//
    [self addCloseButton];
//    }
}

#pragma mark: getBannerAds

-(void)getBannerAds
{
    
    NSString *sopMessage=[NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                          "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                          "<soap:Body>"
                          "<GetBannerAds xmlns=\"http://www.hub360.com/\">"
                          "</GetBannerAds>"
                          "</soap:Body>"
                          "</soap:Envelope>"];
    
    NSLog(@"value is%@",sopMessage);
	
	NSURL *url=[NSURL URLWithString:@"http://webdesignjonesboro.com/hub360service/hub360Service.asmx/GetBannerAds"];
	NSMutableURLRequest *req=[NSMutableURLRequest requestWithURL:url];
	NSString *msglength=[NSString stringWithFormat:@"%lu",(unsigned long)[sopMessage length]];
	[req addValue:@"text/xml;charset=utf-8"  forHTTPHeaderField:@"Content-Type"];
	[req addValue:@"http://webdesignjonesboro.com/hub360service/hub360Service.asmx" forHTTPHeaderField:@"SOAPAction"];
	
	[req addValue: msglength forHTTPHeaderField:@"Content-Length"];
	[req setHTTPMethod:@"POST"];
	[req setHTTPBody:[sopMessage dataUsingEncoding:NSUTF8StringEncoding]];
	
	NSError *error1;
	error1 = [NSError new];
	
	NSData *returnData1 = [NSURLConnection sendSynchronousRequest:req returningResponse:nil error:&error1];
    NSUInteger DataLength=[returnData1 length];
    if (!DataLength==0)
    {
        NSString *returnString = [[NSString alloc] initWithData:returnData1 encoding:NSUTF8StringEncoding];
        NSLog(@"%@",returnString);
        
        NSRange start;
        NSRange stop;
        
        start = [returnString rangeOfString:@"<string xmlns=\"http://www.hub360nea.com/\">"];
        stop = [returnString rangeOfString: @"</string>"];
        
        unsigned long start1 = start.location+start.length;
        unsigned long start2 = stop.location-start1;
        
        
        NSString *jsonString = [returnString substringWithRange: NSMakeRange (start1, start2)];
        
        NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        NSError *e = nil;
        NSMutableArray *json = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&e];
        NSLog(@"%@", json);
        
        _bannerImagesArray = [json valueForKey:@"List`1"];
        
        
        // create the add image
        [self setFrame:CGRectMake(0, 494, 320, 56)];
        self.alpha = 0.0;
        
        if([_bannerImagesArray count]>0){
            
            
            int randomIndex = [self generateRandomNumberForAds:[_bannerImagesArray count]];
            [_chosenNumbersBanner addObject:[NSNumber numberWithInt:randomIndex]];
            
            _bannerImagesView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
            NSString *url = [[_adsImagesArray objectAtIndex:randomIndex] objectForKey:@"ImageURL"];
            
            NSString *tmpimageurl = url;
            NSArray *cachingkey = [tmpimageurl componentsSeparatedByString:@"/"];
            
            [_bannerImagesView loadImageFromURL:[NSURL URLWithString:url]
                               placeholderImage:[UIImage imageNamed:@""]
                                     cachingKey:[cachingkey lastObject]
                                          frame:_bannerImagesView.frame];
            
            
            [_bannerImagesView setBackgroundColor:[UIColor whiteColor]];
            [self addSubview:_bannerImagesView];
            
            [UIView animateWithDuration:0.5 animations:^{
                self.alpha = 1.5;
            }];
            
            
            [NSTimer scheduledTimerWithTimeInterval: 0 target: self
                                           selector: @selector(rotateBannerImage:)
                                           userInfo: [NSNumber numberWithInt:1]
                                            repeats: NO];        
            
        }

    }
    else
    {
    
        UIAlertView *alret=[[UIAlertView alloc]initWithTitle:@"Network Error" message:@"Your Network Is Disconnected. Please Check Your Network Connection And Try Again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alret show];
    }
    
    
}
@end
