//
//  Home.h
//  HUB360-NEW
//
//  Created by Oyuka on 12/18/13.
//  Copyright (c) 2013 Oyuka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CategoryButton.h"
#import "Carousel.h"
#import "NavigationControllerBuilder.h"
#import "ChangeCategoryDelegate.h"
#import "SideNavigationDelegate.h"
#import <CoreLocation/CoreLocation.h>
#import "Reachability.h"
@interface Home : UIViewController <UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, UIScrollViewDelegate, ChangeCategoryDelegate, SideNavigationDelegate, UISearchBarDelegate, CLLocationManagerDelegate>
{

    Reachability *reachability;
    Reachability *internetReachable;
}
@property(nonatomic,assign) BOOL hasInet;
@end
