//
//  Carousel.m
//  FlowerShopNetwork
//
//  Created by Oyuka on 8/23/13.
//  Copyright (c) 2013 Oyuka. All rights reserved.
//

#import "Carousel.h"

@implementation Carousel
{
    UIImageView *_item;
    NSInteger _tag;
    
}
@synthesize scrollView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    
        scrollView = [[UIScrollView alloc]initWithFrame:frame];        
//        [scrollView setBackgroundColor:[UIColor greenColor]];
        [self addSubview:scrollView];
    }
    return self;
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    CGPoint touchedLocation = [[touches anyObject] locationInView:self.scrollView];
	NSLog(@"%f,%f",touchedLocation.x,touchedLocation.y);
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
