//
//  NearByMeViewController.h
//  HUB360-NEW
//
//  Created by Oyuka on 12/27/13.
//  Copyright (c) 2013 Oyuka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SideNavigationDelegate.h"
#import "UICustomSlider.h"
@interface NearByMeViewController : UIViewController <SideNavigationDelegate, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, UISearchBarDelegate>
{
     UICustomSlider * slider;
}
@end
