//
//  SideNavigationDelegate.h
//  HUB360-NEW
//
//  Created by Oyuka on 12/29/13.
//  Copyright (c) 2013 Oyuka. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SideNavigationDelegate <NSObject>

-(void)selectMenuItem:(NSInteger)index;

@end
