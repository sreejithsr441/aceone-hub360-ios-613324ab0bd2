//
//  Splash.m
//  HUB360-NEW
//
//  Created by Oyuka on 12/18/13.
//  Copyright (c) 2013 Oyuka. All rights reserved.
//

#import "Splash.h"
#import "Home.h"
#import "SRUtility.h"
@interface Splash ()

@end

@implementation Splash{
    NSUserDefaults *userDefaultsAppLoadCount;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self setUpRechability];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    
    internetReachable = [Reachability reachabilityForInternetConnection] ;
    
    [internetReachable startNotifier];
    userDefaultsAppLoadCount = [NSUserDefaults standardUserDefaults];
    [userDefaultsAppLoadCount setObject:@"1" forKey:@"saveCount"];
    [userDefaultsAppLoadCount synchronize];
    
    [self.view setBackgroundColor:[UIColor whiteColor]];

    
    CGRect screenBound= [[UIScreen mainScreen] bounds];
    CGFloat screenHeight = screenBound.size.height;
    
    UIImageView *logoImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"retina"]];
    [logoImageView setFrame:CGRectMake(0, 0, 320, 480)];
    
    NSLog(@"height: %f", screenHeight);
    
    if(screenHeight > 480.0){
        logoImageView.image = [UIImage imageNamed:@"Default-568h"];
        [logoImageView setFrame:CGRectMake(0, 0, 320, 568)];
    }

//    UIImageView *jonesborocomlogo = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"JonesboroOCLogo"]];
//    [jonesborocomlogo setFrame:CGRectMake(119, 200, 82, 84)];
    [self.view addSubview:logoImageView];
//    [self.view addSubview:jonesborocomlogo];
    
    // ~~~~~~~~~~~~~~~~~~~~~ open homePage
    
   
    [NSTimer scheduledTimerWithTimeInterval:1//change to 5 later
                                     target:self
                                   selector:@selector(openHomePage)
                                   userInfo:nil
                                    repeats:NO];
}
#pragma mark
#pragma mark NetworkConnectionChange

-(void)setUpRechability

{   [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNetworkChange:) name:kReachabilityChangedNotification object:nil];
    reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    if          (remoteHostStatus == NotReachable)      {NSLog(@"no");      self.hasInet-=NO;   }
    else if     (remoteHostStatus == ReachableViaWiFi)  {NSLog(@"wifi");    self.hasInet-=YES;  }
    else if     (remoteHostStatus == ReachableViaWWAN)  {NSLog(@"cell");    self.hasInet-=YES;  }
}



- (void) handleNetworkChange:(NSNotification *)notice

{
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    if          (remoteHostStatus == NotReachable)      {NSLog(@"no");      self.hasInet-=NO;   }
    else if     (remoteHostStatus == ReachableViaWiFi)  {NSLog(@"wifi");    self.hasInet-=YES;  }
    else if     (remoteHostStatus == ReachableViaWWAN)  {NSLog(@"cell");    self.hasInet-=YES;  }
    
}

- (void)checkNetworkStatus:(NSNotification *)notice
{
    NetworkStatus internetStatus = [internetReachable currentReachabilityStatus];
    switch (internetStatus)
    {  case NotReachable:
        {
            NSLog(@"The internet is down.");
            UIAlertView *alret=[[UIAlertView alloc]initWithTitle:@"Network Error" message:@"Your Network Is Disconnected. Please Check Your Network Connection And Try Again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alret show];
            
        }
            break;
        case ReachableViaWiFi:
        {
            NSLog(@"The internet is working via WIFI");
            [NSTimer scheduledTimerWithTimeInterval:1//change to 5 later
        target:self
        selector:@selector(openHomePage)
        userInfo:nil
        repeats:NO];
            
            
        }
            break;
            
        case ReachableViaWWAN:
        {
            NSLog(@"The internet is working via WWAN!");
            [NSTimer scheduledTimerWithTimeInterval:1//change to 5 later
                                             target:self
                                           selector:@selector(openHomePage)
                                           userInfo:nil
                                            repeats:NO];
            
        }
            
            break;
            
    }
    
}

-(void)openHomePage
{
    if ([SRUtility reachable])
    {
        Home *homePage = [Home new];
        [self presentViewController:homePage animated:YES completion:nil];
   
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Network Error" message:@"No internet connection available. Please check your connection." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
