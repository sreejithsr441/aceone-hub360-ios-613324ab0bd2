//
//  hub360Config.m
//  HUB360-NEW
//
//  Created by Oyuka on 12/29/13.
//  Copyright (c) 2013 Oyuka. All rights reserved.
//

#import "hub360Config.h"

@implementation hub360Config


-(CGFloat)height
{
    return [UIScreen mainScreen].bounds.size.height;
}

-(CGFloat)width
{
    return [UIScreen mainScreen].bounds.size.width;
}

-(CLLocationCoordinate2D) getLocation{
    
    
    CLLocationManager *locationManager = [[CLLocationManager alloc] init];
    
    // location
    //---- For getting current gps location
//    locationManager = [CLLocationManager new];
    locationManager.delegate = self;
    locationManager.distanceFilter = 100.0; // Will notify the LocationManager every 100 meters
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    [locationManager startUpdatingLocation];
    
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0){
        [locationManager requestAlwaysAuthorization];
    }

    
//    locationManager.delegate = self;
//    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
//    locationManager.distanceFilter = kCLDistanceFilterNone;
//    [locationManager startUpdatingLocation];
    CLLocation *location = [locationManager location];
    CLLocationCoordinate2D coordinate = [location coordinate];
    
    return coordinate;
}

@end
