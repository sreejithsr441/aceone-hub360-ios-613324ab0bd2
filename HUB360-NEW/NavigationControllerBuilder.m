//  NavigationControllerBuilder.m
//  HUB360-NEW
//
//  Created by Oyuka on 12/19/13.
//  Copyright (c) 2013 Oyuka. All rights reserved.
//

#import "NavigationControllerBuilder.h"

@implementation NavigationControllerBuilder

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
//        self.backgroundColor = [UIColor colorWithRed:(255.0/255.0) green:(80.0/255.0) blue:(7.0/255.0) alpha:0.5];
        self.backgroundColor = [UIColor colorWithRed:(237.0/255.0) green:(237.0/255.0) blue:(237.0/255.0) alpha:(1)];
        //self.backgroundColor = [UIColor whiteColor];
        self.layer.shadowColor = [UIColor darkGrayColor].CGColor;
        self.layer.shadowOpacity = 1;
        self.layer.shadowRadius = 2;
        self.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
        [self addLogo];
        
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void)changeTitle:(NSString *)title
{
    lblTitle.text = title;
}

-(void)addNavigationContoller:(NSString *)title :(BOOL)backButton{

    if(lblTitle == nil){
        lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(60, 0, 200, 45)];
        lblTitle.textColor = [UIColor colorWithRed:(61.0/255.0) green:(61.0/255.0) blue:(61.0/255.0) alpha:1];
        lblTitle.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:16.0f];
        lblTitle.numberOfLines = 2;
        [lblTitle setTextAlignment:NSTextAlignmentCenter];
    }
    
    lblTitle.text = title;
    
    if(backButton)
    {
        [self addBackButton];
        [lblTitle setFrame:CGRectMake(75, 0, 190, 45)];
    }else
    {
        [self addNavIcon];
    }
    
     [self addSubview:lblTitle];

}

-(void)addBackButton
{
    
    UIButton *backButton = [[UIButton alloc]initWithFrame:CGRectMake(10, 8, 60, 30)];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back-button@2x"] forState:UIControlStateNormal];
    backButton.titleLabel.textColor = [UIColor whiteColor];
    backButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:12.0f];
    [backButton addTarget:[self superview] action:@selector(clickBackButton) forControlEvents:(UIControlEventTouchUpInside)];
    [self addSubview:backButton];
    
}

-(void)addNavIcon
{
    _leftNavigationButton = [[UIButton alloc] initWithFrame:CGRectMake(10, 8, 38, 30)];
    [_leftNavigationButton setBackgroundImage:[UIImage imageNamed:@"nav-icon"] forState:UIControlStateNormal];
    
    [_leftNavigationButton addTarget:self action:@selector(showLeftNav) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_leftNavigationButton];
}

-(void)addLogo
{
    UIImageView *LogoImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ios7@2x"]];
    [LogoImage setFrame:CGRectMake(260, 3, 40, 40)];
    [self addSubview:LogoImage];
}

-(void)showLeftNav
{
    UIView *mainContainer = [self superview];
    NSLog(@"%@", mainContainer);
    NSInteger x = mainContainer.frame.origin.x;
    CGRect frame;
    if (x == 0) {
        frame = CGRectMake(260, 20, mainContainer.frame.size.width, mainContainer.frame.size.height);
    }
    else
    {
        frame = CGRectMake(0, 20, mainContainer.frame.size.width, mainContainer.frame.size.height);
    }
    
    [UIView animateWithDuration:0.3 animations:^{
        //move main container to right
        [mainContainer setFrame:frame];
    }];
    
    [self.superview.superview endEditing:YES];
}

@end
