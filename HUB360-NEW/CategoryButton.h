//
//  CategoryButton.h

#import <UIKit/UIKit.h>
#import "ChangeCategoryDelegate.h"

@interface CategoryButton : UIButton{
    UILabel * lblTitle;
    UIColor * colorNormal;
    UIColor * colorHighlighted;
    UIColor * colorSelected;
}
@property(nonatomic, assign) id<ChangeCategoryDelegate> delegate;
- (id)initWithDelegate:(id<ChangeCategoryDelegate>)delegate initWithFrame:(CGRect)frame;
- (void) setCustomTitle:(NSString *)title :(UIColor *)bgColor;
- (void) setCustomIcon:(UIImage *)icon;
@end
