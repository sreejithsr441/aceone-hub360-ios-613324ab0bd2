//
//  EventsViewController.h
//  HUB360-NEW
//
//  Created by Oyuka on 12/26/13.
//  Copyright (c) 2013 Oyuka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SideNavigationDelegate.h"

@interface EventsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, SideNavigationDelegate, UISearchBarDelegate>

@end
