//
//  ViewDetailViewController.h
//  HUB360-NEW
//
//  Created by Oyuka on 1/10/14.
//  Copyright (c) 2014 Oyuka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Carousel.h"
#import "NavigationControllerBuilder.h"
#import <CoreLocation/CoreLocation.h>

@interface ViewDetailViewController : UIViewController <UIGestureRecognizerDelegate, UIScrollViewDelegate, CLLocationManagerDelegate>
{
    
}

@property NSString *mainCategoryName;
@property NSString *subCategoryName;
@property int subCategoryId;

@property NSString *businessName;
@property NSString *businessAddress;
@property float businessDistance;
@property NSString *phoneNumber;
@property NSString *businessHours;
@property NSString *trending;
@property NSString *rating;
@property NSString *latestReview;
@property NSString *couponImageURL;
@property NSString *offer;
@property NSString *offerID;
@property NSString *webAddress;
@property NSString *mapAddress;
@property NSString *menuLink;
@property NSString *companyID;

@property BOOL withCoupon;


@end
