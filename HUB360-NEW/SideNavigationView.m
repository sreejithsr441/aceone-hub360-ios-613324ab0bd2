//
//  SideNavigationView.m
//  HUB360-NEW
//
//  Created by Oyuka on 12/29/13.
//  Copyright (c) 2013 Oyuka. All rights reserved.
//

#import "SideNavigationView.h"

@implementation SideNavigationView

- (id)initWithDelegate:(id<SideNavigationDelegate>)delegate frame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setBackgroundColor:[UIColor colorWithRed:(61/255.0) green:(61/255.0) blue:(61/255.0) alpha:1]];
        self.separatorColor = [UIColor blackColor];
        self.separatorInset = UIEdgeInsetsZero;
        self.delegate = delegate;
    }
    return self;
}

#pragma mark - Helper Methods
-(NSArray*)listMenuItems
{
    return  @[@"Home",
              @"My Favorites",
              @"Coupons - Our pick",
              @"ASU",
              @"Jonesboro Sports",
              @"News & Events"
              ];
}

-(NSArray*)listMenuItemIcons{
    return  @[@"menu_cell_category_icon2",
              @"menu_cell_shopping_icon1",
              @"menu_schedule_icon",
              @"menu_highschool_icon",
              @"menu_score_alert_icon",
              @"menu_news_icon"
              ];
}

-(int)menucount{
    return  7;
}

@end
