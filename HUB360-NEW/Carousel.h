//
//  Carousel.h
//  FlowerShopNetwork
//
//  Created by Oyuka on 8/23/13.
//  Copyright (c) 2013 Oyuka. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Carousel : UIView<UIScrollViewDelegate, UIGestureRecognizerDelegate>

@property(strong,nonatomic)UIScrollView *scrollView;

@end
