//
//  NavigationControllerBuilder.h
//  HUB360-NEW
//
//  Created by Oyuka on 12/19/13.
//  Copyright (c) 2013 Oyuka. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavigationControllerBuilder : UIView
{
    UILabel * lblTitle;
    UIColor * colorNormal;
    UIColor * colorHighlighted;
    UIColor * colorSelected;
}

-(void)changeTitle:(NSString *)title;
-(void)addNavigationContoller:(NSString *)title :(BOOL)backButton;
-(void)showLeftNav;
-(void)clickBackButton;
-(void)clickLogo;
@property (nonatomic, strong) UIButton *leftNavigationButton;
@end
