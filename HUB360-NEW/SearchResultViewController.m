//
//  SearchResultViewController.m
//  HUB360-NEW
//
//  Created by Oyuka on 1/13/14.
//  Copyright (c) 2014 Oyuka. All rights reserved.
//

#import "SearchResultViewController.h"
#import "EventsViewController.h"
#import "NearByMeViewController.h"
#import "SideNavigationView.h"
#import "hub360Config.h"
#import "Home.h"
#import "ViewDetailViewController.h"
#import "Reachability.h"


@interface SearchResultViewController ()

@end

@implementation SearchResultViewController
{
    Carousel *_carouselView;
    SideNavigationView *_sideNav;
    UIView *_mainContainer;
    hub360Config *_config;
    NavigationControllerBuilder *_navigationBar;
    UITableView *_resultTableWithOffer;
    UITableView *_resultTableWithOutOffer;
    
    NSMutableArray *_resultWithOfferArray;
    NSMutableArray *_resultWithoutOfferArray;
    
    UILabel *_withOfferLabel;
    UILabel *_withoutOfferLabel;
    
    CLLocationManager *_locationManager;
}

@synthesize distance;
@synthesize subCategoryName;
@synthesize subCategoryId;
@synthesize searchText;
@synthesize showCouponOnly;
@synthesize mainCategory;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    
    // location
    //---- For getting current gps location
    _locationManager = [CLLocationManager new];
    _locationManager.delegate = self;
    _locationManager.distanceFilter = 100.0; // Will notify the LocationManager every 100 meters
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    [_locationManager startUpdatingLocation];
    
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0){
        [_locationManager requestAlwaysAuthorization];
    }
    

//    _locationManager = [[CLLocationManager alloc] init];
//    _locationManager.distanceFilter = kCLDistanceFilterNone;
//    _locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
//    [_locationManager startUpdatingLocation];
    
    showCouponOnly = FALSE;
    _config = [hub360Config new];
    
    _sideNav = [[SideNavigationView alloc]initWithDelegate:self frame:CGRectMake(0, 20, [_config width]-60, [_config height])];
    [_sideNav setDataSource:self];
    [self.view addSubview:_sideNav];
    
    // main container
    _mainContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 20, [_config width], [_config height])];
    _mainContainer.backgroundColor =[UIColor colorWithRed:(237.0/255.0) green:(237.0/255.0) blue:(237.0/255.0) alpha:(1)];
    [self.view addSubview:_mainContainer];
    
    
    // Allocate a reachability object
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    
    if(networkStatus == NotReachable){
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Network Error" message:@"No internet connection available. Please check your connection." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];

    }else{
        
        [self getResult];
    };
    
    // adding navigation controller
    _navigationBar = [[NavigationControllerBuilder alloc]initWithFrame:CGRectMake(0, 0, 320, 45)];
    
    [_navigationBar addNavigationContoller:[NSString stringWithFormat:@"\"%@\" Result",searchText] :YES];
    [_mainContainer addSubview:_navigationBar];
    
}

#pragma mark - get result

-(void)getResult{
    
    float latitude = _locationManager.location.coordinate.latitude;
    float longitude = _locationManager.location.coordinate.longitude;
    
    searchText = [searchText stringByReplacingOccurrencesOfString:@"'" withString:@""];
    
    NSLog(@"*dLatitude : %f", latitude);
    NSLog(@"*dLongitude : %f",longitude);
    
    NSString *sopMessage1=[NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                           "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                           "<soap:Body>"
                           "<SearchNew xmlns=\"http://www.hub360.com/\">"
                           "<criteria>%@</criteria>"
                           "<la>%f</la>"
                           "<lo>%f</lo>"
                           "<mile>%f</mile>"
                           "<PageId>%d</PageId>"
                           "</SearchNew>"
                           "</soap:Body>"
                           "</soap:Envelope>",
                           searchText,
                           latitude,
                           longitude,
                           distance,
                           1];
    
    NSLog(@"%@",sopMessage1);
	
	NSURL *url=[NSURL URLWithString:@"http://webdesignjonesboro.com/hub360service/hub360Service.asmx/SearchNew"];//
	NSMutableURLRequest *req=[NSMutableURLRequest requestWithURL:url];
	NSString *msglength=[NSString stringWithFormat:@"%lu",(unsigned long)[sopMessage1 length]];
	[req addValue:@"text/xml;charset=utf-8"  forHTTPHeaderField:@"Content-Type"];
	[req addValue:@"http://webdesignjonesboro.com/hub360service/hub360Service.asmx" forHTTPHeaderField:@"SOAPAction"]; //
    [req addValue: msglength forHTTPHeaderField:@"Content-Length"];
	[req setHTTPMethod:@"POST"];
	[req setHTTPBody:[sopMessage1 dataUsingEncoding:NSUTF8StringEncoding]];
	
	NSError *error1;
	error1 = [[NSError alloc] init];
	
	NSData *returnData1 = [NSURLConnection sendSynchronousRequest:req returningResponse:nil error:&error1];
	NSString *returnString1 = [[NSString alloc] initWithData:returnData1 encoding:NSUTF8StringEncoding];
	NSLog(@"The return values are%@",returnString1);
    
    NSRange start;
    NSRange stop;
    
    start = [returnString1 rangeOfString:@"<string xmlns=\"http://www.hub360nea.com/\">"];
    stop = [returnString1 rangeOfString: @"</string>"];
    
    unsigned long start1 = start.location+start.length;
    unsigned long start2 = stop.location-start1;
    
    NSString *jsonString = [returnString1 substringWithRange: NSMakeRange (start1, start2)];
    
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *e = nil;
    NSMutableArray *json = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&e];
    NSLog(@"%@", json);
    
    _resultWithoutOfferArray = [json valueForKey:@"List`1"];
    NSLog(@"Without offer %@", _resultWithoutOfferArray);

    [self showResultInTable];
    
}


-(void)showResultInTable{
    
    
    
    if([_resultWithOfferArray count] > 0){
        
        _withOfferLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 55, 300, 30)];
        [_withOfferLabel setText:@"  Premium Listing"];
        [_withOfferLabel setBackgroundColor:[UIColor colorWithRed:(1.0/255.0) green:(157.0/255.0) blue:(89.0/255.0) alpha:1]];
        [_withOfferLabel setTextColor:[UIColor whiteColor]];
        [_mainContainer addSubview:_withOfferLabel];
        
        _resultTableWithOffer = [[UITableView alloc]initWithFrame:CGRectMake(10, 85, 300, 210)];
        _resultTableWithOffer.separatorInset = UIEdgeInsetsZero;
        [_resultTableWithOffer setDataSource:self];
        _resultTableWithOffer.delegate = self;
        
        [_mainContainer addSubview:_resultTableWithOffer];
        
        if(showCouponOnly){
            [_resultTableWithOffer setFrame:CGRectMake(10, 85, 300, [_config height]-150)];
        }
        
    }
    
    
    if(!showCouponOnly){
        
        if([_resultWithOfferArray count] == 0){
            _withoutOfferLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 55, 300, 30)];
            _resultTableWithOutOffer = [[UITableView alloc]initWithFrame:CGRectMake(10, 85, 300, [_config height]-150)];
        }else{
            _withoutOfferLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 300, 300, 30)];
            _resultTableWithOutOffer = [[UITableView alloc]initWithFrame:CGRectMake(10, 330, 300, 200)];
        }
        
        [_withoutOfferLabel setText:@"  Listing"];
        [_withoutOfferLabel setBackgroundColor: [UIColor colorWithRed:(255.0/255.0) green:(181.0/255.0) blue:(2.0/255.0) alpha:1]];
        [_withoutOfferLabel setTextColor:[UIColor whiteColor]];
        [_mainContainer addSubview:_withoutOfferLabel];
        
        _resultTableWithOutOffer.separatorInset = UIEdgeInsetsZero;
        _resultTableWithOutOffer.delegate = self;
        [_resultTableWithOutOffer setDataSource:self];
        
        [_mainContainer addSubview:_resultTableWithOutOffer];
    }
    
    if([_resultWithOfferArray count]==0 && [_resultWithoutOfferArray count]==0){

        [_withoutOfferLabel removeFromSuperview];
        [_resultTableWithOutOffer removeFromSuperview];
        UILabel *noResult = [[UILabel alloc]initWithFrame:CGRectMake(30, 80, 260, 100)];
        [noResult setTextAlignment:NSTextAlignmentCenter];
        noResult.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:14.0];
        noResult.textColor = [UIColor grayColor];
        noResult.text = [NSString stringWithFormat: @"There is no result with  \"%@\" search. Please try again.", searchText];
        noResult.numberOfLines = 3;
        [_mainContainer addSubview:noResult];
        
    }
    

}

#pragma mark - TableView Delegate Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(tableView == _resultTableWithOffer){
        
        return 60.0f;
        
    }else if (tableView == _resultTableWithOutOffer){
        
        return 60.0f;
        
    }else{
        
        return 40.0f;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == _resultTableWithOffer){
        
        return [_resultWithOfferArray count];
        
    }else{
        
        return [_resultWithoutOfferArray count];
        
    }

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell==nil) {
        
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier ];
        
    }
    
    //set image per main category
    if(tableView == _resultTableWithOffer || tableView == _resultTableWithOutOffer){
        
        
        //         cell.imageView.backgroundColor = [UIColor lightGrayColor];
        cell.imageView.frame = CGRectMake(0, 0, 30, 30);
        
        if(tableView == _resultTableWithOffer){
            
            if([mainCategory isEqualToString:@"Shopping"]){
                cell.imageView.image = [UIImage imageNamed:@"shoppingwithoffer-icon"];
            }else if([mainCategory isEqualToString:@"Dining"]){
                cell.imageView.image = [UIImage imageNamed:@"diningwithoffer-icon"];
            }else{
                cell.imageView.image = [UIImage imageNamed:@"serviceswithoffer-icon"];
            }
        }
        
        if(tableView == _resultTableWithOutOffer){
            
            if([mainCategory isEqualToString:@"Shopping"]){
                cell.imageView.image = [UIImage imageNamed:@"shoppingwithoutoffer-icon"];
            }else if([mainCategory isEqualToString:@"Dining"]){
                cell.imageView.image = [UIImage imageNamed:@"diningwithoutoffer-icon"];
            }else{
                cell.imageView.image = [UIImage imageNamed:@"serviceswithoutoffer-icon"];
            }
        }
        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.indentationWidth = 0.0;
        cell.indentationLevel = 0.0;
        
    }
    
    // table with offer
    if(tableView == _resultTableWithOffer){
        
        NSLog(@"%@", [[_resultWithOfferArray objectAtIndex:indexPath.row] objectForKey:@"companyName"]);
        
        cell.textLabel.text = [self replaceAscii:[[_resultWithOfferArray objectAtIndex:indexPath.row] objectForKey:@"companyName"]];
        [cell.textLabel setTextColor:[UIColor colorWithRed:(1.0/255.0) green:(157.0/255.0) blue:(89.0/255.0) alpha:1]];
        cell.textLabel.numberOfLines = 2;
        cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:14.0];
        
        cell.backgroundColor = [UIColor clearColor];
        cell.detailTextLabel.text = [[_resultWithOfferArray objectAtIndex:indexPath.row] objectForKey:@"address"];
        cell.detailTextLabel.textColor = [UIColor grayColor];
        cell.detailTextLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:12.0];
        
    }
    //table without offer
    else if(tableView == _resultTableWithOutOffer){
        
        NSLog(@"%@", [[_resultWithoutOfferArray objectAtIndex:indexPath.row] objectForKey:@"companyName"]);
        cell.textLabel.textColor = [UIColor colorWithRed:(255.0/255.0) green:(181.0/255.0) blue:(2.0/255.0) alpha:1];
        
        cell.textLabel.text = [self replaceAscii:[[_resultWithoutOfferArray objectAtIndex:indexPath.row] objectForKey:@"companyName"]];
        cell.textLabel.numberOfLines = 2;
        cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:14.0];
        
        cell.backgroundColor = [UIColor clearColor];
        cell.detailTextLabel.text = [[_resultWithoutOfferArray objectAtIndex:indexPath.row] objectForKey:@"address"];
        cell.detailTextLabel.textColor = [UIColor grayColor];
        cell.detailTextLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:12.0];
        
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ViewDetailViewController *viewDetail = [ViewDetailViewController new];
    
    if(tableView == _resultTableWithOffer){
        
        viewDetail.withCoupon = YES;
        viewDetail.mainCategoryName = mainCategory ;
        viewDetail.subCategoryName = subCategoryName;
        viewDetail.subCategoryId = subCategoryId;
        viewDetail.businessName = [self replaceAscii: [[_resultWithOfferArray objectAtIndex:indexPath.row] objectForKey:@"companyName"]];
        viewDetail.businessAddress = [[_resultWithOfferArray objectAtIndex:indexPath.row] objectForKey:@"address"];
        viewDetail.businessDistance = [[[_resultWithOfferArray objectAtIndex:indexPath.row] objectForKey:@"miles"] floatValue];
        viewDetail.phoneNumber = [[_resultWithOfferArray objectAtIndex:indexPath.row] objectForKey:@"phone"];
        viewDetail.businessHours = [[_resultWithOfferArray objectAtIndex:indexPath.row] objectForKey:@"hours"];
        viewDetail.trending = [[_resultWithOfferArray objectAtIndex:indexPath.row] objectForKey:@"offer"];
        viewDetail.rating = [[_resultWithOfferArray objectAtIndex:indexPath.row] objectForKey:@"rating"];
        viewDetail.latestReview = [[_resultWithOfferArray objectAtIndex:indexPath.row] objectForKey:@"review"];
        viewDetail.couponImageURL = [[_resultWithOfferArray objectAtIndex:indexPath.row] objectForKey:@"imageURL"];
        viewDetail.offer = [[_resultWithOfferArray objectAtIndex:indexPath.row] objectForKey:@"offer"];
        viewDetail.offerID = [[_resultWithOfferArray objectAtIndex:indexPath.row] objectForKey:@"offerID"];
        viewDetail.webAddress = [[_resultWithOfferArray objectAtIndex:indexPath.row] objectForKey:@"webAdd"];
        viewDetail.menuLink = [[_resultWithOfferArray objectAtIndex:indexPath.row] objectForKey:@"menu"];
        viewDetail.companyID = [[_resultWithOfferArray objectAtIndex:indexPath.row] objectForKey:@"companyID"];
        
        NSLog(@"Menu item selected");
        [self presentViewController:viewDetail animated:YES completion:nil];
    }
    
    if(tableView == _resultTableWithOutOffer){
        
        viewDetail.withCoupon = NO;
        viewDetail.mainCategoryName = mainCategory ;
        viewDetail.subCategoryName = subCategoryName;
        viewDetail.subCategoryId = subCategoryId;
        viewDetail.businessName = [self replaceAscii:[[_resultWithoutOfferArray objectAtIndex:indexPath.row] objectForKey:@"companyName"]];
        viewDetail.businessAddress = [[_resultWithoutOfferArray objectAtIndex:indexPath.row] objectForKey:@"address"];
        viewDetail.businessDistance = [[[_resultWithoutOfferArray objectAtIndex:indexPath.row] objectForKey:@"miles"] floatValue];
        viewDetail.phoneNumber = [[_resultWithoutOfferArray objectAtIndex:indexPath.row] objectForKey:@"phone"];
        //        viewDetail.offerID = [[_resultWithOfferArray objectAtIndex:indexPath.row] objectForKey:@"offerID"];
        //        viewDetail.businessHours = [[_resultWithoutOfferArray objectAtIndex:indexPath.row] objectForKey:@"hours"];
        //        viewDetail.trending = [[_resultWithoutOfferArray objectAtIndex:indexPath.row] objectForKey:@"offer"];
        viewDetail.rating = [[_resultWithoutOfferArray objectAtIndex:indexPath.row] objectForKey:@"rating"];
        viewDetail.latestReview = [[_resultWithoutOfferArray objectAtIndex:indexPath.row] objectForKey:@"review"];
        viewDetail.webAddress = [[_resultWithoutOfferArray objectAtIndex:indexPath.row] objectForKey:@"webAdd"];
        viewDetail.mapAddress = [[_resultWithoutOfferArray objectAtIndex:indexPath.row] objectForKey:@"mapAdd"];
        viewDetail.companyID = [[_resultWithoutOfferArray objectAtIndex:indexPath.row] objectForKey:@"companyID"];
        
        
        NSLog(@"Menu item selected");
        [self presentViewController:viewDetail animated:YES completion:nil];
    }
}

#pragma mark - Helper Methods

-(NSString *)replaceAscii:(NSString*)string{
    NSString *s = string;
    s = [s stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
//    s = [s stringByReplacingOccurrencesOfString:@"'" withString:@""];
    return s;
}

-(void)selectMenuItem:(NSInteger)index{
    [_navigationBar showLeftNav];
    
    // change view controller
    
    switch (index) {
        case 1:
        {
            Home *homePage = [Home new];
            [self presentViewController:homePage animated:YES completion:nil];
        }
            break;
            
        default:
            break;
    }
    
}

-(void)clickBackButton{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
