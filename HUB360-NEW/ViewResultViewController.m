//
//  ViewResultViewController.m
//  HUB360-NEW
//
//  Created by Oyuka on 1/2/14.
//  Copyright (c) 2014 Oyuka. All rights reserved.
//

#import "ViewResultViewController.h"
#import "EventsViewController.h"
#import "NearByMeViewController.h"
#import "hub360Config.h"
#import "Home.h"
#import "ViewDetailViewController.h"

#import "ActivityIndicator.h"
#import "AdvertisementBlock.h"
#import "SRUtility.h"
@interface ViewResultViewController ()

@end

@implementation ViewResultViewController{
    Carousel *_carouselView;
    UIView *_mainContainer;
    hub360Config *_config;
    NavigationControllerBuilder *_navigationBar;
    UITableView *_resultTableWithOffer;
    UITableView *_resultTableWithOutOffer;
   
    NSMutableArray *_resultWithOfferArray;
    NSMutableArray *_resultWithoutOfferArray;
    
    UILabel *_withOfferLabel;
    UILabel *_withoutOfferLabel;
    
    CLLocationManager *_locationManager;
    
    ActivityIndicator *_activityIndicator;
    AdvertisementBlock *_advertisementBanner;
}

@synthesize distance;
@synthesize showCouponOnly;
@synthesize subCategoryName;
@synthesize subCategoryId;
@synthesize mainCategory;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // location 
    _locationManager = [CLLocationManager new];
    _locationManager.delegate = self;
    _locationManager.distanceFilter = 100.0; // Will notify the LocationManager every 100 meters
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    [_locationManager startUpdatingLocation];
    
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0){
        [_locationManager requestAlwaysAuthorization];
    }

    
    _config = [hub360Config new];
    
    // main container
    _mainContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 20, [_config width], [_config height])];
    _mainContainer.backgroundColor =[UIColor colorWithRed:(237.0/255.0) green:(237.0/255.0) blue:(237.0/255.0) alpha:(1)];
    [self.view addSubview:_mainContainer];
    
    
    [self getResult];
    
    // adding navigation controller
    _navigationBar = [[NavigationControllerBuilder alloc]initWithFrame:CGRectMake(0, 0, 320, 45)];
    
    [_navigationBar addNavigationContoller:[NSString stringWithFormat:@"%@ Result",mainCategory] :YES];
    [_mainContainer addSubview:_navigationBar];
    
    _activityIndicator = [ActivityIndicator new];
    [_mainContainer addSubview:_activityIndicator];
    
    
    // Banner add
    _advertisementBanner = [AdvertisementBlock new];
    [_mainContainer addSubview:_advertisementBanner];
    [_advertisementBanner getBannerAds];
    
    
    if([_config height] < 500){
        [_advertisementBanner setFrame:CGRectMake(0, 410, 320, 50)];
    }
}

#pragma mark - get result

-(void)getResult
{
    
    float latitude = _locationManager.location.coordinate.latitude;
    float longitude = _locationManager.location.coordinate.longitude;
    
    NSString *urlString = [NSString stringWithFormat:@"http://webdesignjonesboro.com/hub360service/hub360Service.asmx/GetDetailsWithofferNew?la=%f&lan=%f&mil=%@&SubCategoryName=%@&SubCategoryID=%@",
                           latitude ,
                           longitude,
                           [NSString stringWithFormat:@"%.1f", distance],
                           subCategoryName,
                           [NSString stringWithFormat:@"%d",subCategoryId]];
    
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *req=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    [req addValue:@"http://webdesignjonesboro.com/hub360service/hub360Service.asmx" forHTTPHeaderField:@"SOAPAction"]; //
    [req setHTTPMethod:@"GET"];
    
    NSError *error1 = [NSError new];
    
    NSData *returnData1 = [NSURLConnection sendSynchronousRequest:req returningResponse:nil error:&error1];
    NSString *returnString = [[NSString alloc] initWithData:returnData1 encoding:NSUTF8StringEncoding];
    NSUInteger DataLength=[returnData1 length];
    
    if (!DataLength==0)
    {
        NSLog(@"With Offer Result: %@",returnString);
        
        NSRange start;
        NSRange stop;
        
        start = [returnString rangeOfString:@"<string xmlns=\"http://www.hub360nea.com/\">"];
        stop = [returnString rangeOfString: @"</string>"];
        
        unsigned long start1 = start.location+start.length;
        unsigned long start2 = stop.location-start1;
        
        NSString *jsonString = [returnString substringWithRange: NSMakeRange (start1, start2)];
        
        NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        NSError *e = nil;
        NSMutableArray *json = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&e];
        
        _resultWithOfferArray = [json valueForKey:@"List`1"];
        
        
        if(!showCouponOnly){
            
            NSString *urlString = [NSString stringWithFormat:@"http://webdesignjonesboro.com/hub360service/hub360Service.asmx/GetDetailsWithoutOfferNew?zip=%@&la=%f&lo=%f&mile=%@&SubCategoryName=%@&SubCategoryID=%@&PageId=%@",
                                   @"72401",
                                   latitude ,
                                   longitude,
                                   [NSString stringWithFormat:@"%.1f", distance],
                                   subCategoryName,
                                   [NSString stringWithFormat:@"%d",subCategoryId],
                                   @"1"];
            
            urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSMutableURLRequest *req1=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
            [req1 addValue:@"http://webdesignjonesboro.com/hub360service/hub360Service.asmx" forHTTPHeaderField:@"SOAPAction"]; //
            [req1 setHTTPMethod:@"GET"];
            
            NSError *error1;
            error1 = [NSError new];
            
            NSData *returnData1 = [NSURLConnection sendSynchronousRequest:req1 returningResponse:nil error:&error1];
            NSString *returnString1 = [[NSString alloc] initWithData:returnData1 encoding:NSUTF8StringEncoding];
            NSLog(@"%@",returnString1);
            
            NSRange start;
            NSRange stop;
            
            start = [returnString1 rangeOfString:@"<string xmlns=\"http://www.hub360nea.com/\">"];
            stop = [returnString1 rangeOfString: @"</string>"];
            
            unsigned long start1 = start.location+start.length;
            unsigned long start2 = stop.location-start1;
            
            NSString *jsonString = [returnString1 substringWithRange: NSMakeRange (start1, start2)];
            
            NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
            NSError *e = nil;
            NSMutableArray *json = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&e];
            NSLog(@"%@", json);
            
            _resultWithoutOfferArray = [json valueForKey:@"List`1"];
            NSLog(@"Without offer %@", _resultWithoutOfferArray);
        }
        
        [self showResultInTable];
    }
    else
    {
        UIAlertView *alret=[[UIAlertView alloc]initWithTitle:@"Network Error" message:@"Your Network Is Disconnected. Please Check Your Network Connection And Try Again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alret show];
    
    }
    
   
}

-(void)showResultInTable
{
    
    if ([SRUtility reachable])
    {
        
        if([_resultWithOfferArray count] > 0){
            
            _withOfferLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 55, 300, 30)];
            [_withOfferLabel setText:@"  Premium Listing"];
            [_withOfferLabel setBackgroundColor:[UIColor colorWithRed:(1.0/255.0) green:(157.0/255.0) blue:(89.0/255.0) alpha:1]];
            [_withOfferLabel setTextColor:[UIColor whiteColor]];
            [_mainContainer addSubview:_withOfferLabel];
            
            _resultTableWithOffer = [[UITableView alloc]initWithFrame:CGRectMake(10, 85, 300, 200)];
            _resultTableWithOffer.separatorInset = UIEdgeInsetsZero;
            [_resultTableWithOffer setDataSource:self];
            _resultTableWithOffer.delegate = self;
            
            [_mainContainer addSubview:_resultTableWithOffer];
            
            if([_config height] < 500){
                [_resultTableWithOffer setFrame:CGRectMake(10, 85, 300, 150)];
            }
            
            if(showCouponOnly){
                [_resultTableWithOffer setFrame:CGRectMake(10, 85, 300, [_config height]-160)];
            }
            
            
        }
        
        
        if(!showCouponOnly){
            
            if([_resultWithOfferArray count] == 0){
                
                _withoutOfferLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 55, 300, 30)];
                _resultTableWithOutOffer = [[UITableView alloc]initWithFrame:CGRectMake(10, 85, 300, [_config height]-160)];
                
            }else{
                
                _withoutOfferLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 290, 300, 30)];
                _resultTableWithOutOffer = [[UITableView alloc]initWithFrame:CGRectMake(10, 320, 300, 175)];
                
                if([_config height] < 500){
                    [_withoutOfferLabel setFrame:CGRectMake(10, 237, 300, 30)];
                    [_resultTableWithOutOffer setFrame:CGRectMake(10, 267, 300, 140)];
                }
            }
            
            [_withoutOfferLabel setText:@"  Listing"];
            [_withoutOfferLabel setBackgroundColor: [UIColor colorWithRed:(255.0/255.0) green:(181.0/255.0) blue:(2.0/255.0) alpha:1]];
            [_withoutOfferLabel setTextColor:[UIColor whiteColor]];
            [_mainContainer addSubview:_withoutOfferLabel];
            
            _resultTableWithOutOffer.separatorInset = UIEdgeInsetsZero;
            _resultTableWithOutOffer.delegate = self;
            [_resultTableWithOutOffer setDataSource:self];
            
            [_mainContainer addSubview:_resultTableWithOutOffer];
        }
        
        if([_resultWithOfferArray count] == 0 && [_resultWithoutOfferArray count]==0){
            
            [_withoutOfferLabel removeFromSuperview];
            [_resultTableWithOutOffer removeFromSuperview];
            UILabel *noResult = [[UILabel alloc]initWithFrame:CGRectMake(30, 80, 260, 100)];
            [noResult setTextAlignment:NSTextAlignmentCenter];
            noResult.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:14.0];
            noResult.textColor = [UIColor grayColor];
            noResult.text = @"There is no result under this category. Please try again later.";
            noResult.numberOfLines = 3;
            [_mainContainer addSubview:noResult];
            
        }

        
    }
    else
    {
    
        UIAlertView *alret=[[UIAlertView alloc]initWithTitle:@"Network Error" message:@"Your Network Is Disconnected. Please Check Your Network Connection And Try Again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alret show];
    }
    
    
   
}

#pragma mark - TableView Delegate Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

        
        return 60.0f;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == _resultTableWithOffer){
        
        return [_resultWithOfferArray count];
        
    }else{
        
        return [_resultWithoutOfferArray count];
        
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell==nil) {
        
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier ];
        
    }
    
    //set image per main category
     if(tableView == _resultTableWithOffer || tableView == _resultTableWithOutOffer){
         
         
//         cell.imageView.backgroundColor = [UIColor lightGrayColor];
         cell.imageView.frame = CGRectMake(0, 0, 30, 30);
         
         if(tableView == _resultTableWithOffer){
             
             if([mainCategory isEqualToString:@"Shopping"]){
                 cell.imageView.image = [UIImage imageNamed:@"shoppingwithoffer-icon"];
             }else if([mainCategory isEqualToString:@"Dining"]){
                 cell.imageView.image = [UIImage imageNamed:@"diningwithoffer-icon"];
             }else{
                 cell.imageView.image = [UIImage imageNamed:@"serviceswithoffer-icon"];
             }
         }
         
         if(tableView == _resultTableWithOutOffer){
             
             if([mainCategory isEqualToString:@"Shopping"]){
                 cell.imageView.image = [UIImage imageNamed:@"shoppingwithoutoffer-icon"];
             }else if([mainCategory isEqualToString:@"Dining"]){
                 cell.imageView.image = [UIImage imageNamed:@"diningwithoutoffer-icon"];
             }else{
                 cell.imageView.image = [UIImage imageNamed:@"serviceswithoutoffer-icon"];
             }
         }
         
         cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
         cell.indentationWidth = 0.0;
         cell.indentationLevel = 0.0;
         
     }
    
    // table with offer
    if(tableView == _resultTableWithOffer){
        
        NSLog(@"%@", [[_resultWithOfferArray objectAtIndex:indexPath.row] objectForKey:@"companyName"]);
        
        cell.textLabel.text = [self replaceAscii:[[_resultWithOfferArray objectAtIndex:indexPath.row] objectForKey:@"companyName"]];
        [cell.textLabel setTextColor:[UIColor colorWithRed:(1.0/255.0) green:(157.0/255.0) blue:(89.0/255.0) alpha:1]];
        cell.textLabel.numberOfLines = 2;
        cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:14.0];
        
        cell.backgroundColor = [UIColor clearColor];
        cell.detailTextLabel.text = [[_resultWithOfferArray objectAtIndex:indexPath.row] objectForKey:@"address"];
        cell.detailTextLabel.textColor = [UIColor grayColor];
        cell.detailTextLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:12.0];
    
    }
    //table without offer
    else if(tableView == _resultTableWithOutOffer){
        
        NSLog(@"%@", [[_resultWithoutOfferArray objectAtIndex:indexPath.row] objectForKey:@"companyName"]);
        cell.textLabel.textColor = [UIColor colorWithRed:(255.0/255.0) green:(181.0/255.0) blue:(2.0/255.0) alpha:1];
        
        cell.textLabel.text = [self replaceAscii:[[_resultWithoutOfferArray objectAtIndex:indexPath.row] objectForKey:@"companyName"]];
        cell.textLabel.numberOfLines = 2;
        cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:14.0];
        
        cell.backgroundColor = [UIColor clearColor];
        cell.detailTextLabel.text = [[_resultWithoutOfferArray objectAtIndex:indexPath.row] objectForKey:@"address"];
        cell.detailTextLabel.textColor = [UIColor grayColor];
        cell.detailTextLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:12.0];
        
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [NSThread detachNewThreadSelector:@selector(startActivityIndicator) toTarget:_activityIndicator withObject:nil];

    ViewDetailViewController *viewDetail = [ViewDetailViewController new];
    
    if(tableView == _resultTableWithOffer)
    {
        
        viewDetail.withCoupon = YES;
        viewDetail.mainCategoryName = mainCategory ;
        viewDetail.subCategoryName = subCategoryName;
        viewDetail.subCategoryId = subCategoryId;
        viewDetail.businessName = [self replaceAscii: [[_resultWithOfferArray objectAtIndex:indexPath.row] objectForKey:@"companyName"]];
        viewDetail.businessAddress = [[_resultWithOfferArray objectAtIndex:indexPath.row] objectForKey:@"address"];
        viewDetail.businessDistance = [[[_resultWithOfferArray objectAtIndex:indexPath.row] objectForKey:@"miles"] floatValue];
        viewDetail.phoneNumber = [[_resultWithOfferArray objectAtIndex:indexPath.row] objectForKey:@"phone"];
        viewDetail.businessHours = [[_resultWithOfferArray objectAtIndex:indexPath.row] objectForKey:@"hours"];
        viewDetail.trending = [[_resultWithOfferArray objectAtIndex:indexPath.row] objectForKey:@"offer"];
        viewDetail.rating = [[_resultWithOfferArray objectAtIndex:indexPath.row] objectForKey:@"rating"];
        viewDetail.latestReview = [[_resultWithOfferArray objectAtIndex:indexPath.row] objectForKey:@"review"];
        viewDetail.couponImageURL = [[_resultWithOfferArray objectAtIndex:indexPath.row] objectForKey:@"imageURL"];
        viewDetail.offer = [[_resultWithOfferArray objectAtIndex:indexPath.row] objectForKey:@"offer"];
        viewDetail.offerID = [[_resultWithOfferArray objectAtIndex:indexPath.row] objectForKey:@"offerID"];
        viewDetail.webAddress = [[_resultWithOfferArray objectAtIndex:indexPath.row] objectForKey:@"webAdd"];
        viewDetail.menuLink = [[_resultWithOfferArray objectAtIndex:indexPath.row] objectForKey:@"menu"];
        viewDetail.companyID = [[_resultWithOfferArray objectAtIndex:indexPath.row] objectForKey:@"companyID"];
        
        NSLog(@"Menu item selected");
        [self presentViewController:viewDetail animated:YES completion:nil];
    }

    if(tableView == _resultTableWithOutOffer)
    {
        
        viewDetail.withCoupon = NO;
        viewDetail.mainCategoryName = mainCategory ;
        viewDetail.subCategoryName = subCategoryName;
        viewDetail.subCategoryId = subCategoryId;
        viewDetail.businessName = [self replaceAscii:[[_resultWithoutOfferArray objectAtIndex:indexPath.row] objectForKey:@"companyName"]];
        viewDetail.businessAddress = [[_resultWithoutOfferArray objectAtIndex:indexPath.row] objectForKey:@"address"];
        viewDetail.businessDistance = [[[_resultWithoutOfferArray objectAtIndex:indexPath.row] objectForKey:@"miles"] floatValue];
        viewDetail.phoneNumber = [[_resultWithoutOfferArray objectAtIndex:indexPath.row] objectForKey:@"phone"];
//        viewDetail.offerID = [[_resultWithOfferArray objectAtIndex:indexPath.row] objectForKey:@"offerID"];
//        viewDetail.businessHours = [[_resultWithoutOfferArray objectAtIndex:indexPath.row] objectForKey:@"hours"];
//        viewDetail.trending = [[_resultWithoutOfferArray objectAtIndex:indexPath.row] objectForKey:@"offer"];
        viewDetail.rating = [[_resultWithoutOfferArray objectAtIndex:indexPath.row] objectForKey:@"rating"];
        viewDetail.latestReview = [[_resultWithoutOfferArray objectAtIndex:indexPath.row] objectForKey:@"review"];
        viewDetail.webAddress = [[_resultWithoutOfferArray objectAtIndex:indexPath.row] objectForKey:@"webAdd"];
        viewDetail.mapAddress = [[_resultWithoutOfferArray objectAtIndex:indexPath.row] objectForKey:@"mapAdd"];
        viewDetail.companyID = [[_resultWithoutOfferArray objectAtIndex:indexPath.row] objectForKey:@"companyID"];

        
        NSLog(@"Menu item selected");
        [self presentViewController:viewDetail animated:YES completion:nil];
    }
    
    [NSThread detachNewThreadSelector:@selector(stopActivityIndicator) toTarget:_activityIndicator withObject:nil];

}

#pragma mark - Helper Methods

-(NSString *)replaceAscii:(NSString*)string
{
    NSString *s = string;
    s = [s stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    return s;
}

-(void)selectMenuItem:(NSInteger)index
{
    [_navigationBar showLeftNav];
    
    // change view controller
    
    switch (index) {
        case 1:
        {
            Home *homePage = [Home new];
            [self presentViewController:homePage animated:YES completion:nil];
        }
            break;
            
        default:
            break;
    }
    
}

-(void)clickBackButton{

    
    [self dismissViewControllerAnimated:NO completion:nil];
}

@end
