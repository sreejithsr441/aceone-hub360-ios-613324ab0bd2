//
//  ByMainCategoryViewController.m
//  HUB360-NEW
//
//  Created by Oyuka on 1/13/14.
//  Copyright (c) 2014 Oyuka. All rights reserved.
//

#import "ByMainCategoryViewController.h"

#import "NearByMeViewController.h"
#import "NavigationControllerBuilder.h"
#import "SideNavigationView.h"
#import "hub360Config.h"
#import "Home.h"
#import "ViewResultViewController.h"
#import "MyFavoritesViewController.h"
#import "SearchBox.h"
#import "SearchResultViewController.h"
#import "ActivityIndicator.h"
#import "AdvertisementBlock.h"

@interface ByMainCategoryViewController ()
{
    UIView *_mainContainer;
    SideNavigationView *_sideNav;
    NavigationControllerBuilder *_navigationBar;
    hub360Config *_config;
    UIButton *_mainCategoryShoppingButton;
    UIButton *_mainCategoryDiningButton;
    UIButton *_mainCategoryServicesButton;
    
    UILabel *_distanceLabel;
    UISwitch *_showCouponOnly;
    
    UIScrollView *_subCategoryContainer;
    NSMutableArray *_subCategoriesArray;
    
    UIButton *_viewResultButton;
    
    UIImage *_activeImage;
    UIImage *_inactiveImage;
    
    SearchBox *_searchBox;
    
    ActivityIndicator *_activityIndicator;
    AdvertisementBlock *_advertisementBanner;
    AdvertisementBlock *_advertisementHalfPage;
}

@end

@implementation ByMainCategoryViewController
@synthesize mainCategory;
@synthesize mainCategoryID;

#define IS_IPHONE_5 (((double)[[UIScreen mainScreen] bounds].size.height) == ((double)568))

-(void)viewDidAppear:(BOOL)animated
{
    
    NSUserDefaults *userDefaults=[NSUserDefaults standardUserDefaults];
    NSString *countString = [userDefaults objectForKey:@"saveCountMainCategoryAds"];
    NSLog(@"Your Count: %@",countString);
    
    if(countString == nil)
        [userDefaults setObject:@"1" forKey:@"saveCountMainCategoryAds"];
    
    [userDefaults setObject:[NSString stringWithFormat:@"%ld", [countString integerValue]+1] forKey:@"saveCountMainCategoryAds"];
    [userDefaults synchronize];
    
    if(([countString integerValue]) % 5 == 0 && countString != nil)
    {
        _advertisementHalfPage = [AdvertisementBlock new];
        [_mainContainer addSubview:_advertisementHalfPage];
        [_advertisementHalfPage getFullAndHalfAds];
        [userDefaults setObject:@"1" forKey:@"saveCountMainCategoryAds"];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    _config = [hub360Config new];
    
    // side navigation table view
    
    _sideNav = [[SideNavigationView alloc]initWithDelegate:self frame:CGRectMake(0, 20, [_config width]-60, [_config height])];
    [_sideNav setDataSource:self];
    [self.view addSubview:_sideNav];
    
    [self displayPageContents];
    
}

#pragma mark - display page contents
-(void)displayPageContents{
    // main container
    _mainContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 20, [_config width], [_config height])];
    _mainContainer.backgroundColor =[UIColor whiteColor];
    [self.view addSubview:_mainContainer];
    
    // adding navigation controller
    _navigationBar = [[NavigationControllerBuilder alloc]initWithFrame:CGRectMake(0, 0, 320, 45)];
    [_navigationBar addNavigationContoller:[NSString stringWithFormat:@"%@", mainCategory] :YES];
    [_mainContainer addSubview:_navigationBar];
    
    
    UIView *topSubView = [[UIView alloc]initWithFrame:CGRectMake(10, 55, 300, 90)];
    topSubView.backgroundColor = [UIColor colorWithRed:(240.0/255.0) green:(240.0/255.0) blue:(240.0/255.0) alpha:1];
    [_mainContainer addSubview:topSubView];
    
    //1. Distance slider
    [self addDistanceSlider];
    
    //2. Show coupon only checkbox
    [self addShowCouponOnlySwitch];
    
//    //3. Main categories
//    [self addMainCategoryButtons];
    
    //4. Sub categories
    _subCategoryContainer = [[UIScrollView alloc]initWithFrame:CGRectMake(10, 150, 300, 300)];
    [_subCategoryContainer setBackgroundColor:[UIColor whiteColor]];
    [_mainContainer addSubview:_subCategoryContainer];
    
    _subCategoriesArray = [NSMutableArray new];
    [self getSubCategories:mainCategoryID];
    
    //5. View Results button
    _viewResultButton = [[UIButton alloc]initWithFrame:CGRectMake(80, 490, 160, 40)];
    [_viewResultButton setBackgroundColor:[UIColor grayColor]];
    [_viewResultButton setTitle:@"View Result" forState:UIControlStateNormal];
    [_viewResultButton addTarget:self action:@selector(ViewResultButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [_mainContainer addSubview:_viewResultButton];
    
    _activityIndicator = [ActivityIndicator new];
    [_mainContainer addSubview:_activityIndicator];
    
    if([mainCategory isEqualToString:@"Services"]){
        
        // Banner add
        _advertisementBanner = [AdvertisementBlock new];
        [_mainContainer addSubview:_advertisementBanner];
        [_advertisementBanner getBannerAds];
        
        [_subCategoryContainer setFrame: CGRectMake(10, 150, 300, 260)];
        [_viewResultButton setFrame:CGRectMake(80, 450, 160, 40)];
    }
    
    if([_config height] < 500){
        
        [_subCategoryContainer setFrame: CGRectMake(10, 150, 300, 250)];
        [_viewResultButton setFrame:CGRectMake(80, 410, 160, 40)];
        
        if([mainCategory isEqualToString:@"Services"]){
            
            // Banner add
            [_advertisementBanner setFrame:CGRectMake(0, 410, 320, 50)];
            [_subCategoryContainer setFrame: CGRectMake(10, 150, 300, 200)];
            [_viewResultButton setFrame:CGRectMake(80, 360, 160, 40)];
        }
    }
}

#pragma mark - TableView Delegate Methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_sideNav menucount];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *list = [[NSArray alloc]initWithArray:[_sideNav listMenuItems]];
    NSArray *iconList = [[NSArray alloc]initWithArray:[_sideNav listMenuItemIcons]];
    
    UITableViewCell *cell;
    
    if(indexPath.row == 0){
        cell = [tableView dequeueReusableCellWithIdentifier:@"searchBox"];
    }else{
        cell = [tableView dequeueReusableCellWithIdentifier:[list objectAtIndex:indexPath.row-1]];
    }
    
    
    if (cell == nil){
        
        if(indexPath.row == 0){
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"searchBox"];
            cell.backgroundColor = [UIColor clearColor];
            
             _searchBox = [[SearchBox alloc]initWithFrame:CGRectMake(0, 0, cell.frame.size.width-60, cell.frame.size.height)];
            _searchBox.delegate = self;
            [cell.contentView addSubview:_searchBox];
            
        }else{
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:[list objectAtIndex:indexPath.row-1]];
            
            cell.textLabel.text = [list objectAtIndex:indexPath.row-1];
            [cell.textLabel setTextColor:[UIColor whiteColor]];
            cell.backgroundColor = [UIColor clearColor];
            
            UIImageView *accessoryImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 12.5, 19)];
            [accessoryImage setImage:[UIImage imageNamed:@"menu_cell_indicator"]];
            cell.accessoryView = accessoryImage;
            [cell.imageView setImage:[UIImage imageNamed:[iconList objectAtIndex:indexPath.row-1]]];
        }
        
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self selectMenuItem:indexPath.row];
    NSLog(@"Menu item selected");
}

#pragma mark - Helper Methods

-(void)selectMenuItem:(NSInteger)index{
    [_navigationBar showLeftNav];
    
    // change view controller
    
    switch (index)
    {
        case 1:
        {
            Home *homePage = [Home new];
            [self presentViewController:homePage animated:YES completion:nil];
        }
            break;
        
        case 2:
        {
            MyFavoritesViewController *myFavoritesPage = [MyFavoritesViewController new];
            [self presentViewController:myFavoritesPage animated:YES completion:nil];
            
        }
        default:
            break;
    }
    
}

#pragma mark - Sub Categories

-(void)displaySubCategories:(NSInteger)parentCategoryId{
    //    [self getSubCategories:parentCategoryId-100];
    
    UIColor *buttonColor;
    
    if([mainCategory isEqualToString:@"Dining"]){
        buttonColor = [UIColor colorWithRed:(255/255.0) green:(180/255.0) blue:(2/255.0) alpha:1];
        _activeImage = [UIImage imageNamed:@"diningwithoutoffer-icon"];
        _inactiveImage = [UIImage imageNamed:@"dininggray-icon"];
    }else if([mainCategory isEqualToString:@"Services"]){
        buttonColor = [UIColor colorWithRed:(211/255.0) green:(62/255.0) blue:(42/255.0) alpha:1];
        _activeImage =[UIImage imageNamed:@"servicesred-icon"];
        _inactiveImage = [UIImage imageNamed:@"servicesgray-icon"];
    }else{
        buttonColor = [UIColor colorWithRed:(1/255.0) green:(157/255.0) blue:(89/255.0) alpha:1];
        _activeImage =[UIImage imageNamed: @"shoppingwithoffer-icon"];
        _inactiveImage = [UIImage imageNamed:@"shoppinggray-icon"];
    }
    
    
    for(UIButton *subview in [_subCategoryContainer subviews]) {
        
        if([subview isKindOfClass:[UIButton class]])
            [subview removeFromSuperview];
    }
    
    int x = 0;
    int y = 0;
    UIButton *subCategoryButton;
    
    for(NSArray *subCategory in _subCategoriesArray) {
        
        subCategoryButton = [[UIButton alloc]initWithFrame:CGRectMake(x, y, 300, 50)];
        [subCategoryButton setTitle:[subCategory valueForKey:@"categoryName"] forState:(UIControlStateNormal)];
        subCategoryButton.titleLabel.numberOfLines = 2;
        subCategoryButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:16.0];
        [subCategoryButton.titleLabel setFrame:CGRectMake(10, 10, 280, 30)];
        subCategoryButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        subCategoryButton.contentEdgeInsets = UIEdgeInsetsMake(10, 50, 0, 0);
        //        [subCategoryButton.titleLabel setTextAlignment:NSTextAlignmentLeft];
        
        UIImageView *buttonImage = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 30, 30)];
        buttonImage.image = _inactiveImage;
        [subCategoryButton addSubview:buttonImage];
        [subCategoryButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [subCategoryButton setTitleColor:buttonColor forState:UIControlStateSelected];
        
        subCategoryButton.tag = [[subCategory valueForKey:@"categoryID"] integerValue];
        NSLog(@"SubCatName: %@", [subCategory valueForKey:@"categoryName"]);
        [_subCategoryContainer addSubview:subCategoryButton];
        
        [subCategoryButton addTarget:self action:@selector(selectSubCategoryAction:) forControlEvents:UIControlEventTouchUpInside];
        
        // by default first sub category is selected
        if(x==0 && y==0){
            subCategoryButton.selected = YES;
            buttonImage.image = _activeImage;
            
        }
        
        // border bottom
        UILabel *line = [[UILabel alloc]initWithFrame:CGRectMake(0, 49, 300, 1)];
        [line setBackgroundColor:[UIColor colorWithRed:(228/255.0) green:(228/255.0) blue:(228/255.0) alpha:1]];
        [subCategoryButton addSubview:line];
        
        y+=51;
    }
    _subCategoryContainer.contentSize = CGSizeMake(300, y);
    _subCategoryContainer.scrollEnabled = YES;
    
}

-(void)getSubCategories:(int)categoryId{
    
    NSString *urlString = [NSString stringWithFormat:@"http://webdesignjonesboro.com/hub360service/hub360Service.asmx/GetSubCategoryListNew?categoryId=%d", categoryId];
    
    NSMutableURLRequest *req=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    [req addValue:@"text/plain;charset=UTF-8"  forHTTPHeaderField:@"Content-Type"];
    [req addValue:@"http://webdesignjonesboro.com/hub360service/hub360Service.asmx" forHTTPHeaderField:@"SOAPAction"];
    [req setHTTPMethod:@"GET"];
    
    NSError *error1 = [NSError new];
    
    NSData *returnData1 = [NSURLConnection sendSynchronousRequest:req returningResponse:nil error:&error1];
    NSString *returnString = [[NSString alloc] initWithData:returnData1 encoding:NSUTF8StringEncoding];
    NSLog(@"GetSubCategory: %@",returnString);
    
    // get rid of the xml envelope
    NSRange start;
    NSRange stop;
    
    start = [returnString rangeOfString:@"<string xmlns=\"http://www.hub360nea.com/\">"];
    stop = [returnString rangeOfString: @"</string>"];
    
    unsigned long start1 = start.location+start.length;
    unsigned long start2 = stop.location-start1;
    
    
    NSString *jsonString = [returnString substringWithRange: NSMakeRange (start1, start2)];
    
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *e = nil;
    NSMutableArray *json = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&e];
    NSLog(@"%@", json);
    _subCategoriesArray = [json valueForKey:@"List`1"];
    
    [self displaySubCategories:categoryId];
}

-(void)selectSubCategoryAction:(id)sender{
    
    UIButton *selectedSubCategoryButton = (UIButton *)sender;
    selectedSubCategoryButton.selected = YES;
    
    if([mainCategory isEqualToString:@"Dining"]){
        _activeImage = [UIImage imageNamed:@"diningwithoutoffer-icon"];
        _inactiveImage = [UIImage imageNamed:@"dininggray-icon"];
    }else if([mainCategory isEqualToString:@"Services"]){
        _activeImage =[UIImage imageNamed:@"servicesred-icon"];
        _inactiveImage = [UIImage imageNamed:@"servicesgray-icon"];
    }else{
        _activeImage =[UIImage imageNamed: @"shoppingwithoffer-icon"];
        _inactiveImage = [UIImage imageNamed:@"shoppinggray-icon"];
    }
    
    for(UIImageView *image in [selectedSubCategoryButton subviews]){
        if([image isKindOfClass:[UIImageView class]]){
            image.image = _activeImage;
        }
    }
    
    
    NSLog(@"button tag %ld", (long)selectedSubCategoryButton.tag);
    
    for(UIButton *button in [_subCategoryContainer subviews]){
        
        if([button isKindOfClass:[UIButton class]] && (button.tag != selectedSubCategoryButton.tag)){
            button.selected = NO;
            
            for(UIImageView *image in [button subviews]){
                if([image isKindOfClass:[UIImageView class]]){
                    image.image = _inactiveImage;
                }
            }
        }
        
    }
    
}

#pragma mark - Distance Slider

-(void)addDistanceSlider{
    
    UILabel *distanceNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(20, 70, 100, 20)];
    [distanceNameLabel setText:@"Distance (miles): "];
    [distanceNameLabel setTextColor:[UIColor grayColor]];
    [distanceNameLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:13.0]];
    [_mainContainer addSubview:distanceNameLabel];
    
    _distanceLabel = [[UILabel alloc]initWithFrame:CGRectMake(270, 70, 60, 20)];
    [_distanceLabel setText:@"5.0"];
    [_distanceLabel setTextColor:[UIColor grayColor]];
    [_distanceLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:14.0]];
    [_mainContainer addSubview:_distanceLabel];

    slider = [[UICustomSlider alloc] initWithFrame:CGRectMake( 120, 65, 150, 30)];
    [slider addTarget:self action:@selector(distanceSliderAction:) forControlEvents:UIControlEventValueChanged];
    [_mainContainer addSubview:slider];
}

-(IBAction)distanceSliderAction:(id)sender
{
//    UISlider *slider = (UISlider *)sender;
    [_distanceLabel setText:[NSString stringWithFormat:@"%.1f", slider.value]];
}

#pragma mark - Show Coupon Only

-(void)addShowCouponOnlySwitch
{
    
    UILabel *showCouponOnlyLabel = [[UILabel alloc]initWithFrame:CGRectMake(20, 115, 200, 20)];
    [showCouponOnlyLabel setText:@"Show coupons/discounts only: "];
    [showCouponOnlyLabel setTextColor:[UIColor grayColor]];
    [showCouponOnlyLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:13.0]];
    [_mainContainer addSubview:showCouponOnlyLabel];
    
    _showCouponOnly = [[UISwitch alloc]initWithFrame:CGRectMake(230, 110, 0,0)];
    _showCouponOnly.on = NO;
//    _showCouponOnly.onTintColor = [UIColor colorWithRed:(27.0/255.0) green:(112.0/255.0) blue:(239.0/255.0) alpha:1];
    _showCouponOnly.onTintColor = [UIColor colorWithRed:(108.0/255.0) green:(168.0/255.0) blue:(164.0/255.0) alpha:1];
    [self.view addSubview:_showCouponOnly];
    [_mainContainer addSubview:_showCouponOnly];
}

#pragma mark - ViewResultButtonClick

-(void)ViewResultButtonClick{
    
    [NSThread detachNewThreadSelector:@selector(startActivityIndicator) toTarget:_activityIndicator withObject:nil];

    int subCategoryId;
    NSString *subCategoryName;
    
    for(UIButton *subview in [_subCategoryContainer subviews]) {
        if([subview isKindOfClass:[UIButton class]] && subview.selected){
            subCategoryId = (int)subview.tag;
            subCategoryName = subview.titleLabel.text;
        }
    }
    
    ViewResultViewController *viewResult = [ViewResultViewController new];
    
    //pass values
    viewResult.distance = slider.value;
    viewResult.subCategoryName = subCategoryName;
    viewResult.subCategoryId = subCategoryId;
    viewResult.showCouponOnly = _showCouponOnly.on;
    viewResult.mainCategory = mainCategory;
    
    [self presentViewController:viewResult animated:YES completion:nil];
    
    [NSThread detachNewThreadSelector:@selector(stopActivityIndicator) toTarget:_activityIndicator withObject:nil];

}

#pragma mark - Search Method

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    //    [_leftMenu menuCollapse];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    NSLog(@"Start Search");
    NSLog(@"search term %@", searchBar.text);
    
    SearchResultViewController *viewResultFromSearch = [SearchResultViewController new];
    
    //pass values
    viewResultFromSearch.distance = 0;
    viewResultFromSearch.subCategoryName = @"All";
    viewResultFromSearch.subCategoryId = 33;
    viewResultFromSearch.searchText = searchBar.text;
    viewResultFromSearch.mainCategory = @"Shopping";
    
    
    [self presentViewController:viewResultFromSearch animated:YES completion:nil];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

-(void)clickBackButton{
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
