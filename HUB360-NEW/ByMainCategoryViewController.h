//
//  ByMainCategoryViewController.h
//  HUB360-NEW
//
//  Created by Oyuka on 1/13/14.
//  Copyright (c) 2014 Oyuka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SideNavigationDelegate.h"
#import "UICustomSlider.h"

@interface ByMainCategoryViewController : UIViewController <SideNavigationDelegate, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, UISearchBarDelegate>
{
    UICustomSlider *slider;
}
@property NSString *mainCategory;
@property int mainCategoryID;

@end
