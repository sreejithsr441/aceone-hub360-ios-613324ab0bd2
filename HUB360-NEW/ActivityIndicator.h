//
//  ActivityIndicator.h
//  HUB360-NEW
//
//  Created by Oyuka on 1/16/14.
//  Copyright (c) 2014 Oyuka. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivityIndicator : UIActivityIndicatorView

-(void)startActivityIndicator;
-(void)stopActivityIndicator;

@end
