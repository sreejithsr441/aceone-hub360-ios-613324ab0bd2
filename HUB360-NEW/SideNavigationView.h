//
//  SideNavigationView.h
//  HUB360-NEW
//
//  Created by Oyuka on 12/29/13.
//  Copyright (c) 2013 Oyuka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SideNavigationDelegate.h"

@interface SideNavigationView : UITableView

@property(nonatomic, assign) id<SideNavigationDelegate>delegate;

- (id)initWithDelegate:(id<SideNavigationDelegate>)delegate frame:(CGRect)frame;
-(NSArray*)listMenuItems;
-(NSArray*)listMenuItemIcons;
-(int)menucount;
@end
