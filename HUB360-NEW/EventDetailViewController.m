//
//  EventDetailViewController.m
//  HUB360-NEW
//
//  Created by Oyuka on 12/27/13.
//  Copyright (c) 2013 Oyuka. All rights reserved.
//

#import "EventDetailViewController.h"
#import "hub360Config.h"
#import "SideNavigationView.h"
#import "Home.h"
#import "AdvertisementBlock.h"
#import "SRUtility.h"
@interface EventDetailViewController ()

@end

@implementation EventDetailViewController{
    hub360Config *_config;
    SideNavigationView *_sideNav;
    NavigationControllerBuilder *_navigationBar;
    UIView *_mainContainer;
    AdvertisementBlock *_advertisementBanner;
}
@synthesize EventName;
@synthesize EventDesc;
@synthesize EventDate;
@synthesize EventDateFull;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    _config = [hub360Config new];
    
//    _sideNav = [[SideNavigationView alloc]initWithDelegate:self frame:CGRectMake(0, 20, [_config width]-60, [_config height])];
//    [_sideNav setDataSource:self];
//    [self.view addSubview:_sideNav];
    
    
    _mainContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 20, [_config width], [_config height])];
    [_mainContainer setBackgroundColor:[UIColor colorWithRed:(237.0/255.0) green:(237.0/255.0) blue:(237.0/255.0) alpha:(1)]];
    [self.view addSubview:_mainContainer];
    
    UILabel *EventDateLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 70, 60, 60)];
    EventDateLabel.backgroundColor = [UIColor colorWithRed:(68.0/255.0) green:(98.0/255.0) blue:(204.0/255.0) alpha:1];
    EventDateLabel.textColor = [UIColor whiteColor];
    [EventDateLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:24.0f]];
    [EventDateLabel setTextAlignment:NSTextAlignmentCenter];
    EventDateLabel.numberOfLines = 2;
    EventDateLabel.text = EventDate;
    [_mainContainer addSubview: EventDateLabel];
    
    UILabel *EventNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(90, 70, 210, 60)];
    EventNameLabel.numberOfLines = 3;
    [EventNameLabel setTextColor:[UIColor colorWithRed:(68.0/255.0) green:(98.0/255.0) blue:(204.0/255.0) alpha:1]];
    [EventNameLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:16.0f]];
    [EventNameLabel setText:EventName];
    [EventNameLabel setTextAlignment:NSTextAlignmentLeft];
    [_mainContainer addSubview: EventNameLabel];
    
    UITextView *EventDescriptionText = [[UITextView alloc]initWithFrame:CGRectMake(20, 140, 280, 295)];
    EventDescriptionText.text = EventDesc;
    
    [EventDescriptionText setBackgroundColor:[UIColor whiteColor]];
    [EventDescriptionText setTextColor:[UIColor darkGrayColor]];
    [EventDescriptionText setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:14.0f]];
    EventDescriptionText.editable = NO;
    EventDescriptionText.scrollEnabled = YES;
    [_mainContainer addSubview: EventDescriptionText];
    
    UIButton *addToCalendar = [[UIButton alloc]initWithFrame:CGRectMake(60, 445, 200, 40)];
    [addToCalendar setBackgroundColor:[UIColor colorWithRed:(202.0/255.0) green:(52.0/255.0) blue:(36.0/255.0) alpha:1]];
    [addToCalendar setTintColor:[UIColor whiteColor]];
    [addToCalendar setTitle:@"Add to Calendar" forState:UIControlStateNormal];
    [addToCalendar addTarget:self action:@selector(addToCalendar) forControlEvents:UIControlEventTouchUpInside];
    [_mainContainer addSubview:addToCalendar];
    
    // adding navigation controller
    _navigationBar = [[NavigationControllerBuilder alloc]initWithFrame:CGRectMake(0, 0, 320, 45)];
    [_navigationBar addNavigationContoller: @"Event" :YES];
    [_mainContainer addSubview:_navigationBar];
    
    // Banner add
    _advertisementBanner = [AdvertisementBlock new];
    [_mainContainer addSubview:_advertisementBanner];
    [_advertisementBanner getBannerAds];
    
    if([_config height] < 500){
        [_advertisementBanner setFrame:CGRectMake(0, 410, 320, 50)];
        [EventDescriptionText setFrame:CGRectMake(20, 145, 280, 200)];
        [addToCalendar setFrame:CGRectMake(60, 360, 200, 40)];
    }
    
    [_mainContainer bringSubviewToFront:addToCalendar];
}

-(void)addToCalendar
{
    
    if ([SRUtility reachable])
    {
        EKEventStore *es = [[EKEventStore alloc] init];
        EKAuthorizationStatus authorizationStatus = [EKEventStore authorizationStatusForEntityType:EKEntityTypeEvent];
        BOOL needsToRequestAccessToEventStore = (authorizationStatus == EKAuthorizationStatusNotDetermined);
        
        if (needsToRequestAccessToEventStore) {
            [es requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
                if (granted) {
                    // Access granted
                    [self performCalendarActivity:es];
                } else {
                    // Denied
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                    message:@"Please check your Privacy Settings to enable Calendar Access."
                                                                   delegate:self
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                    [alert show];
                }
            }];
        } else {
            BOOL granted = (authorizationStatus == EKAuthorizationStatusAuthorized);
            if (granted) {
                // Access granted
                [self performCalendarActivity:es];
            } else {
                // Denied
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                message:@"Please check your Privacy Settings to enable Calendar Access."
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }
        }

        
    }
    else
    {
        UIAlertView *alret=[[UIAlertView alloc]initWithTitle:@"Network Error" message:@"Your Network Is Disconnected. Please Check Your Network Connection And Try Again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alret show];
    }
    
    
}

-(void)performCalendarActivity:(EKEventStore *)eventStore{
    
    // access granted
    NSLog(@"granted");
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"MM-dd-yyyy hh:mm a"];
    NSDate *date = [df dateFromString: EventDateFull];
    
    EKEvent *event  = [EKEvent eventWithEventStore:eventStore];
    event.title     = EventName;
    event.startDate = date;
    event.endDate = date;
    event.notes     = EventDesc;
    [event setCalendar:[eventStore defaultCalendarForNewEvents]];
    
    EKEventEditViewController *eventViewController = [[EKEventEditViewController alloc] init];
    eventViewController.event = event;
    eventViewController.eventStore = eventStore;
    eventViewController.editViewDelegate = self;
    
    [self presentViewController:eventViewController animated:YES completion:nil];
    
}

-(void)eventEditViewController:(EKEventEditViewController *)controller didCompleteWithAction:(EKEventEditViewAction)action{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

@end
