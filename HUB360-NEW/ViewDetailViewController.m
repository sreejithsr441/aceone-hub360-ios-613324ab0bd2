//
//  ViewDetailViewController.m
//  HUB360-NEW
//
//  Created by Oyuka on 1/10/14.
//  Copyright (c) 2014 Oyuka. All rights reserved.
//

#import "ViewDetailViewController.h"
#import "ViewResultViewController.h"
#import "EventsViewController.h"
#import "NearByMeViewController.h"
#import "hub360Config.h"
#import "Home.h"
#import "UIImageView+Network.h"

@interface ViewDetailViewController ()
@end

@implementation ViewDetailViewController{
    
    UIActivityIndicatorView *_activityIndicator;
    Carousel *_carouselView;
    UIView *_mainContainer;
    hub360Config *_config;
    NavigationControllerBuilder *_navigationBar;
    
    NSMutableArray *_resultWithOfferArray;
    NSMutableArray *_resultWithoutOfferArray;
    
    UILabel *_withOfferLabel;
    UILabel *_withoutOfferLabel;
    
    UIScrollView *_detailContainer;
    
    //company details
    UIImageView *_mainCategoryIconView;
    UILabel *_businessNameLabel;
    UILabel *_businessAddressLabel;
    
    UILabel *_phoneNumberLabel;
    UILabel *_ratingLabel;
    UILabel *_webAddLabel;
    UITextView *_reviewTextView;
 
    UIImage *_couponImage;
    UIImageView *_couponImageView;
    UIImageView *_downArrow;
    UIImageView *_upArrow;
    
    CLLocationManager *_locationManager;
}

@synthesize withCoupon;
@synthesize mainCategoryName;
@synthesize subCategoryName;
@synthesize subCategoryId;

@synthesize businessName;
@synthesize businessAddress;
@synthesize businessDistance;
@synthesize phoneNumber;
@synthesize businessHours;
@synthesize trending;
@synthesize rating;
@synthesize latestReview;
@synthesize couponImageURL;
@synthesize offer;
@synthesize webAddress;
@synthesize mapAddress;
@synthesize menuLink;
@synthesize companyID;
@synthesize offerID;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // location
    //---- For getting current gps location
    _locationManager = [CLLocationManager new];
    _locationManager.delegate = self;
    _locationManager.distanceFilter = 100.0; // Will notify the LocationManager every 100 meters
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    [_locationManager startUpdatingLocation];
    
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0){
        [_locationManager requestAlwaysAuthorization];
    }

    
    _config = [hub360Config new];
    
    // main container
    _mainContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 20, [_config width], [_config height])];
    _mainContainer.backgroundColor =[UIColor colorWithRed:(237.0/255.0) green:(237.0/255.0) blue:(237.0/255.0) alpha:(1)];
    [self.view addSubview:_mainContainer];
    
    // scroll view
    _carouselView = [[Carousel alloc]initWithFrame:CGRectMake(0, 0, [_config width], [_config height]-45)];
    
    [_carouselView setBackgroundColor:[UIColor clearColor]];
    _carouselView.scrollView.showsVerticalScrollIndicator = YES;
    [_carouselView.scrollView setDelegate:self];
    [_carouselView.scrollView setFrame:CGRectMake(0, 0, 320, [_config height])];
    _carouselView.scrollView.clipsToBounds = YES;
    [_mainContainer addSubview:_carouselView];
    [_carouselView.scrollView flashScrollIndicators];
    
    // display company detail
    
    if([latestReview isEqualToString:@""] || latestReview == nil){
        latestReview = @"No Review Available";
    }
    
    // 1. coupon image
    [self showCouponImage];
    

    // adding navigation controller
    _navigationBar = [[NavigationControllerBuilder alloc]initWithFrame:CGRectMake(0, 0, 320, 45)];
    
    [_navigationBar addNavigationContoller:[NSString stringWithFormat:@"%@", businessName] :YES];
    [_mainContainer addSubview:_navigationBar];
    
    [self addButtons];
    [self addDetails];
    
}

-(void)addButtons{
    
    //save to favorites
    UIButton *saveToFavoritesButton = [[UIButton alloc]initWithFrame:CGRectMake(10, _couponImageView.frame.origin.y+_couponImageView.frame.size.height +5, 145, 20)];
    [saveToFavoritesButton setTitle:@"Save to Favorites" forState:UIControlStateNormal];
    [saveToFavoritesButton setBackgroundColor:[UIColor colorWithRed:(245.0/255.0) green:(76.0/255.0) blue:(10.0/255.0) alpha:1]];
    saveToFavoritesButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:12.0];
    [saveToFavoritesButton addTarget:self action:@selector(addToFavorites) forControlEvents:UIControlEventTouchUpInside];
    [_carouselView.scrollView addSubview:saveToFavoritesButton];
    
    //menu button
    NSURL* url = [NSURL URLWithString:menuLink];
    if(url != nil && ![menuLink isEqualToString:@"" ]){
        
        UIButton *menuButton = [[UIButton alloc]initWithFrame:CGRectMake(165, _couponImageView.frame.origin.y+_couponImageView.frame.size.height +5, 145, 20)];
        [menuButton setTitle:@"Menu" forState:UIControlStateNormal];
        [menuButton setBackgroundColor:[UIColor colorWithRed:(245.0/255.0) green:(76.0/255.0) blue:(10.0/255.0) alpha:1]];
        menuButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:12.0];
        [menuButton addTarget:self action:@selector(openMenuLink) forControlEvents:UIControlEventTouchUpInside];
        [_carouselView.scrollView addSubview:menuButton];
        
    }else{
        
        //show favorites button in the center
        [saveToFavoritesButton setFrame:CGRectMake(92.5, _couponImageView.frame.origin.y+_couponImageView.frame.size.height+5, 145, 20)];
    }
}

-(void)addDetails{
    
    // detail in a scroll view
    _detailContainer = [[UIScrollView alloc]initWithFrame:CGRectMake(0,
                                                                     _couponImageView.frame.origin.y+_couponImageView.frame.size.height+30,
                                                                     320,
                                                                     [_config height] - (_couponImageView.frame.origin.y+_couponImageView.frame.size.height+30))];
    
    [_detailContainer setBackgroundColor:[UIColor colorWithRed:(245.0/255.0) green:(245.0/255.0) blue:(245.0/255.0) alpha:(1)]];
    _detailContainer.scrollEnabled = YES;
    _detailContainer.contentSize = CGSizeMake(300, 340);
    
    [_carouselView.scrollView addSubview:_detailContainer];
    
    NSString *mainCatImage;
    
    if([mainCategoryName isEqualToString:@"Dining"]){
        mainCatImage = withCoupon ? @"diningwithoffer-icon@2x" : @"diningwithoutoffer-icon@2x";
    }else if([mainCategoryName isEqualToString:@"Services"]){
        mainCatImage = withCoupon ? @"servicesred-icon@2x": @"serviceswithoutoffer-icon@2x";
    }else{
        mainCatImage = withCoupon ? @"shoppingwithoffer-icon@2x": @"shoppingwithoutoffer-icon@2x";
    }
    
    _mainCategoryIconView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:mainCatImage]];
    [_mainCategoryIconView setFrame:CGRectMake(10, 10, 30, 30)];
    [_detailContainer addSubview:_mainCategoryIconView];
    
    _businessNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(50, 10, 240, 30)];
    [_businessNameLabel setTextColor:[UIColor darkGrayColor]];
    _businessNameLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:14.0];
    _businessNameLabel.numberOfLines = 2;
    [_businessNameLabel setText:businessName];
    [_detailContainer addSubview:_businessNameLabel];
    
    // separator line
    [_detailContainer addSubview:[self addLine:CGRectMake(45, 10, 1, 350)]];
    
    // address
    _businessAddressLabel = [self addLabel:businessAddress :@"pin-icon@2x" :CGRectMake(50, 45, 270, 21) :[UIColor whiteColor]];
    _businessAddressLabel.userInteractionEnabled = YES;
    _businessAddressLabel.textColor = [UIColor colorWithRed:(13/255.0) green:(111/255.0) blue:(180/255.0) alpha:1];
    UITapGestureRecognizer *addressLinkGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(goToAddress:)];
    addressLinkGesture.delegate = self;
    [_businessAddressLabel addGestureRecognizer:addressLinkGesture];
    
    if(phoneNumber == NULL || [phoneNumber isEqualToString:@""]){
        phoneNumber = @"NaN";
    }
    
    if(withCoupon){
        
        // down arrow
        _downArrow = [[UIImageView alloc]initWithFrame:CGRectMake(12.5, 130, 25, 25)];
        _downArrow.image = [UIImage imageNamed:@"down-arrow@2x"];
        _downArrow.tag = 888;
        [_detailContainer addSubview:_downArrow];
        
        _downArrow.userInteractionEnabled = YES;
        UITapGestureRecognizer *tapGestureToScrollDown = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(scroll:)];
        tapGestureToScrollDown.delegate = self;
        [_downArrow addGestureRecognizer:tapGestureToScrollDown];
        
        // down arrow
        _upArrow = [[UIImageView alloc]initWithFrame:CGRectMake(12.5, 300, 25, 25)];
        _upArrow.image = [UIImage imageNamed:@"up-arrow@2x"];
        _upArrow.tag = 889;
        [_detailContainer addSubview:_upArrow];
        
        _upArrow.userInteractionEnabled = YES;
        UITapGestureRecognizer *tapGestureToScrollUp = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(scroll:)];
        tapGestureToScrollUp.delegate = self;
        [_upArrow addGestureRecognizer:tapGestureToScrollUp];
        
        [self showDetailWithCoupon];
        
        
//        if([_config height]<500){
//            [_downArrow setFrame:CGRectMake(12.5, 50, 25, 25)];
//            [_detailContainer setFrame:CGRectMake(0, 325, 320, 80)];
//        }
        
    }else{
//        [_detailContainer setFrame:CGRectMake(0, 55, 320, 340)];
        [self showDetailWithoutCoupon];
    }
}

#pragma mark - add label with icon and text
-(UILabel* )addLabel:(NSString*)labelText :(NSString*)iconImageName :(CGRect)frame :(UIColor*)labelColor{
    
    UIView *labelView = [[UIView alloc]initWithFrame:frame];
    [labelView setBackgroundColor:labelColor];
    [_detailContainer addSubview:labelView];
    
    UILabel *labelViewText = [[UILabel alloc]initWithFrame:CGRectMake(30, 0, 240, 20)];
    [labelViewText setTextColor:[UIColor colorWithRed:(54/255.0) green:(54/255.0) blue:(54/255.0) alpha:(1)]];
    labelViewText.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:12.0];
    [labelViewText setText:labelText];
    [labelView addSubview:labelViewText];
    
    UIImageView *labelViewIcon = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
    labelViewIcon.image = [UIImage imageNamed:iconImageName];
    [labelView addSubview:labelViewIcon];
    
    return labelViewText;
    
}

#pragma mark - WithCoupon detail 

-(void)showDetailWithCoupon{

    // hours
    [self addLabel:businessHours :@"clock-icon@2x" :CGRectMake(50, 66, 270, 21) :[UIColor clearColor]];
    
    // phone no
    _phoneNumberLabel = [self addLabel:phoneNumber :@"phone-icon@2x" :CGRectMake(50, 87, 270, 21) :[UIColor whiteColor]];
    _phoneNumberLabel.userInteractionEnabled = YES;
    [_phoneNumberLabel setTextColor:[UIColor colorWithRed:(13/255.0) green:(111/255.0) blue:(180/255.0) alpha:1]];
    UITapGestureRecognizer *phoneTapGesture =  [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dialPhoneNumber:)];
    phoneTapGesture.delegate = self;
    [_phoneNumberLabel addGestureRecognizer:phoneTapGesture];

    // rating
    [self addLabel:rating :@"rating-icon@2x" :CGRectMake(50, 108, 270, 21) :[UIColor clearColor]];
    
    // wed address
    _webAddLabel = [self addLabel:webAddress :@"www-icon@2x" :CGRectMake(50, 129, 270, 21) :[UIColor whiteColor]];
    _webAddLabel.userInteractionEnabled = YES;
    _webAddLabel.textColor = [UIColor colorWithRed:(13/255.0) green:(111/255.0) blue:(180/255.0) alpha:1];
    UITapGestureRecognizer *linkTapGesture =  [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(openURL:)];
    linkTapGesture.delegate = self;
    [_webAddLabel addGestureRecognizer:linkTapGesture];
    
    
    // trending
    [self addLabel:@"Trending" :@"" :CGRectMake(50, 150, 270, 21) :[UIColor clearColor]];
    
    UILabel *trendingLabel = [[UILabel alloc]initWithFrame:CGRectMake(80, 171, 270, 30)];
    [trendingLabel setTextColor:[UIColor grayColor]];
    trendingLabel.numberOfLines = 2;
    trendingLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:12.0];
    [trendingLabel setText:offer];
    [_detailContainer addSubview:trendingLabel];

    
    // review
    [self addLabel:@"Latest Review" :@"" :CGRectMake(50, 201, 270, 21) :[UIColor whiteColor]];
    
    _reviewTextView = [[UITextView alloc]initWithFrame:CGRectMake(78, 222, 230, 150)];
    [_reviewTextView setTextColor:[UIColor grayColor]];
    _reviewTextView.editable = FALSE;
    _reviewTextView.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:12.0];
    [_reviewTextView setBackgroundColor:[UIColor clearColor]];
    _reviewTextView.contentInset = UIEdgeInsetsMake(-9, 0, 0, 0);
    [_reviewTextView setText:latestReview];
    [_detailContainer addSubview:_reviewTextView];

}

#pragma mark - WithCoupon detail

-(void)showDetailWithoutCoupon{
    
    // phone no
    _phoneNumberLabel = [self addLabel:phoneNumber :@"phone-icon@2x" :CGRectMake(50, 66, 270, 21) :[UIColor clearColor]];
    _phoneNumberLabel.userInteractionEnabled = YES;
    _phoneNumberLabel.textColor = [UIColor colorWithRed:(13/255.0) green:(111/255.0) blue:(180/255.0) alpha:1];
    UITapGestureRecognizer *phoneTapGesture =  [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dialPhoneNumber:)];
    phoneTapGesture.delegate = self;
    [_phoneNumberLabel addGestureRecognizer:phoneTapGesture];

    // rating
    [self addLabel:rating :@"rating-icon@2x" :CGRectMake(50, 87, 270, 21) :[UIColor whiteColor]];

    // wed address
    _webAddLabel = [self addLabel:webAddress :@"www-icon@2x" :CGRectMake(50, 108, 270, 21) :[UIColor clearColor]];
    _webAddLabel.userInteractionEnabled = YES;
     _webAddLabel.textColor = [UIColor colorWithRed:(13/255.0) green:(111/255.0) blue:(180/255.0) alpha:1];
    UITapGestureRecognizer *linkTapGesture =  [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(openURL:)];
    linkTapGesture.delegate = self;
    [_webAddLabel addGestureRecognizer:linkTapGesture];
    
    // review
    [self addLabel:@"Latest Review" :@"" :CGRectMake(50, 129, 270, 21) :[UIColor whiteColor]];
    
    _reviewTextView = [[UITextView alloc]initWithFrame:CGRectMake(78, 150, 230, 200)];
    [_reviewTextView setTextColor:[UIColor grayColor]];
    _reviewTextView.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:12.0];
    [_reviewTextView setBackgroundColor:[UIColor clearColor]];
    _reviewTextView.contentInset = UIEdgeInsetsMake(-9, 0, 0, 0);
    [_reviewTextView setText:latestReview];
    _reviewTextView.editable = FALSE;
    [_detailContainer addSubview:_reviewTextView];

}

#pragma mark - Helper Methods

-(void)scroll:(UIGestureRecognizer*)recognizer{
    
    NSLog(@"scroll");
    UIImageView *arrowImage = (UIImageView*)[recognizer view];
    
    if(arrowImage.tag == 888){
        CGPoint bottomOffset = CGPointMake(0, _detailContainer.contentSize.height - _detailContainer.bounds.size.height);
        [_detailContainer setContentOffset:bottomOffset animated:YES];
    }else{
        [_detailContainer setContentOffset:CGPointMake(_detailContainer.contentOffset.x, 0) animated:YES];
    }
    
}

-(void)makeActivityIndicator
{
    //activity inidicator
    _activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _activityIndicator.frame = CGRectMake(0,0,200,200);
    CGAffineTransform transform = CGAffineTransformMakeScale(1.5f, 1.5f);
    _activityIndicator.transform = transform;

    _activityIndicator.center = CGPointMake (self.view.bounds.size.width * 0.5F, self.view.bounds.size.height * 0.3F);
    
    _activityIndicator.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin);
    _activityIndicator.hidden = FALSE;
    [_mainContainer addSubview:_activityIndicator];
    [self startActivityIndicator];
}

-(void)startActivityIndicator
{
    [_activityIndicator startAnimating];
}
-(void)stopActivityIndicator
{
    sleep(1.5);
    [_activityIndicator stopAnimating];
}

-(NSString *)replaceAscii:(NSString*)string{
    NSString *s = string;
    s = [s stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    return s;
}

-(UILabel*)addLine:(CGRect)frame{
    // border bottom
    UILabel *line = [[UILabel alloc]initWithFrame:frame];
    [line setBackgroundColor:[UIColor colorWithRed:(215/255.0) green:(215/255.0) blue:(215/255.0) alpha:1]];
    return line;
}

-(void)selectMenuItem:(NSInteger)index{
    [_navigationBar showLeftNav];
    
    // change view controller
    
    switch (index) {
        case 0:
        {
            Home *homePage = [Home new];
            [self presentViewController:homePage animated:YES completion:nil];
        }
            break;
            
        default:
            break;
    }
    
}

-(void)clickBackButton{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)goToAddress:(UIGestureRecognizer*)recognizer{
    UILabel *thisLabel = (UILabel*)[recognizer view];
    
    
    if(thisLabel.text != nil && ![thisLabel.text isEqualToString:@"NaN"]){
        
        float latitude = _locationManager.location.coordinate.latitude;
        float longitude = _locationManager.location.coordinate.longitude;
            
            NSLog(@"Coordinate valid");
            NSString* url = [NSString stringWithFormat: @"http://maps.google.com/maps?saddr=%f,%f&daddr=%@",
                             latitude, longitude,
                             [thisLabel.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            [[UIApplication sharedApplication] openURL: [NSURL URLWithString: url]];

    }
}

-(void)dialPhoneNumber:(UIGestureRecognizer*)recognizer{
    
    UILabel *thisLabel = (UILabel*)[recognizer view];
    if(thisLabel.text != nil && ![thisLabel.text isEqualToString:@"NaN"]){
            NSLog(@"ph: %@", thisLabel.text);
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",thisLabel.text]]];
    }
    
}

-(void)openURL:(UIGestureRecognizer*)recognizer{
    NSLog(@"tap tap");
    UILabel *thisLabel =(UILabel*)[recognizer view];
    if(thisLabel.text != nil && ![thisLabel.text isEqualToString:@"NaN"]){
        NSLog(@"link: %@", thisLabel.text);
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",thisLabel.text]]];
    }
    
}

-(void)showCouponImage{
    
    [self makeActivityIndicator];

        _couponImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 45, 0, 0)];
    if(withCoupon){
        
        _couponImageView.frame = CGRectMake(0, 45, 320, 280);
        NSString *tmpimageurl = couponImageURL;
        UIImage *imgtmp = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:tmpimageurl]]];
        
        NSArray *cachingkey = [tmpimageurl componentsSeparatedByString:@"/"];
        
        [_couponImageView loadImageFromURL:[NSURL URLWithString:couponImageURL]
                          placeholderImage:[UIImage imageNamed:@"default"]
                                cachingKey:[cachingkey lastObject]
                                     frame:CGRectMake(0, 45, 320, 280)];
        
        _couponImageView.contentMode = UIViewContentModeScaleAspectFit;
        
        [_couponImageView setFrame:CGRectMake(_couponImageView.frame.origin.x,
                                             _couponImageView.frame.origin.y,
                                             _couponImageView.frame.size.width,
                                             _couponImageView.frame.size.width*imgtmp.size.height/imgtmp.size.width)];
    
        [_couponImageView setBackgroundColor:[UIColor whiteColor]];
        [_carouselView.scrollView addSubview:_couponImageView];
    }
    
    [self stopActivityIndicator];
    
}

-(void)openMenuLink{
    
    NSURL* url = [NSURL URLWithString:menuLink];
    if(url!=nil){
        NSLog(@"menu - %@", menuLink);
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:menuLink]];
    }
}

-(void)addToFavorites{

    UIDevice *myDevice = [UIDevice currentDevice];
	NSString *deviceNumber = [[myDevice identifierForVendor] UUIDString];
    NSString *globaluid=deviceNumber;
    
    unsigned long length = [companyID length];
    //if([f numberFromString: coid] == NULL){
    
    NSString *urlString;
    
    if(length > 7){
        
        urlString = [NSString stringWithFormat:@"http://webdesignjonesboro.com/hub360service/hub360Service.asmx/AddtoFavourites?CoName=%@&Redeemcode=%@&latitude=%@&longitude=%@&username=%@&offer=%@&CoID=25&cat=%@&withoutofferid1=%d&address=%@",
                     businessName,@"",@"",@"",globaluid,@"", subCategoryName, [companyID intValue], businessAddress];
    }else{
        
        urlString = [NSString stringWithFormat:@"http://webdesignjonesboro.com/hub360service/hub360Service.asmx/AddtoFavourites?CoName=%@&Redeemcode=%@&latitude=%@&longitude=%@&username=%@&offer=%@&CoID=%d&cat=%@&withoutofferid1=0&address=",
                     businessName,
                     @"",@"",@"",
                     globaluid,
                     [offer stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding],
                     [companyID intValue],
                     subCategoryName];
    }
	
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	NSMutableURLRequest *req=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
	[req addValue:@"http://webdesignjonesboro.com/hub360service/hub360Service.asmx" forHTTPHeaderField:@"SOAPAction"];
	[req setHTTPMethod:@"GET"];
	
	NSError *error1;
	error1 = [[NSError alloc] init];
	
	NSData *returnData1 = [NSURLConnection sendSynchronousRequest:req returningResponse:nil error:&error1];
	NSString *returnString = [[NSString alloc] initWithData:returnData1 encoding:NSUTF8StringEncoding];
	NSLog(@"%@",returnString);
    
    NSString *successMsg = @"Successfully Add to your favourite";
    NSString *dupliacteMsg = @"duplicate";
    NSString *alertMsg =@"";
    if([returnString rangeOfString:successMsg].location != NSNotFound){
        alertMsg = @"Successfully added to My Favorites";
        
    }else if([returnString rangeOfString:dupliacteMsg].location != NSNotFound){
        
        alertMsg = @"already exists in My Favorites";
    }
    
	UIAlertView *alrt=[[UIAlertView alloc]initWithTitle:businessName
                                                message:alertMsg
                                               delegate:self
                                      cancelButtonTitle:nil
                                      otherButtonTitles:@"Ok",nil];
	[alrt show];

}


@end
