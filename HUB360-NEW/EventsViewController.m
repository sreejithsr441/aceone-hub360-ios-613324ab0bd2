//
//  EventsViewController.m
//  HUB360-NEW
//
//  Created by Oyuka on 12/26/13.
//  Copyright (c) 2013 Oyuka. All rights reserved.
//

#import "EventsViewController.h"
#import "NavigationControllerBuilder.h"
#import "EventDetailViewController.h"
#import "MyFavoritesViewController.h"
#import "hub360Config.h"
#import "SideNavigationView.h"
#import "Home.h"
#import "SearchBox.h"
#import "SearchResultViewController.h"
#import "ActivityIndicator.h"
#import "AdvertisementBlock.h"
#import "SRUtility.h"
@interface EventsViewController ()

@end

@implementation EventsViewController
{
    ActivityIndicator *_activityIndicator;
    NSArray *_eventListArray;
    UITableView *eventListTableView;
    NSXMLParser *xmlParser;
    NSMutableData *eventListMutableData;
    NSDateFormatter *formatter;
    NSString *dateString;
    UITextView *eventDesc;
    UILabel *eventDatelbl;
    UILabel *eventTitle;
    UIView *eventDetailView;
    UIView *_mainContainer;
    hub360Config *_config;
    SideNavigationView *_sideNav;
    NavigationControllerBuilder *_navigationBar;
    SearchBox *_searchBox;
    Carousel *_carouselView;
    AdvertisementBlock *_advertisementBanner;
    AdvertisementBlock *_advertisementFullPage;
    NSString *_eventDateFormatted;
}

-(void)viewDidAppear:(BOOL)animated{
    
    NSUserDefaults *userDefaults=[NSUserDefaults standardUserDefaults];
    NSString *countString = [userDefaults objectForKey:@"saveCountEvents"];
    NSLog(@"Your Count: %@",countString);
    
    if(countString == nil)
        [userDefaults setObject:@"1" forKey:@"saveCountEvents"];
    
    [userDefaults setObject:[NSString stringWithFormat:@"%d", [countString intValue]+1] forKey:@"saveCountEvents"];
    [userDefaults synchronize];
    
    if(([countString integerValue]) % 3 == 0 && countString != nil)
    {
        _advertisementFullPage = [AdvertisementBlock new];
        [_mainContainer addSubview:_advertisementFullPage];
        [_advertisementFullPage getFullAds];
        [userDefaults setObject:@"1" forKey:@"saveCountEvents"];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    _config = [hub360Config new];
    
    _sideNav = [[SideNavigationView alloc]initWithDelegate:self frame:CGRectMake(0, 20, [_config width]-60, [_config height])];
    [_sideNav setDataSource:self];
    [self.view addSubview:_sideNav];
  
    _mainContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 20, [_config width], [_config height])];
    [_mainContainer setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:_mainContainer];

    
    // get today's date
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyy-MM-dd"];
    
    dateString = [formatter stringFromDate:[NSDate date]];
    NSLog(@"date is %@", dateString);
    
    //events
    NSString *sopMessage=[NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                          "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                          "<soap:Body>"
                          "<GetAllEventDetailNew xmlns=\"http://www.hub360.com/\">"
                          "</GetAllEventDetailNew>"
                          "</soap:Body>"
                          "</soap:Envelope>"];
    
    NSLog(@"value is%@",sopMessage);
	
	NSURL *url=[NSURL URLWithString:@"http://webdesignjonesboro.com/hub360service/hub360Service.asmx/GetAllEventDetailNew"];
	NSMutableURLRequest *req=[NSMutableURLRequest requestWithURL:url];
	NSString *msglength=[NSString stringWithFormat:@"%lu",(unsigned long)[sopMessage length]];
	[req addValue:@"text/xml;charset=utf-8"  forHTTPHeaderField:@"Content-Type"];
	[req addValue:@"http://webdesignjonesboro.com/hub360service/hub360Service.asmx" forHTTPHeaderField:@"SOAPAction"];
	
	[req addValue: msglength forHTTPHeaderField:@"Content-Length"];
	[req setHTTPMethod:@"POST"];
	[req setHTTPBody:[sopMessage dataUsingEncoding:NSUTF8StringEncoding]];
	
	NSError *error1;
	error1 = [NSError new];
	
	NSData *returnData1 = [NSURLConnection sendSynchronousRequest:req returningResponse:nil error:&error1];
	NSString *returnString = [[NSString alloc] initWithData:returnData1 encoding:NSUTF8StringEncoding];
	NSLog(@"%@",returnString);
    
    NSRange start;
    NSRange stop;
    
    start = [returnString rangeOfString:@"<string xmlns=\"http://www.hub360nea.com/\">"];
    stop = [returnString rangeOfString: @"</string>"];
    
    unsigned long start1 = start.location+start.length;
    unsigned long start2 = stop.location-start1;
    
    
    NSString *jsonString = [returnString substringWithRange: NSMakeRange (start1, start2)];
    
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *e = nil;
    NSMutableArray *json = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&e];
    NSLog(@"%@", json);
    _eventListArray = [json valueForKey:@"List`1"];
    
    // adding navigation controller
    _navigationBar = [[NavigationControllerBuilder alloc]initWithFrame:CGRectMake(0, 0, 320, 45)];
    [_navigationBar addNavigationContoller:@"Events" :YES];
    [_mainContainer addSubview:_navigationBar];
    
    
    // event table
    eventListTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 45, 320, [_config height]-115)];
    eventListTableView.layer.borderColor = [[UIColor grayColor] CGColor];
    eventListTableView.layer.borderWidth = 1.0f;
    [_mainContainer addSubview:eventListTableView];
    eventListTableView.delegate = self;
    eventListTableView.dataSource = self;
    
    _activityIndicator = [ActivityIndicator new];
    [_mainContainer addSubview:_activityIndicator];
    
    // Banner add
    _advertisementBanner = [AdvertisementBlock new];
    [_mainContainer addSubview:_advertisementBanner];
    [_advertisementBanner getBannerAds];
    
    if([_config height]<500){
         [_advertisementBanner setFrame:CGRectMake(0, 410, 320, 50)];
    }

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if(tableView == eventListTableView){
        
        NSLog(@"count count: %lu", (unsigned long)[_eventListArray count]);
        return [_eventListArray count];
        
    }else{
        
        return [_sideNav menucount];
        
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(tableView == eventListTableView)
        return 80.0;
    else{
        return 40.0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    UITableViewCell *cell;
    
    if(tableView == eventListTableView){

        cell = [tableView dequeueReusableCellWithIdentifier:[[_eventListArray objectAtIndex:indexPath.row] objectForKey:@"eventDate"]];
        
        if (cell == nil){
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:[[_eventListArray objectAtIndex:indexPath.row] objectForKey:@"eventDate"]];
            
            if([_eventListArray count] == 0){
                cell.textLabel.text = @"No events available";
                cell.detailTextLabel.text = @"";
                return cell;
            }else{
                NSLog(@"Events %lu", (unsigned long)[_eventListArray count]);
            }
            
            // ============== add date on each event start===========
            
            //NSLog(@"date is %@", dateString);
            
            NSLog(@"Date: %@",[[_eventListArray objectAtIndex:indexPath.row] objectForKey:@"eventDate"]);
            NSArray *dateExp = [[[_eventListArray objectAtIndex:indexPath.row] objectForKey:@"eventDate"] componentsSeparatedByString:@"-"];
            
            UILabel *lblTemp = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, 60, 60)];
            lblTemp.backgroundColor = [UIColor colorWithRed:(68.0/255.0) green:(98.0/255.0) blue:(204.0/255.0) alpha:1];
            lblTemp.textColor = [UIColor whiteColor];
            [lblTemp setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:24.0f]];
            [lblTemp setTextAlignment:NSTextAlignmentCenter];
            lblTemp.numberOfLines = 2;
            
            lblTemp.text = [NSString stringWithFormat:@"%@ %@",[self formatMonth:dateExp[1]], dateExp[2]];
            _eventDateFormatted = [NSString stringWithFormat:@"%@-%@-%@",dateExp[1], dateExp[2], dateExp[0]];
            
            //    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 35)];
            //    imgView.image = [UIImage imageNamed:@"calendar.png"];
            
            // ============== add date on each event end===========
            
            
            [cell.contentView addSubview:lblTemp];
            
            cell.indentationLevel = 7;
            cell.accessoryType =  UITableViewCellAccessoryDisclosureIndicator;
            //    cell.imageView.image = imgView.image;
            
            cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14.0f];
            cell.textLabel.text = [self replaceAscii:[[_eventListArray objectAtIndex:indexPath.row] objectForKey:@"eventName"]];
            cell.textLabel.numberOfLines = 2;
            cell.textLabel.textColor = [UIColor grayColor];
            
            cell.accessibilityLabel = [NSString stringWithFormat:@"%@ %@",[self formatMonth:dateExp[1]], dateExp[2]];
            cell.detailTextLabel.text = [[_eventListArray objectAtIndex:indexPath.row] objectForKey:@"eventDesc"];
            cell.detailTextLabel.textColor = [UIColor colorWithRed:(68.0/255.0) green:(98.0/255.0) blue:(204.0/255.0) alpha:1];
            cell.detailTextLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:12.0f];
            cell.detailTextLabel.numberOfLines = 2;
        }

    }else{
    
        NSArray *list = [[NSArray alloc]initWithArray:[_sideNav listMenuItems]];
        NSArray *iconList = [[NSArray alloc]initWithArray:[_sideNav listMenuItemIcons]];
        
        if(indexPath.row == 0){
            cell = [tableView dequeueReusableCellWithIdentifier:@"searchBox"];
        }else{
            cell = [tableView dequeueReusableCellWithIdentifier:[list objectAtIndex:indexPath.row-1]];
        }
        
        
        if (cell == nil){
            
            if(indexPath.row == 0){
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"searchBox"];
                cell.backgroundColor = [UIColor clearColor];
                
                 _searchBox = [[SearchBox alloc]initWithFrame:CGRectMake(0, 0, cell.frame.size.width-60, cell.frame.size.height)];
                _searchBox.delegate = self;
                [cell.contentView addSubview:_searchBox];
                
            }else{
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:[list objectAtIndex:indexPath.row-1]];
                
                cell.textLabel.text = [list objectAtIndex:indexPath.row-1];
                [cell.textLabel setTextColor:[UIColor whiteColor]];
                cell.backgroundColor = [UIColor clearColor];
                
                UIImageView *accessoryImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 12.5, 19)];
                [accessoryImage setImage:[UIImage imageNamed:@"menu_cell_indicator"]];
                cell.accessoryView = accessoryImage;
                [cell.imageView setImage:[UIImage imageNamed:[iconList objectAtIndex:indexPath.row-1]]];
            }
            
        }

    }
    
    return cell;
}

-(NSString *)formatMonth:(NSString *)month{
    
    NSLog(@"%@", month);
    NSString *monthInFormat;
    switch ([month intValue]) {
        case 1:
            monthInFormat = @"JAN";
            break;

        case 2:
            monthInFormat = @"FEB";
            break;
            
        case 3:
            monthInFormat = @"MAR";
            break;
            
        case 4:
            monthInFormat = @"APR";
            break;
            
        case 5:
            monthInFormat = @"MAY";
            break;
            
        case 6:
            monthInFormat = @"JUN";
            break;
            
        case 7:
            monthInFormat = @"JUL";
            break;
            
        case 8:
            monthInFormat = @"AUG";
            break;
            
        case 9:
            monthInFormat = @"SEP";
            break;
            
        case 10:
            monthInFormat = @"OCT";
            break;
            
        case 11:
            monthInFormat = @"NOV";
            break;
            
        case 12:
            monthInFormat = @"DEC";
            break;
            
        default:
            break;
    }
    return monthInFormat;
}

-(NSString *)replaceAscii:(NSString*)string{
    NSString *s = string;
    s = [s stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    return s;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([SRUtility reachable])
    {
        
    
       [NSThread detachNewThreadSelector:@selector(startActivityIndicator) toTarget:_activityIndicator withObject:nil];
       if(tableView == eventListTableView)
       {
        
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        
        EventDetailViewController *eventDetail = [EventDetailViewController new];
        eventDetail.EventName = cell.textLabel.text;
        eventDetail.EventDesc = cell.detailTextLabel.text;
        eventDetail.EventDate = cell.accessibilityLabel;
        
        NSLog(@"Date: %@",[[_eventListArray objectAtIndex:indexPath.row] objectForKey:@"eventDate"]);
        NSArray *dateExp = [[[_eventListArray objectAtIndex:indexPath.row] objectForKey:@"eventDate"] componentsSeparatedByString:@"-"];
        _eventDateFormatted = [NSString stringWithFormat:@"%@-%@-%@ 08:00 AM",dateExp[1], dateExp[2], dateExp[0]];
        
        eventDetail.EventDateFull = _eventDateFormatted;

        NSUserDefaults *userDefaults=[NSUserDefaults standardUserDefaults];
        NSString *countString = [userDefaults objectForKey:@"saveCountEvents"];
        NSLog(@"Your Count: %@",countString);
        [userDefaults setObject:[NSString stringWithFormat:@"%d", [countString intValue]-1 ] forKey:@"saveCountEvents"];
        
        [self presentViewController:eventDetail animated:YES completion:nil];
        
        
       }else
       {
        
        [self selectMenuItem:indexPath.row];
        NSLog(@"Menu item selected");
        
       }
      [NSThread detachNewThreadSelector:@selector(stopActivityIndicator) toTarget:_activityIndicator withObject:nil];
    }
    
    else
    {
        UIAlertView *alret=[[UIAlertView alloc]initWithTitle:@"Network Error" message:@"Your Network Is Disconnected. Please Check Your Network Connection And Try Again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alret show];
    }
}

#pragma mark - Helper Methods

-(void)selectMenuItem:(NSInteger)index{
    [_navigationBar showLeftNav];
    
    // change view controller
    
    switch (index) {
        case 1:
        {
            Home *homePage = [Home new];
            [self presentViewController:homePage animated:YES completion:nil];
        }
            break;
            
        case 2:
        {
            MyFavoritesViewController *myFavoritesPage = [MyFavoritesViewController new];
            [self presentViewController:myFavoritesPage animated:YES completion:nil];
            
        }
            
        default:
            break;
    }
}

#pragma mark - Search Method

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    //    [_leftMenu menuCollapse];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    NSLog(@"Start Search");
    NSLog(@"search term %@", searchBar.text);
    
    SearchResultViewController *viewResultFromSearch = [SearchResultViewController new];
    
    //pass values
    viewResultFromSearch.distance = 0;
    viewResultFromSearch.subCategoryName = @"All";
    viewResultFromSearch.subCategoryId = 33;
    viewResultFromSearch.searchText = searchBar.text;
    viewResultFromSearch.mainCategory = @"Shopping";
    
    
    [self presentViewController:viewResultFromSearch animated:YES completion:nil];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

-(void)clickBackButton{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
