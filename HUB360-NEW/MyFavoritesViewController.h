//
//  MyFavoritesViewController.h
//  HUB360-NEW
//
//  Created by Oyuka on 1/13/14.
//  Copyright (c) 2014 Oyuka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SideNavigationDelegate.h"

@interface MyFavoritesViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, SideNavigationDelegate, UISearchBarDelegate>

@end