//
//  GalleryImageDetailViewController.h
//  HUB360-NEW
//
//  Created by Oyuka on 1/16/14.
//  Copyright (c) 2014 Oyuka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SideNavigationDelegate.h"

@interface GalleryImageDetailViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, SideNavigationDelegate, UISearchBarDelegate>

@property NSString *imageURL;
@property NSString *eventTitle;
@property NSString *eventDate;
@property NSString *eventDescription;

@end
