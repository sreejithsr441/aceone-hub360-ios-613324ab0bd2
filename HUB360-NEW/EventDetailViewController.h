//
//  EventDetailViewController.h
//  HUB360-NEW
//
//  Created by Oyuka on 12/27/13.
//  Copyright (c) 2013 Oyuka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EventKit/EventKit.h>
#import <EventKitUI/EventKitUI.h>

@interface EventDetailViewController : UIViewController <UIGestureRecognizerDelegate, UISearchBarDelegate, EKEventEditViewDelegate, UIAlertViewDelegate>

@property NSString *EventName;
@property NSString *EventDesc;
@property NSString *EventDate;
@property NSString *EventDateFull;

@end


//-(id)initwithEventDetail:(NSString *)eventName :(NSString *)eventDesc :(NSString *)eventDate;