//
//  WebViewController.m
//  HUB360-NEW
//
//  Created by Oyuka on 10/24/14.
//  Copyright (c) 2014 Oyuka. All rights reserved.
//

#import "WebViewController.h"
#import "NavigationControllerBuilder.h"
#import "hub360Config.h"
#import "SideNavigationView.h"
#import "UIImageView+Network.h"
#import "Home.h"
#import "ViewDetailViewController.h"
#import "SearchBox.h"
#import "SearchResultViewController.h"
#import "ActivityIndicator.h"
#import "MyFavoritesViewController.h"
#import "AdvertisementBlock.h"

@interface WebViewController ()

@end
@implementation WebViewController
{
    NSMutableArray *_myFavoritesListArray;
    UITableView *_myFavoritesTable;
    NSXMLParser *xmlParser;
    UIView *_mainContainer;
    hub360Config *_config;
    SideNavigationView *_sideNav;
    NavigationControllerBuilder *_navigationBar;
    SearchBox *_searchBox;
    ActivityIndicator *_activityIndicator;
    
    AdvertisementBlock *_advertisementHalfPage;
    
    UIWebView *_webView;
}
@synthesize webURL;
@synthesize title;

-(void)viewDidAppear:(BOOL)animated{
    
    [self checkAdsCount];
}

-(void)checkAdsCount{

    NSUserDefaults *userDefaults=[NSUserDefaults standardUserDefaults];
    NSString *countString = [userDefaults objectForKey:@"saveCountWebViewAds"];
    NSLog(@"Your Count: %@",countString);
    
    if(countString == nil)
        [userDefaults setObject:@"1" forKey:@"saveCountWebViewAds"];
    
    [userDefaults setObject:[NSString stringWithFormat:@"%d", [countString intValue]+1] forKey:@"saveCountWebViewAds"];
    [userDefaults synchronize];
    
    if(([countString integerValue]) % 2 == 0 && countString != nil){
        _advertisementHalfPage = [AdvertisementBlock new];
        [_mainContainer addSubview:_advertisementHalfPage];
        [_advertisementHalfPage getHalfAds];
        [userDefaults setObject:@"1" forKey:@"saveCountWebViewAds"];
    }

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _config = [hub360Config new];
    
    _sideNav = [[SideNavigationView alloc]initWithDelegate:self frame:CGRectMake(0, 20, [_config width]-60, [_config height])];
    [_sideNav setDataSource:self];
    [self.view addSubview:_sideNav];
    
    _mainContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 20, [_config width], [_config height])];
    [_mainContainer setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:_mainContainer];
    
    // adding navigation controller
    _navigationBar = [[NavigationControllerBuilder alloc]initWithFrame:CGRectMake(0, 0, 320, 45)];
    [_navigationBar addNavigationContoller:title :NO];
    [_mainContainer addSubview:_navigationBar];
    
    _webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 44, 320, [_config height]-60)];
    _webView.delegate = self;
    [_mainContainer addSubview:_webView];
    [self loadWebView:webURL];
    
    _activityIndicator = [ActivityIndicator new];
    [_mainContainer addSubview:_activityIndicator];
    [NSThread detachNewThreadSelector:@selector(startActivityIndicator) toTarget:_activityIndicator withObject:nil];
    [_activityIndicator performSelector:@selector(stopAnimating) withObject:nil afterDelay:1];
}

-(void)loadWebView:(NSURL*)URl{
    
    [NSThread detachNewThreadSelector:@selector(startActivityIndicator) toTarget:_activityIndicator withObject:nil];
    [_webView loadRequest: [NSURLRequest requestWithURL:URl]];
    [_activityIndicator performSelector:@selector(stopAnimating) withObject:nil afterDelay:3];
}

#pragma mark - TableView Delegate Methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_sideNav menucount];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *list = [[NSArray alloc]initWithArray:[_sideNav listMenuItems]];
    NSArray *iconList = [[NSArray alloc]initWithArray:[_sideNav listMenuItemIcons]];
    
    UITableViewCell *cell;
    
    if(indexPath.row == 0){
        cell = [tableView dequeueReusableCellWithIdentifier:@"searchBox"];
    }else{
        cell = [tableView dequeueReusableCellWithIdentifier:[list objectAtIndex:indexPath.row-1]];
    }
    
    
    if (cell == nil){
        
        if(indexPath.row == 0){
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"searchBox"];
            cell.backgroundColor = [UIColor clearColor];
            
            _searchBox = [[SearchBox alloc]initWithFrame:CGRectMake(0, 0, cell.frame.size.width-60, cell.frame.size.height)];
            _searchBox.delegate = self;
            [cell.contentView addSubview:_searchBox];
            
        }else{
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:[list objectAtIndex:indexPath.row-1]];
            
            cell.textLabel.text = [list objectAtIndex:indexPath.row-1];
            [cell.textLabel setTextColor:[UIColor whiteColor]];
            cell.backgroundColor = [UIColor clearColor];
            cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14.0];
            
            UIImageView *accessoryImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 12.5, 19)];
            [accessoryImage setImage:[UIImage imageNamed:@"menu_cell_indicator"]];
            cell.accessoryView = accessoryImage;
            [cell.imageView setImage:[UIImage imageNamed:[iconList objectAtIndex:indexPath.row-1]]];
        }
        
        
        
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.row > 0){
        [self selectMenuItem:indexPath.row];
        NSLog(@"Menu item selected");
    }
}

#pragma mark - Helper Methods

-(void)selectMenuItem:(NSInteger)index{
    [_navigationBar showLeftNav];
    
    // change view controller
    
    switch (index) {
        case 1:
        {
            Home *homePage = [Home new];
            [self presentViewController:homePage animated:YES completion:nil];
        }
            
            
        case 2:
        {
            MyFavoritesViewController *myFavoritesPage = [MyFavoritesViewController new];
            [self presentViewController:myFavoritesPage animated:YES completion:nil];
            break;
        }
            
        case 3:
        {
            title = @"Coupons-Our Pick";
            [_navigationBar changeTitle:title];
            [self loadWebView:[NSURL URLWithString:@"http://aceonetechnologies.com/coupons-our-pick/"]];
            [self checkAdsCount];
            break;
            
        }
            
        case 4:
        {
            title = @"ASU Game Schedule";
            [_navigationBar changeTitle:title];
            [self loadWebView:[NSURL URLWithString:@"http://aceonetechnologies.com/asu-game-schedule/"]];
            [self checkAdsCount];
            break;
        }
            
        case 5:
        {
            [self loadWebView:[NSURL URLWithString:@"http://aceonetechnologies.com/score-alert/"]];
            title = @"Jonesboro Sports";
            [_navigationBar changeTitle:title];
            [self checkAdsCount];
            break;
        }
            
        case 6:
        {
            [self loadWebView:[NSURL URLWithString:@"http://aceonetechnologies.com/news-events/"]];
            title = @"News And Events";
            [_navigationBar changeTitle:title];
            [self checkAdsCount];
            break;

        }


            
        default:
            break;
    }
    
}


@end
