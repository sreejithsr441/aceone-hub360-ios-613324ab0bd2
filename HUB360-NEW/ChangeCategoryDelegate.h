//
//  ChangeCategoryDelegate.h
//  HUB360-NEW
//
//  Created by Oyuka on 12/26/13.
//  Copyright (c) 2013 Oyuka. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ChangeCategoryDelegate <NSObject>

- (void)categoryButtonTappedWithTag:(int)buttonTag;

@end
