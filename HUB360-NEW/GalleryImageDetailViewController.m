//
//  GalleryImageDetailViewController.m
//  HUB360-NEW
//
//  Created by Oyuka on 1/16/14.
//  Copyright (c) 2014 Oyuka. All rights reserved.
//

#import "GalleryImageDetailViewController.h"
#import "NavigationControllerBuilder.h"
#import "hub360Config.h"
#import "SideNavigationView.h"
#import "MyFavoritesViewController.h"
#import "Home.h"
#import "SearchBox.h"
#import "SearchResultViewController.h"
#import "Carousel.h"
#import "UIImageView+Network.m"
#import "ActivityIndicator.h"
#import "AdvertisementBlock.h"
#import "SRUtility.h"
@interface GalleryImageDetailViewController ()
@end

@implementation GalleryImageDetailViewController
{
    UIView *_mainContainer;
    hub360Config *_config;
    SideNavigationView *_sideNav;
    NavigationControllerBuilder *_navigationBar;
    SearchBox *_searchBox;
    Carousel *_carouselView;
    AdvertisementBlock *_advertisementBanner;
}

@synthesize imageURL;
@synthesize eventDate;
@synthesize eventTitle;
@synthesize eventDescription;


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    _config = [hub360Config new];
    
    _sideNav = [[SideNavigationView alloc]initWithDelegate:self frame:CGRectMake(0, 20, [_config width]-60, [_config height])];
    [_sideNav setDataSource:self];
    [self.view addSubview:_sideNav];
    
    _mainContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 20, [_config width], [_config height])];
    _mainContainer.backgroundColor =[UIColor colorWithRed:(237.0/255.0) green:(237.0/255.0) blue:(237.0/255.0) alpha:(1)];
    [self.view addSubview:_mainContainer];
    
    //scroll view
    
    _carouselView = [[Carousel alloc]initWithFrame:CGRectMake(0, 45, [_config width], [_config height])];
    
    [_carouselView setBackgroundColor:[UIColor clearColor]];
    _carouselView.scrollView.showsVerticalScrollIndicator = YES;
    [_carouselView.scrollView setDelegate:self];
    [_carouselView.scrollView setFrame:CGRectMake(0, 0, 320, [_config height])];
    _carouselView.scrollView.clipsToBounds = YES;
    [_mainContainer addSubview:_carouselView];
    [_carouselView.scrollView flashScrollIndicators];
    
     [self showImage];
        [self showImageDetail];
        
        // adding navigation controller
        _navigationBar = [[NavigationControllerBuilder alloc]initWithFrame:CGRectMake(0, 0, 320, 45)];
        [_navigationBar addNavigationContoller:@"Gallery" :YES];
        [_mainContainer addSubview:_navigationBar];
        
        // Banner add
        //_advertisementBanner = [AdvertisementBlock new];
        //[_mainContainer addSubview:_advertisementBanner];
       // [_advertisementBanner getBannerAds];
        
        if([_config height] < 500){
            _carouselView.scrollView.contentSize = CGSizeMake(320, 570);
            [_advertisementBanner setFrame:CGRectMake(0, 410, 320, 50)];
        }

    
    
    
}

-(void)showImageDetail
{
    UILabel *imageTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 315, 300, 25)];
    imageTitle.text = eventTitle;
    imageTitle.textColor = [UIColor darkGrayColor];
    [imageTitle setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:14.0f]];
    [_carouselView.scrollView addSubview:imageTitle];
    
    UILabel *imageDate = [[UILabel alloc] initWithFrame:CGRectMake(10, 335, 300, 20)];
    imageDate.text = eventDate;
    imageDate.textColor = [UIColor grayColor];
    [imageDate setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:12.0f]];
    [_carouselView.scrollView addSubview:imageDate];
    
    UITextView *imageDesc = [[UITextView alloc] initWithFrame:CGRectMake(10, 355, 300, 90)];
    imageDesc.editable = NO;
    imageDesc.scrollEnabled = YES;
    imageDesc.text = eventDescription;
    imageDesc.textColor = [UIColor grayColor];
    [imageDate setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:12.0f]];
    [_carouselView.scrollView addSubview:imageDesc];
}

-(void)showImage
{
    
    // image 1
    UIView *galleryImageView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 300, 300)];
    [_carouselView.scrollView addSubview:galleryImageView];
    
    galleryImageView.layer.shadowColor = [UIColor darkGrayColor].CGColor;
    galleryImageView.layer.shadowOpacity = 1;
    galleryImageView.layer.shadowRadius = 2;
    galleryImageView.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
    
    galleryImageView.layer.borderColor = [[UIColor whiteColor]CGColor];
    galleryImageView.layer.borderWidth = 5;
    galleryImageView.layer.cornerRadius = 5;
    
    UIImageView *galleryImage = [[UIImageView alloc]initWithFrame:CGRectMake(5, 5, 290, 290)];
    
    //activity indicator on the image till image is loaded
    ActivityIndicator *activityIndicator = [ActivityIndicator new];
    [activityIndicator setFrame:CGRectMake(0, 0, 300, 300)];
    [galleryImage addSubview:activityIndicator];
    
    [NSThread detachNewThreadSelector:@selector(startActivityIndicator) toTarget:activityIndicator withObject:nil];
    
    NSString *tmpimageurl = imageURL;
    NSArray *cachingkey = [tmpimageurl componentsSeparatedByString:@"/"];
    
    [galleryImage loadImageFromURL:[NSURL URLWithString:imageURL]
                  placeholderImage:[UIImage imageNamed:@"galleryimage580@2x"]
                        cachingKey:[cachingkey lastObject]
                             frame:CGRectMake(5, 5, 290, 290)];
    galleryImage.contentMode = UIViewContentModeScaleAspectFit;
    
    [galleryImageView setBackgroundColor:[UIColor whiteColor]];
    [galleryImageView addSubview:galleryImage];
    [_carouselView.scrollView addSubview:galleryImageView];

    [NSThread detachNewThreadSelector:@selector(stopActivityIndicator) toTarget:activityIndicator withObject:nil];
    
}

-(void)clickBackButton
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [_sideNav menucount];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 40.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSArray *list = [[NSArray alloc]initWithArray:[_sideNav listMenuItems]];
    NSArray *iconList = [[NSArray alloc]initWithArray:[_sideNav listMenuItemIcons]];
    
    UITableViewCell *cell;
    
    if(indexPath.row == 0){
        cell = [tableView dequeueReusableCellWithIdentifier:@"searchBox"];
    }else{
        cell = [tableView dequeueReusableCellWithIdentifier:[list objectAtIndex:indexPath.row-1]];
    }
    
    
    if (cell == nil){
        
        if(indexPath.row == 0){
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"searchBox"];
            cell.backgroundColor = [UIColor clearColor];
            
            _searchBox = [[SearchBox alloc]initWithFrame:CGRectMake(0, 0, cell.frame.size.width-60, cell.frame.size.height)];
            _searchBox.delegate = self;
            [cell.contentView addSubview:_searchBox];
            
        }else{
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:[list objectAtIndex:indexPath.row-1]];
            
            cell.textLabel.text = [list objectAtIndex:indexPath.row-1];
            [cell.textLabel setTextColor:[UIColor whiteColor]];
            cell.backgroundColor = [UIColor clearColor];
            
            UIImageView *accessoryImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 12.5, 19)];
            [accessoryImage setImage:[UIImage imageNamed:@"menu_cell_indicator"]];
            cell.accessoryView = accessoryImage;
            [cell.imageView setImage:[UIImage imageNamed:[iconList objectAtIndex:indexPath.row-1]]];
        }
        
    }
    
    return cell;
}

-(NSString *)replaceAscii:(NSString*)string{
    NSString *s = string;
    s = [s stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    return s;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   
    
    
    if ([SRUtility reachable])
    {
        [self selectMenuItem:indexPath.row];

    }
    else
    {
        UIAlertView *alret=[[UIAlertView alloc]initWithTitle:@"Network Error" message:@"Your Network Is Disconnected. Please Check Your Network Connection And Try Again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alret show];
    }
    
    
    NSLog(@"Menu item selected");
}

#pragma mark - Helper Methods

-(void)selectMenuItem:(NSInteger)index{
    [_navigationBar showLeftNav];
    
    // change view controller
    
    switch (index) {
        case 1:
        {
            Home *homePage = [Home new];
            [self presentViewController:homePage animated:YES completion:nil];
        }
            break;
            
        case 2:
        {
            MyFavoritesViewController *myFavoritesPage = [MyFavoritesViewController new];
            [self presentViewController:myFavoritesPage animated:YES completion:nil];
            
        }
            
        default:
            break;
    }
}

#pragma mark - Search Method

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    //    [_leftMenu menuCollapse];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    NSLog(@"Start Search");
    NSLog(@"search term %@", searchBar.text);
    
    SearchResultViewController *viewResultFromSearch = [SearchResultViewController new];
    
    //pass values
    viewResultFromSearch.distance = 0;
    viewResultFromSearch.subCategoryName = @"All";
    viewResultFromSearch.subCategoryId = 33;
    viewResultFromSearch.searchText = searchBar.text;
    viewResultFromSearch.mainCategory = @"Shopping";
    
    
    [self presentViewController:viewResultFromSearch animated:YES completion:nil];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

@end
