//
//  GalleryViewController.h
//  HUB360-NEW
//
//  Created by Oyuka on 1/13/14.
//  Copyright (c) 2014 Oyuka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SideNavigationDelegate.h"

@interface GalleryViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, SideNavigationDelegate, UISearchBarDelegate, UIGestureRecognizerDelegate>

@end
