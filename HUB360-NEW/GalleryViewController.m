//
//  GalleryViewController.m
//  HUB360-NEW
//
//  Created by Oyuka on 1/13/14.
//  Copyright (c) 2014 Oyuka. All rights reserved.
//

#import "GalleryViewController.h"
#import "NavigationControllerBuilder.h"
#import "hub360Config.h"
#import "SideNavigationView.h"
#import "MyFavoritesViewController.h"
#import "Home.h"
#import "SearchBox.h"
#import "SearchResultViewController.h"
#import "Carousel.h"
#import "UIImageView+Network.m"
#import "GalleryImageDetailViewController.h"
#import "ActivityIndicator.h"
#import "AdvertisementBlock.h"
#import "SRUtility.h"
@interface GalleryViewController ()

@end

#define IS_IPHONE_5 (((double)[[UIScreen mainScreen] bounds].size.height) == ((double)568))

@implementation GalleryViewController
{
    UIView *_mainContainer;
    hub360Config *_config;
    SideNavigationView *_sideNav;
    NavigationControllerBuilder *_navigationBar;
    SearchBox *_searchBox;
    Carousel *_carouselView;
    ActivityIndicator *_activityIndicator;
    NSArray *_galleryImagesArray;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    _config = [hub360Config new];
    
    _sideNav = [[SideNavigationView alloc]initWithDelegate:self frame:CGRectMake(0, 20, [_config width]-60, [_config height])];
    [_sideNav setDataSource:self];
    [self.view addSubview:_sideNav];
    
    _mainContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 20, [_config width], [_config height])];
    _mainContainer.backgroundColor =[UIColor colorWithRed:(237.0/255.0) green:(237.0/255.0) blue:(237.0/255.0) alpha:(1)];
    [self.view addSubview:_mainContainer];
    
    //scroll view
    
    _carouselView = [[Carousel alloc]initWithFrame:CGRectMake(0, 45, [_config width], [_config height])];
    
    [_carouselView setBackgroundColor:[UIColor clearColor]];
    _carouselView.scrollView.showsVerticalScrollIndicator = YES;
    [_carouselView.scrollView setDelegate:self];
    [_carouselView.scrollView setFrame:CGRectMake(0, 0, 320, [_config height])];
    _carouselView.scrollView.clipsToBounds = YES;
    [_mainContainer addSubview:_carouselView];
    [_carouselView.scrollView flashScrollIndicators];
    
    
    [self showImages];
    
    
    // adding navigation controller
    _navigationBar = [[NavigationControllerBuilder alloc]initWithFrame:CGRectMake(0, 0, 320, 45)];
    [_navigationBar addNavigationContoller:@"Gallery" :YES];
    [_mainContainer addSubview:_navigationBar];
    
    _activityIndicator = [ActivityIndicator new];
    [_carouselView addSubview:_activityIndicator];
    
    //AdvertisementBlock *halfPageAdvertisement = [AdvertisementBlock new];
    //[_mainContainer addSubview:halfPageAdvertisement];
    //[halfPageAdvertisement getHalfAds];
    
   // if(!IS_IPHONE_5)
   // {
     //   [halfPageAdvertisement setFrame:CGRectMake(0, 80, 320, 337)];
   // }
    
}

-(void)showImages
{
    
    
    //galleryImages
    NSString *sopMessage=[NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                          "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                          "<soap:Body>"
                          "<GetGalleryImage xmlns=\"http://www.hub360.com/\">"
                          "</GetGalleryImage>"
                          "</soap:Body>"
                          "</soap:Envelope>"];
    
    NSLog(@"value is%@",sopMessage);
	
	NSURL *url=[NSURL URLWithString:@"http://webdesignjonesboro.com/hub360service/hub360Service.asmx/GetGalleryImage"];
	NSMutableURLRequest *req=[NSMutableURLRequest requestWithURL:url];
	NSString *msglength=[NSString stringWithFormat:@"%lu",(unsigned long)[sopMessage length]];
	[req addValue:@"text/xml;charset=utf-8"  forHTTPHeaderField:@"Content-Type"];
	[req addValue:@"http://webdesignjonesboro.com/hub360service/hub360Service.asmx" forHTTPHeaderField:@"SOAPAction"];
	
	[req addValue: msglength forHTTPHeaderField:@"Content-Length"];
	[req setHTTPMethod:@"POST"];
	[req setHTTPBody:[sopMessage dataUsingEncoding:NSUTF8StringEncoding]];
	
	NSError *error1;
	error1 = [NSError new];
	
	NSData *returnData1 = [NSURLConnection sendSynchronousRequest:req returningResponse:nil error:&error1];
    NSUInteger DataLength=[returnData1 length];
    if (DataLength==0)
    {
        UIAlertView *alret=[[UIAlertView alloc]initWithTitle:@"Network Error" message:@"Your Network Is Disconnected. Please Check Your Network Connection And Try Again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alret show];
    }
    else
    {
        
        NSString *returnString = [[NSString alloc] initWithData:returnData1 encoding:NSUTF8StringEncoding];
        NSLog(@"%@",returnString);
        
        NSRange start;
        NSRange stop;
        
        start = [returnString rangeOfString:@"<string xmlns=\"http://www.hub360nea.com/\">"];
        stop = [returnString rangeOfString: @"</string>"];
        
        unsigned long start1 = start.location+start.length;
        unsigned long start2 = stop.location-start1;
        
        
        NSString *jsonString = [returnString substringWithRange: NSMakeRange (start1, start2)];
        
        NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        NSError *e = nil;
        NSMutableArray *json = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&e];
        NSLog(@"%@", json);
        _galleryImagesArray = [json valueForKey:@"List`1"];
        
        
        [self displayGalleryImages];
     
    }
    
   
    
}

-(void)displayGalleryImages{
    
    if([_galleryImagesArray count]>0)
    {
        
        int x = 5;
        int y = 5;
        
        for(int i=0; i<[_galleryImagesArray count]; i++)
        {
            
            if(i%2 == 1 && i!= 0){
                x += 160;
                y -=160;
            }
            
            
            // image 1
            UIView *galleryImageView = [[UIImageView alloc]initWithFrame:CGRectMake(x, y, 150, 150)];
            galleryImageView.userInteractionEnabled = YES;
            
            UITapGestureRecognizer *openImageGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(openGalleryImage:)];
            openImageGesture.delegate = self;
            [galleryImageView addGestureRecognizer:openImageGesture];
            
            
            galleryImageView.layer.shadowColor = [UIColor darkGrayColor].CGColor;
            galleryImageView.layer.shadowOpacity = 1;
            galleryImageView.layer.shadowRadius = 2;
            galleryImageView.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
            
            galleryImageView.layer.borderColor = [[UIColor whiteColor]CGColor];
            galleryImageView.layer.borderWidth = 5;
            galleryImageView.layer.cornerRadius = 5;
            galleryImageView.backgroundColor = [UIColor whiteColor];
            galleryImageView.tag = i;
            
            UIImageView *galleryImage = [[UIImageView alloc]initWithFrame:CGRectMake(5, 5, 140, 140)];
            galleryImage.contentMode = UIViewContentModeScaleAspectFit;
            NSString *tmpimageurl = [[_galleryImagesArray objectAtIndex:i] objectForKey:@"ImageURL"];
            NSArray *cachingkey = [tmpimageurl componentsSeparatedByString:@"/"];
            
            [galleryImage loadImageFromURL:[NSURL URLWithString:[[_galleryImagesArray objectAtIndex:i] objectForKey:@"ImageURL"]]
                          placeholderImage:[UIImage imageNamed:@"galleryimage280@2x"]
                                cachingKey:[cachingkey lastObject]
                                     frame:CGRectMake(5, 5, 140, 140)];
            
            [galleryImageView addSubview:galleryImage];
            [_carouselView.scrollView addSubview:galleryImageView];
            
            y+=160;
            x = 5;
            
        }
         _carouselView.scrollView.contentSize = CGSizeMake(320, y+150);
    }
    
  
   

}

-(void)openGalleryImage:(UIGestureRecognizer*)gestureRecognizer
{
  
    
    if ([SRUtility reachable])
    {
        
        UIImageView* imageView = (UIImageView*)gestureRecognizer.view;
        NSInteger index = imageView.tag;
        
        [NSThread detachNewThreadSelector:@selector(startActivityIndicator) toTarget:_activityIndicator withObject:nil];
        GalleryImageDetailViewController *galleryDetailPage = [GalleryImageDetailViewController new];
        
        galleryDetailPage.imageURL = [[_galleryImagesArray objectAtIndex:index] objectForKey:@"ImageURL"];
        galleryDetailPage.eventTitle =[[_galleryImagesArray objectAtIndex:index] objectForKey:@"ImageTitle"];
        galleryDetailPage.eventDate = [[_galleryImagesArray objectAtIndex:index] objectForKey:@"DateAdded"];
        galleryDetailPage.eventDescription = [[_galleryImagesArray objectAtIndex:index] objectForKey:@"ImageDescription"];
        
        [self presentViewController:galleryDetailPage animated:YES completion:nil];
        
        [NSThread detachNewThreadSelector:@selector(stopActivityIndicator) toTarget:_activityIndicator withObject:nil];
    }
    else
    {
        UIAlertView *alret=[[UIAlertView alloc]initWithTitle:@"Network Error" message:@"Your Network Is Disconnected. Please Check Your Network Connection And Try Again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alret show];
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [_sideNav menucount];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 40.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSArray *list = [[NSArray alloc]initWithArray:[_sideNav listMenuItems]];
    NSArray *iconList = [[NSArray alloc]initWithArray:[_sideNav listMenuItemIcons]];
    
    UITableViewCell *cell;
    
    if(indexPath.row == 0){
        cell = [tableView dequeueReusableCellWithIdentifier:@"searchBox"];
    }else{
        cell = [tableView dequeueReusableCellWithIdentifier:[list objectAtIndex:indexPath.row-1]];
    }
    
    
    if (cell == nil){
        
        if(indexPath.row == 0){
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"searchBox"];
            cell.backgroundColor = [UIColor clearColor];
            
             _searchBox = [[SearchBox alloc]initWithFrame:CGRectMake(0, 0, cell.frame.size.width-60, cell.frame.size.height)];
            _searchBox.delegate = self;
            [cell.contentView addSubview:_searchBox];
            
        }else{
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:[list objectAtIndex:indexPath.row-1]];
            
            cell.textLabel.text = [list objectAtIndex:indexPath.row-1];
            [cell.textLabel setTextColor:[UIColor whiteColor]];
            cell.backgroundColor = [UIColor clearColor];
            
            UIImageView *accessoryImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 12.5, 19)];
            [accessoryImage setImage:[UIImage imageNamed:@"menu_cell_indicator"]];
            cell.accessoryView = accessoryImage;
            [cell.imageView setImage:[UIImage imageNamed:[iconList objectAtIndex:indexPath.row-1]]];
        }

    }
    
    return cell;
}

-(NSString *)replaceAscii:(NSString*)string{
    NSString *s = string;
    s = [s stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    return s;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([SRUtility reachable])
    {
        [self selectMenuItem:indexPath.row];
        
        NSLog(@"Menu item selected");

    }
    else
    {
        UIAlertView *alret=[[UIAlertView alloc]initWithTitle:@"Network Error" message:@"Your Network Is Disconnected. Please Check Your Network Connection And Try Again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alret show];
    }
}

#pragma mark - Helper Methods

-(void)selectMenuItem:(NSInteger)index{
    [_navigationBar showLeftNav];
    
    // change view controller
    
    switch (index) {
        case 1:
        {
            Home *homePage = [Home new];
            [self presentViewController:homePage animated:YES completion:nil];
        }
            break;
            
        case 2:
        {
            MyFavoritesViewController *myFavoritesPage = [MyFavoritesViewController new];
            [self presentViewController:myFavoritesPage animated:YES completion:nil];
            
        }
            
        default:
            break;
    }
}

#pragma mark - Search Method

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    //    [_leftMenu menuCollapse];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    NSLog(@"Start Search");
    NSLog(@"search term %@", searchBar.text);
    
    SearchResultViewController *viewResultFromSearch = [SearchResultViewController new];
    
    //pass values
    viewResultFromSearch.distance = 0;
    viewResultFromSearch.subCategoryName = @"All";
    viewResultFromSearch.subCategoryId = 33;
    viewResultFromSearch.searchText = searchBar.text;
    viewResultFromSearch.mainCategory = @"Shopping";
    
    
    [self presentViewController:viewResultFromSearch animated:YES completion:nil];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

-(void)clickBackButton{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
