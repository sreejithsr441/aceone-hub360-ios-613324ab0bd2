//
//  SearchBox.m
//  HUB360-NEW
//
//  Created by Oyuka on 1/13/14.
//  Copyright (c) 2014 Oyuka. All rights reserved.
//

#import "SearchBox.h"

@implementation SearchBox

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0f) {
            for (UIView *view in [self subviews]) {
                
                for (UIView *secondLeveSubView in [view subviews])
                {
                    if (![secondLeveSubView isKindOfClass:[UITextField class]]) {
                        [secondLeveSubView removeFromSuperview];
                        NSLog(@"removing subview");
                    }
                    else
                    {
                        UITextField *v = (UITextField*)secondLeveSubView;
                        //  [v setBackground:[UIImage imageNamed:@"searchboxbg"]];
                        [v setBorderStyle:UITextBorderStyleRoundedRect];
                        v.layer.borderWidth = 1;
                        v.layer.cornerRadius = 5;
                        v.layer.borderColor = [[UIColor colorWithRed:(245/255.0) green:(76/255.0) blue:(10/255.0) alpha:1] CGColor];
                        [v setTextAlignment:NSTextAlignmentLeft];
                        NSLog(@"adding background image");
                    }
                    
                    
                    if ([secondLeveSubView conformsToProtocol:@protocol(UITextInputTraits)]) {
                        
                        @try {
                            NSLog(@"adding return key type");
                            [(UITextField *)secondLeveSubView setReturnKeyType:UIReturnKeySearch];
                        }
                        @catch (NSException * e) {
                            NSLog(@"something happened %@",e);
                        }
                    }
                }
            }
        }else if([[[UIDevice currentDevice] systemVersion] floatValue] < 7.0f){
            for (UIView *view in [self subviews]) {
                if (![view isKindOfClass:[UITextField class]]) {
                    [view removeFromSuperview];
                }
                else
                {
                    UITextField *v = (UITextField*)view;
                    //  [v setBackground:[UIImage imageNamed:@"searchboxbg"]];
                    [v setBorderStyle:UITextBorderStyleRoundedRect];
                    v.layer.borderWidth = 1;
                    v.layer.cornerRadius = 5;
                    v.layer.borderColor = [[UIColor colorWithRed:(245/255.0) green:(76/255.0) blue:(10/255.0) alpha:1] CGColor];
                    [v setTextAlignment:NSTextAlignmentLeft];
                }
                
                
                if ([view conformsToProtocol:@protocol(UITextInputTraits)]) {
                    
                    @try {
                        
                        [(UITextField *)view setReturnKeyType:UIReturnKeySearch];
                    }
                    @catch (NSException * e) {
                        
                        // ignore exception
                    }
                }
                
            }
        }
        
    }
    
    self.backgroundColor = [UIColor whiteColor];
    self.placeholder = @"Search";
    return self;
}

@end
