//
//  CategoryButton.m

#import "CategoryButton.h"
#import "Home.h"
#import "hub360Config.h"
@implementation CategoryButton{
    hub360Config *_config;
}

-(id)initWithDelegate:(id<ChangeCategoryDelegate>)delegate initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setFrame:frame];
        colorNormal = [UIColor whiteColor];
        colorHighlighted = [UIColor darkGrayColor];
        colorSelected = [UIColor blackColor];
        self.delegate = delegate;
    }
    return self;
}

//- (id)initWithFrame:(CGRect)frame
//{
//    self = [super initWithFrame:frame];
//    if (self) {
//        // Initialization code
//        [self setFrame:frame];
//        colorNormal = [UIColor whiteColor];
//        colorHighlighted = [UIColor darkGrayColor];
//        colorSelected = [UIColor blackColor];
//    }
//    return self;
//}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void) setCustomTitle:(NSString *)title :(UIColor *)bgColor
{
    if (lblTitle == nil) {
        lblTitle = [[UILabel alloc] init];
        [lblTitle setFrame:CGRectMake( 0, 105, 160, 35 )];
        
        if([_config height] < 500){
            [lblTitle setFrame:CGRectMake( 0, 90, 160, 35 )];
        }
        
        [lblTitle setBackgroundColor:[UIColor clearColor]];
        [lblTitle setTextColor:[UIColor whiteColor]];
        [lblTitle setTextAlignment:NSTextAlignmentCenter];
        [lblTitle setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:18.0f]];
        [self addSubview:lblTitle];
        
        [self addTarget:self action:@selector(button_touchdown) forControlEvents:UIControlEventTouchDown];
        [self addTarget:self action:@selector(button_touchup:) forControlEvents:UIControlEventTouchUpInside];
        [self addTarget:self action:@selector(button_touchup:) forControlEvents:UIControlEventTouchUpOutside];
        
        UIColor * color = nil;
        if (self.state == UIControlStateNormal) {
            color = colorNormal;
        }
        else if (self.state == UIControlStateHighlighted) {
            color = colorHighlighted;
        }
        else if (self.state == UIControlStateSelected) {
            color = colorSelected;
        }
        [lblTitle setTextColor:color];
    }
    
    [lblTitle setText:title];
    [self setBackgroundColor:bgColor];
    [lblTitle setTextColor:[UIColor whiteColor]];
}

- (void) setCustomIcon:(UIImage *)icon{
    
    UIImageView *buttonIcon = [[UIImageView alloc] initWithImage:icon];
    CGFloat width = icon.size.width;
    [buttonIcon setFrame:CGRectMake((160-width)/2, 25, width, 80)];
    if([_config height] < 500){
        [buttonIcon setFrame:CGRectMake((160-width)/2, 10, width, 80)];
    }
    [self addSubview:buttonIcon];
}


- (void) setTitleColor:(UIColor *)color forState:(UIControlState)state
{
    [super setTitleColor:color forState:state];
    
    if (state == UIControlStateNormal) {
        colorNormal = color;
    }
    else if (state == UIControlStateHighlighted) {
        colorHighlighted = color;
    }
    else if (state == UIControlStateSelected) {
        colorSelected = color;
    }
    
    [lblTitle setTextColor:color];
}


- (IBAction) button_touchdown
{
    [lblTitle setTextColor:colorHighlighted];
}


- (IBAction) button_touchup:(id)sender
{
    UIButton *thisButton = (UIButton*)sender;
    [lblTitle setTextColor:colorNormal];
    NSLog(@"%ld", (long)thisButton.tag);
    
    [self.delegate categoryButtonTappedWithTag:(int)thisButton.tag];
}


- (void) setSelected:(BOOL)selected
{
    [super setSelected:selected];
    
    if (selected) {
        [lblTitle setTextColor:colorSelected];
    }
    else {
        [lblTitle setTextColor:colorNormal];
    }
}


@end
