//
//  ActivityIndicator.m
//  HUB360-NEW
//
//  Created by Oyuka on 1/16/14.
//  Copyright (c) 2014 Oyuka. All rights reserved.
//

#import "ActivityIndicator.h"
#import "hub360Config.h"

@implementation ActivityIndicator

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        hub360Config *_config = [hub360Config new];
        //activity inidicator
        self.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
        self.frame = CGRectMake(0,0,320,[_config height]);
        CGAffineTransform transform = CGAffineTransformMakeScale(1.5f, 1.5f);
        self.transform = transform;
        
        self.center = CGPointMake ([_config width] * 0.5F, [_config height] * 0.3F);
        
        self.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin);
        self.hidden = TRUE;
    }
    return self;
}

-(void)startActivityIndicator
{
    self.hidden = TRUE;
    [self startAnimating];
}

-(void)stopActivityIndicator
{
    sleep(1.5);
    [self stopAnimating];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
